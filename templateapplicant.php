<?php
if (!defined('WEB_ROOT')) {
	exit;
}

$self = WEB_ROOT . 'index.php';
?>
<!DOCTYPE html>
<html lang="en">
<head>
	
	<!-- start: Meta -->
	<meta charset="utf-8">
	<title><?php echo $pageTitle; ?></title>
	<meta name="description" content="Bootstrap Metro Dashboard">
	<meta name="author" content="@zerochills_">
	<meta name="keyword" content="A fully responsive recruitment portal based on Bootstrap 3">
	<!-- end: Meta -->
	
	<!-- start: Mobile Specific -->
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<!-- end: Mobile Specific -->
	
	<!-- start: CSS -->
	<link id="bootstrap-style" href="css/bootstrap.min.css" rel="stylesheet">
	<link href="css/bootstrap-responsive.min.css" rel="stylesheet">
	<link id="base-style" href="css/style.css" rel="stylesheet">
	<link id="base-style-responsive" href="css/style-responsive.css" rel="stylesheet">
	<link href='http://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800&subset=latin,cyrillic-ext,latin-ext' rel='stylesheet' type='text/css'>
	<!-- end: CSS -->
	

	<!-- The HTML5 shim, for IE6-8 support of HTML5 elements -->
	<!--[if lt IE 9]>
	  	<script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
		<link id="ie-style" href="css/ie.css" rel="stylesheet">
	<![endif]-->
	
	<!--[if IE 9]>
		<link id="ie9style" href="css/ie9.css" rel="stylesheet">
	<![endif]-->
		
	<!-- start: Favicon -->
	<link rel="shortcut icon" href="img/favicon.ico">
	<style type="text/css">
	#icons {
	width: 340px;
	float: none;
}
    #openingicons {
	height: 100px;
	width: 660px;
	padding-left: 40px;
}
#personalprofile {
	height: 100px;
	width: 660px;
	padding-left: 40px;
}
    #icon {
	float: right;
	width: 320px;
}
    body,td,th {
	font-family: "Open Sans", sans-serif;
}
body {
	background-image: url();
}
    </style>
	<!-- end: Favicon -->
	
		
		
		
</head>

<body>
		<!-- start: Header -->
	<div class="navbar">
		<div class="navbar-inner">
			<div class="container-fluid">
				<a class="btn btn-navbar" data-toggle="collapse" data-target=".top-nav.nav-collapse,.sidebar-nav.nav-collapse">
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
				</a>
				<a class="brand" href="indexapplicant.php"><span> <img src="images/Centum-E-Registration-logo.png" alt="logo" style="width:200px; height:98px"></span></a>
								
				<!-- start: Header Menu -->
				<div class="nav-no-collapse header-nav">
					<ul class="nav pull-right">
					
						<!-- start: User Dropdown -->
						<li class="dropdown">
							<a class="btn dropdown-toggle" data-toggle="dropdown" href="#">
								<i class="halflings-icon white user"></i> <?php
include"include/config.php";
$id =$_SESSION['centum_user_id'];
$errorMessage = (isset($_GET['error']) && $_GET['error'] != '') ? $_GET['error'] : '&nbsp;';

$sql="SELECT id, fname,sname,email
FROM c_users WHERE id='$id'";

$result = dbQuery($sql);
$row = dbFetchAssoc($result);
extract($row);
?>
<?php echo $email; ?>

								<span class="caret"></span>
							</a>
							<ul class="dropdown-menu">
								<li class="dropdown-menu-title">
 									<span>Account Settings</span>
								</li>
								<li><a href="http://localhost/centm/indexapplicant.php?view=addpd"><i class="halflings-icon user"></i> Profile</a></li>
								<li><a href="logout.php"><i class="halflings-icon off"></i> Logout</a></li>
							</ul>
						</li>
						<!-- end: User Dropdown -->
					</ul>
					
				</div>
				
				<!-- end: Header Menu -->
				
			</div>
		</div>
	</div>
	<!-- start: Header -->
	
		<div class="container-fluid-full">
		<div class="row-fluid">
		  
		  </div>	
			<!-- start: Main Menu -->
			<div id="sidebar-left" class="span2">
			  <div class="nav-collapse sidebar-nav">
			    <ul class="nav nav-tabs nav-stacked main-menu">
			      <li><a href="indexapplicant.php?view=list"><i class="icon-tasks"></i><span class="hidden-tablet"> Openings</span></a></li>
			      <li> <a class="dropmenu" href="#"><i class="icon-folder-close-alt"></i><span class="hidden-tablet"> Personal Profile</span></a>
			        <ul>
			          <li><a class="submenu" href="indexapplicant.php?view=modifypd"><i class="icon-file-alt"></i><span class="hidden-tablet">Personal Details</span></a></li>
			          <li><a class="submenu" href="indexapplicant.php?view=modifyeb"><i class="icon-file-alt"></i><span class="hidden-tablet"> Educational Background</span></a></li>
			          <li><a class="submenu" href="indexapplicant.php?view=modifyproq"><i class="icon-file-alt"></i><span class="hidden-tablet"> Professional Qualifications</span></a></li>
			          <li><a class="submenu" href="indexapplicant.php?view=modifyeh"><i class="icon-file-alt"></i><span class="hidden-tablet"> Employment History</span></a></li>
			          <li><a class="submenu" href="indexapplicant.php?view=modifysk"><i class="icon-file-alt"></i><span class="hidden-tablet"> Skills</span></a></li>
			          <li><a class="submenu" href="indexapplicant.php?view=modifyareasofinterest"><i class="icon-file-alt"></i><span class="hidden-tablet"> Career discipline</span></a></li>
			          <li><a class="submenu" href="indexapplicant.php?view=modifyai"><i class="icon-file-alt"></i><span class="hidden-tablet"> Additional Information</span></a></li>
			          <li><a class="submenu" href="indexapplicant.php?view=modifydocs"><i class="icon-file-alt"></i><span class="hidden-tablet"> Documentation</span></a></li>
			          <li><a class="submenu" href="indexapplicant.php?view=modifyref"><i class="icon-file-alt"></i><span class="hidden-tablet"> References</span></a></li>
		            </ul>
		          </li>
			      <!--Reports-->
			      <li><a href="indexapplicant.php?view=tnc"><i class="icon-tasks"></i><span class="hidden-tablet"> Terms and Conditions</span></a></li>
			     <!--Staff Referrals-->
		
			      <!--Updates-->
			      <li><a href="indexapplicant.php?view=updates"><i class="icon-eye-open"></i><span class="hidden-tablet"> Updates</span></a></li>
		        </ul>
		      </div>
		  </div>
			<div id="icon" align="right"></div>
			<!-- end: Main Menu -->
			
		  <noscript>
				<div class="alert alert-block span10">
					<h4 class="alert-heading">Warning!</h4>
					<p>You need to have <a href="http://en.wikipedia.org/wiki/JavaScript" target="_blank">JavaScript</a> enabled to use this site.</p>
				</div>
			</noscript>
			
			<!-- start: Content -->
			<div id="content" class="span10">
			  <ul class="breadcrumb">
			    <li> <i class="icon-home"></i> <a href="indexapplicant.php">Home</a> <i class="icon-angle-right"></i> </li>
			    <li><a href="#"><?php echo $heading; ?></a></li>
		      </ul>
			  <div class="row-fluid">
			    <?php require_once $content; ?>
			 
		      </div>
			  <!--/row-->
             
               <div id="icons" >
			   <!-- <div id="openingicons">
			      <div align="center">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a href="indexapplicant.php?view=openings"><img src="images/button-icon-opening.png" alt="Mount kenya climbing" width="200" height="100"></a>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                 <a href="indexapplicant.php?view=modifypd"><img src="images/button-icon-personal-profile.png" alt="Mount kenya climbing" width="200" height="100"></a></div>
		        </div>-->
			  <!--  <div id="personalprofile">
			      <div align="center">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a href="indexapplicant.php?view=updates"><img src="images/button-icon-update.png" alt="Mount kenya climbing" width="200" height="100"></a>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                <a href="indexapplicant.php?view=tnc">  <img src="images/button-icon-terms.png" alt="Mount kenya climbing" width="200" height="100"></a></div>
		        </div>-->
		      </div>
		  </div>
			<!--/.fluid-container-->
	
			<!-- end: Content -->
		</div><!--/#content.span10-->
		</div><!--/fluid-row-->
		
	<div class="modal hide fade" id="myModal">
		<div class="modal-header">
			<button type="button" class="close" data-dismiss="modal">×</button>
			<h3>Settings</h3>
		</div>
		<div class="modal-body">
			<p>Here settings can be configured...</p>
		</div>
		<div class="modal-footer">
			<a href="#" class="btn" data-dismiss="modal">Close</a>
			<a href="#" class="btn btn-primary">Save changes</a>
		</div>
	</div>
	
	<div class="clearfix"></div>
	
	<footer>

		<p>
			<span style="text-align:left;float:left">&copy; 2015 <a href="http://centum.co.ke" alt="Bootstrap_Metro_Dashboard">Centum</a></span>
			
		</p>

	</footer>
	
	<!-- start: JavaScript-->

		<script src="js/jquery-1.9.1.min.js"></script>
	<script src="js/jquery-migrate-1.0.0.min.js"></script>
	
		<script src="js/jquery-ui-1.10.0.custom.min.js"></script>
	
		<script src="js/jquery.ui.touch-punch.js"></script>
	
		<script src="js/modernizr.js"></script>
	
		<script src="js/bootstrap.min.js"></script>
	
		<script src="js/jquery.cookie.js"></script>
	
		<script src='js/fullcalendar.min.js'></script>
	
		<script src='js/jquery.dataTables.min.js'></script>

		<script src="js/excanvas.js"></script>
	<script src="js/jquery.flot.js"></script>
	<script src="js/jquery.flot.pie.js"></script>
	<script src="js/jquery.flot.stack.js"></script>
	<script src="js/jquery.flot.resize.min.js"></script>
	
		<script src="js/jquery.chosen.min.js"></script>
	
		<script src="js/jquery.uniform.min.js"></script>
		
		<script src="js/jquery.cleditor.min.js"></script>
	
		<script src="js/jquery.noty.js"></script>
	
		<script src="js/jquery.elfinder.min.js"></script>
	
		<script src="js/jquery.raty.min.js"></script>
	
		<script src="js/jquery.iphone.toggle.js"></script>
	
		<script src="js/jquery.uploadify-3.1.min.js"></script>
	
		<script src="js/jquery.gritter.min.js"></script>
	
		<script src="js/jquery.imagesloaded.js"></script>
	
		<script src="js/jquery.masonry.min.js"></script>
	
		<script src="js/jquery.knob.modified.js"></script>
	
		<script src="js/jquery.sparkline.min.js"></script>
	
		<script src="js/counter.js"></script>
	
		<script src="js/retina.js"></script>

		<script src="js/custom.js"></script>
	<!-- end: JavaScript-->
	
</body>
</html>