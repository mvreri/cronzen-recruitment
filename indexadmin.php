<?php
require_once 'library/config2.php';
require_once 'functions.php';

checkUser();

$errorMessage = (isset($_GET['error']) && $_GET['error'] != '') ? $_GET['error'] : '&nbsp;';


$view = (isset($_GET['view']) && $_GET['view'] != '') ? $_GET['view'] : '';

switch ($view) {
	case 'list' :
		$heading = 'View Users';
		$content 	= 'admin_viewusers.php';		
		$pageTitle 	= 'Centum E-Recruitment | View Users';
		break;
		
	case 'moreonusers' :
		$heading = 'User Details';
		$content 	= 'admin_viewuserdetails.php';		
		$pageTitle 	= 'Centum E-Recruitment | View User Details';
		break;

	case 'add' :
		$heading = 'Add Users';
		$content 	= 'admin_addusers.php';		
		$pageTitle 	= 'Centum E-Recruitment | Add Users';
		break;
		
		
	case 'addvacancies' :
		$heading = 'Add New Vacancies';
		$content 	= 'admin_addvacancies.php';		
		$pageTitle 	= 'Centum E-Recruitment | Add Vacancy';
		break;
		
	case 'viewvacancies' :
		$heading = 'View Vacancies';
		$content 	= 'admin_viewvacanciesadded.php';		
		$pageTitle 	= 'Centum E-Recruitment | Add Vacancy';
		break;
		
		
	case 'viewapplicants' :
		$heading = 'View Applicants';
		$content 	= 'admin_viewapplicants.php';		
		$pageTitle 	= 'Centum E-Recruitment | View Applicants';
		break;
		
	case 'viewshortlistedapplicants' :
		$heading = 'View Shortlisted Applicants';
		$content 	= 'admin_viewshortlistedapplicants.php';		
		$pageTitle 	= 'Centum E-Recruitment | Shortlisted Applicants';
		break;


	case 'modifyu' :
		$heading = 'Modify User Details';
		$content 	= 'admin_editusers.php';		
		$pageTitle 	= 'Centum E-Recruitment | Modify User Details';
		break;
		
	case 'adddepts' :
		$heading = 'Add New Departments';
		$content 	= 'admin_adddepts.php';		
		$pageTitle 	= 'Centum E-Recruitment | Add Department';
		break;
		
	case 'addpositions' :
		$heading = 'Add New Positions';
		$content 	= 'admin_addpositions.php';		
		$pageTitle 	= 'Centum E-Recruitment | Add Positions';
		break;
		
	case 'addjobgroups' :
		$heading = 'Add Job Groups';
		$content 	= 'admin_addjobgroups.php';		
		$pageTitle 	= 'Centum E-Recruitment | Add Job Groups';
		break;
		
		
	case 'applications' :
		$heading = 'Application Submissions';
		$content 	= 'admin_applications.php';		
		$pageTitle 	= 'Centum E-Recruitment | Application Submissions';
		break;
		
	case 'summaryapplications' :
		$heading = 'Summary Application Submissions';
		$content 	= 'admin_summaryapplications.php';		
		$pageTitle 	= 'Centum E-Recruitment | Application Submissions';
		break;
		
	case 'expand' :
		$heading = 'Job Opportunity';
		$content 	= 'admin_viewopportunity.php';		
		$pageTitle 	= 'Centum E-Recruitment | Job Opportunity';
		break;
		
	case 'addpapers' :
		$heading = 'Professional Papers';
		$content 	= 'admin_addpapers.php';		
		$pageTitle 	= 'Centum E-Recruitment Setup| Add Professional Papers';
		break;
		
	case 'addareasofinterest' :
		$heading = 'Areas of Interest e.g. Finance, IT etc';
		$content 	= 'admin_addareasofinterest.php';		
		$pageTitle 	= 'Centum E-Recruitment Setup| Add Areas of Interest';
		break;
		
		
	case 'reports' :
		$heading = 'Reports';
		$content 	= 'admin_reports.php';		
		$pageTitle 	= 'Centum E-Recruitment | Reports';
		break;
		
	case 'updates' :
		$heading = 'Updates';
		$content 	= 'admin_updates.php';		
		$pageTitle 	= 'Centum E-Recruitment | Updates';
		break;
		
	case 'modify' :
		$heading = 'Edit User Details';
		$content 	= 'admin_modifyuser.php';		
		$pageTitle 	= 'Centum E-Recruitment | Edit User Details';
		break;
		
	case 'email' :
		$heading = 'Send Email';
		$content 	= 'admin_specifyemail.php';		
		$pageTitle 	= 'Centum E-Recruitment | Specify Email to be Sent to Applicants';
		break;
		
	case 'confirmrecepients' :
		$heading = 'Send Email';
		$content 	= 'admin_selectapplicantstoreceiveemail.php';		
		$pageTitle 	= 'Centum E-Recruitment | Specify Applicants to receive Email';
		break;
		
		
	case 'email2' :
		$heading = 'Send Email';
		$content 	= 'admin_specifyemail2.php';		
		$pageTitle 	= 'Centum E-Recruitment | Specify Email to be Sent to Applicants';
		break;
		
	case 'confirmrecepients2' :
		$heading = 'Send Email';
		$content 	= 'admin_selectapplicantstoreceiveemail2.php';		
		$pageTitle 	= 'Centum E-Recruitment | Specify Applicants to receive Email';
		break;
		
	case 'applicationstatus' :
		$heading = 'Apllicants\' Application Status';
		$content 	= 'admin_viewcompleteapplications.php';		
		$pageTitle 	= 'Centum E-Recruitment | Applicant Application Status';
		break;
		
	case 'logins' :
		$heading = 'User Logins';
		$content 	= 'admin_logins.php';		
		$pageTitle 	= 'Centum E-Recruitment | User Logins';
		break;

		
		
	default :
		$heading = 'Welcome';
		$content 	= 'admin_start.php';		
		$pageTitle 	= 'Centum E-Recruitment | Dashboard';
}

require_once 'template.php';
?>
