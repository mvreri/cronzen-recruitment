<?php
//destroy a seesion that may be acive in the current browser
session_start();
session_destroy();

require_once 'library/config2.php';
require_once 'functions.php';

$errorMessage = '&nbsp;';
if (isset($_POST['email'])) {
	$result = doLogin();
	
	if ($result != '') {
		$errorMessage = $result;
	}
}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Centum Careers</title>

<link href="css/centumcareers_style.css" rel="stylesheet" type="text/css" />
<script language="javascript" type="text/javascript">
function clearText(field)
{
    if (field.defaultValue == field.value) field.value = '';
    else if (field.value == '') field.value = field.defaultValue;
}
</script>
</head>
<body>

<div id="templatmeo_wrapper">

    <div id="templatemo_header"  style="color:#fff">
    
        <div id="site_title">
            <h1>
                <span>Centum Careers</span>
            </a></h1>
        </div> <!-- end of site_title -->
        
    </div> <!-- end of templatemo_header -->
    
  
    <div id="templatemo_menu">
    
        <ul>
            <li><a href="#">Home</a></li>
            <li><a href="#">FAQs</a></li>
  
        </ul>    	
    
    </div> <!-- end of templatemo_menu -->
    
    <div id="templatemo_content_wrapper">
    
        
        	
        	  <?php
if (!defined('WEB_ROOT')) {
	exit;
}

$rowsPerPage = 10;
$thedate = date('Y-m-d');
$sql = "SELECT a.id as id, a.refno as refno, a.positiontitle as title, a.closingdate as closingdate, d.department as dept  FROM c_applications a
inner join c_department d 
on d.id = a.deptid
WHERE a.closingdate<$thedate
		ORDER BY dateadded";
$result     = dbQuery(getPagingQuery($sql, $rowsPerPage));;
$pagingLink = getPagingLink($sql, $rowsPerPage);
?> 
<!--<script language="javascript">
function View(id)
{
	window.location.href = 'indexapplicant.php?view=expand&id=' + id;
}

</script>-->
 <table class="table table-striped table-bordered ">
						  <thead>
							  <tr>
								  <th>Job Ref. No.</th>
                                  <th>Job Title</th>
                                  <th>Department</th>
                                  <th>Closing Date</th>
                                  <th></th>
								  
							  </tr>
						  </thead> 
                          <tbody>  
<?php
if (dbNumrows($result)>0){
	while($row = dbFetchAssoc($result)) {
		extract($row);
?>
 
  <tr>
								<td><a href="javascript:View(<?php echo $id; ?>);"><?php echo $refno; ?></a></td>
								<td><?php echo $title; ?></td>
   <td><?php echo $dept; ?></td>
   <td><?php $closingdate = date_create("$closingdate"); echo date_format($closingdate,"d/m/Y");  ?></td>
    </tr><tr> 
   <td>To apply for the jobs click  <a href="registration.php?view=register">here</a> to register</td><td>. If you have registered <a href="index.php">login</a> to apply</td>
								
							</tr>
<?php
}// end while
}else{
?>
<tr><td colspan="5" align="center">There are no openings on the system yet</td></tr>
  <tr> 
  <?php }?>
   <td colspan="5" align="right"><?php 
   echo $pagingLink;
   ?></td>
 </tbody>
					  </table>

                
                <div class="body"></div>
            
            	<div class="box_bottom"><span></span></div>
      </div>
            
           

<div class="cleaner"></div>
    


</div> <!-- end of templatemo_wrapper -->

<div id="templatemo_footer_wrapper">

	<div id="templatemo_footer">

        Copyright © 2015 <a href="#">Centum-Careers</a>
	<!-- end of footer -->
</div> <!-- end of templatmeo_footer_wrapper -->
</html>