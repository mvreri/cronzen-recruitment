<?php
if (!defined('WEB_ROOT')) {
	exit;
}
$cid =$_SESSION['centum_user_id']; 
$sql = "SELECT a.id id, a.refno refno, a.positiontitle positiontitle, d.department department, a.dateadded dateadded FROM c_applications a inner join c_department d on a.deptid = d.id";

$result     = dbQuery($sql);


?>
<div class="row-fluid sortable">		
				<div class="box span12">
					<div class="box-header" data-original-title>
						<h2><i class="halflings-icon user"></i><span class="break"></span>Vacancies Added</h2>
						<div class="box-icon">
							<a href="#" class="btn-setting"><i class="halflings-icon wrench"></i></a>
							<a href="#" class="btn-minimize"><i class="halflings-icon chevron-up"></i></a>
							<a href="#" class="btn-close"><i class="halflings-icon remove"></i></a>
						</div>
					</div>
					<div class="box-content">
						<table class="table table-striped table-bordered bootstrap-datatable datatable">
						  <thead>
							  <tr>
								  <th>Ref. No. </th>
								  <th>Title </th>
								  <th>Department</th>
                                  <th>Date Added</th>
                                  <th></th>
                                  
                                  
                                  
								  
							  </tr>
						  </thead>   
						  <tbody>
                           <?php
if (dbNumRows($result) > 0) {
	$i = 0;
	
	while($row = dbFetchAssoc($result)) {
		extract($row);			
?> 
							<tr>
								<td><?php echo $refno; ?></td>
								<td class="center"><?php echo $positiontitle; ?></td>
								<td class="center"><?php echo $department; ?></td>
								<td class="center"><?php echo $dateadded; ?></td>
                                <td class="center"><input type="button" value="Zoom In" onClick="window.location.href='indexadmin.php?view=expand&id=<?php echo $id; ?>';"></td>
							</tr>
                            <?php
	} // end while


?>
  <?php
}else{
?>
<tr>
								<td colspan="4">NO vacancies have been added by any admin</td>
								
							</tr>

<?php
}
?>
							
							
						  </tbody>
					  </table>            
					</div>
				</div><!--/span-->
			
			</div><!--/row-->