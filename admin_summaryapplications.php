<?php
$cid =$_SESSION['centum_user_id']; 
$sql ="select count(l.id) applied, count(s.id) numbershortlisted,a.positiontitle as job, a.id as id from c_appliedlog l
left join c_applications a
on l.applicationid = a.id
left join shortlist s
on s.vacancyid = a.id
GROUP BY a.refno";

$result     = dbQuery($sql);

?>
<div class="row-fluid sortable">		
				<div class="box span12">
					<div class="box-header" data-original-title>
						<h2><i class="halflings-icon user"></i><span class="break"></span>Applications</h2>
						<div class="box-icon">
							<a href="#" class="btn-setting"><i class="halflings-icon wrench"></i></a>
							<a href="#" class="btn-minimize"><i class="halflings-icon chevron-up"></i></a>
							<a href="#" class="btn-close"><i class="halflings-icon remove"></i></a>
						</div>
					</div>
					<div class="box-content">
						<table class="table table-striped table-bordered bootstrap-datatable datatable">
						  <thead>
							  <tr>
								  <th>Position Advertised</th>
								  <th>Complete Applications </th>
								  <th>Shortlisted Applications</th>
								  
							  </tr>
						  </thead>   
						  <tbody>
                           <?php
if (dbNumRows($result) > 0) {	
	while($row = dbFetchAssoc($result)) {
		extract($row);			
?> 
							<tr>
								<td><a href="indexadmin.php?view=expand&id=<?php echo $id; ?>"><?php echo $job; ?></a></td>
								<td class="center"><?php echo $applied;  ?></td>
								<td class="center"><?php echo $numbershortlisted;  ?></td>
								
							</tr>
							
                            <?php
	} // end while


?>
  <?php
}else{
?>
							<tr>
								<td colspan="3">No positions exist the database</td>								
							</tr>
<?php
}
?>							
						  </tbody>
					  </table>            
					</div>
				</div><!--/span-->
			
			</div><!--/row-->