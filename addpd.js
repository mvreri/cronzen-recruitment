// JavaScript Document
function validateFormOnSubmit(frmaddpersonal) {
var reason = "";

  reason += validateFname(frmaddpersonal.fname);
  reason += validateFname(frmaddpersonal.sname);
  reason += validateDropG(frmaddpersonal.gender);
  reason += validateEmail(frmaddpersonal.email);
  reason += validatePhone(frmaddpersonal.phone);
  reason += validatePhone(frmaddpersonal.natidno);
  reason += validateDropD(frmaddpersonal.gender);
      
  if (reason != "") {
    alert("Some fields need correction:\n" + reason);
    return false;
  }

  return true;
}

function validateFname(fld){
	var alphaExp = /^[a-zA-Z]+$/;
	var error="";
	
	if (fld.value == ""){
		fld.style.background='Yellow';
		error = "Please check the First name and/or the surname.\n";
	} else if (!alphaExp.test(fld.value)){
		fld.style.background='Yellow';
		error = "The first name and surname may only have alphabetical letters.\n";
	} else {
		fld.style.background='White';
	}
	return error;
}

//prevent any empty fields
function validateEmpty(fld) {
    var error = "";
  
    if (fld.value.length == 0) {
        fld.style.background = 'Yellow'; 
        error = "The required field has not been filled in.\n"
    } else {
        fld.style.background = 'White';
    }
    return error;   
}


//validate email address, but first, trimming the whitespace from the address
function trim(s)
{
  return s.replace(/^\s+|\s+$/, '');
} 

function validateEmail(fld) {
    var error="";
    var tfld = trim(fld.value);  // value of field with whitespace trimmed off
    var emailFilter = /^[^@]+@[^@.]+\.[^@]*\w\w$/ ;
    var illegalChars= /[\(\)\<\>\,\;\:\\\"\[\]]/ ;
    
    if (fld.value == "") {
        fld.style.background = 'Yellow';
        error = "You didn't enter an email address.\n";
    } else if (!emailFilter.test(tfld)) {   //test email for illegal characters
        fld.style.background = 'Yellow';
        error = "Please enter a valid email address.\n";
    } else if (fld.value.match(illegalChars)) {
        fld.style.background = 'Yellow';
        error = "The email address contains illegal characters.\n";
    } else {
        fld.style.background = 'White';
    }
    return error;
}

function validatePhone(fld) {
    var error = "";
    var stripped = fld.value.replace(/[\(\)\.\-\ ]/g, '');     

   if (fld.value == "") {
        error = "You didn't enter a phone number.\n";
        fld.style.background = 'Yellow';
    } else if (isNaN(parseInt(stripped))) {
        error = "The phone number contains illegal characters.\n";
        fld.style.background = 'Yellow';
    } else if (!(stripped.length == 10)) {
        error = "The phone number is the wrong length.It needs to be 10 numbers long.\n";
        fld.style.background = 'Yellow';
    } 
    return error;
}

function validateDropG(fld) {
    var error = "";
  
    if ( document.frmaddpersonal.gender.selectedIndex == 0 ){
        fld.style.background = 'Yellow'; 
        error = "The gender field has not been specified.\n"
    } else {
        fld.style.background = 'White';
    }
    return error;   
}

function validateDropD(fld) {
    var error = "";
  
    if ( document.frmaddpersonal.domain.selectedIndex == 0 ){
        fld.style.background = 'Yellow'; 
        error = "You have not specified a gender.\n"
    } else {
        fld.style.background = 'White';
    }
    return error;   
}