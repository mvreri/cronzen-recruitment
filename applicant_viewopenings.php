date <?php
if (!defined('WEB_ROOT')) {
	exit;
}

$rowsPerPage = 10;
$thedate = date('Y-m-d');
$sql = "SELECT a.id as id, a.refno as refno, a.positiontitle as title, a.closingdate as closingdate, d.department as dept  FROM c_applications a
inner join c_department d 
on d.id = a.deptid
WHERE a.closingdate>'$thedate'
		ORDER BY dateadded";
$result     = dbQuery(getPagingQuery($sql, $rowsPerPage));
$pagingLink = getPagingLink($sql, $rowsPerPage);
?> 
<script language="javascript">
function View(id)
{
	window.location.href = 'indexapplicant.php?view=expand&id=' + id;
}

</script>
 <table class="table table-striped table-bordered ">
						  <thead>
							  <tr>
								  <th>Job Ref. No.</th>
                                  <th>Job Title</th>
                                  <th>Department</th>
                                  <th>Closing Date</th>
                                  <th></th>
								  
							  </tr>
						  </thead> 
                          <tbody>  
<?php
if (dbNumrows($result)>0){
	while($row = dbFetchAssoc($result)) {
		extract($row);
?>
 
  <tr>
								<td><a href="javascript:View(<?php echo $id; ?>);"><?php echo $refno; ?></a></td>
								<td><?php echo $title; ?></td>
   <td><?php echo $dept; ?></td>
   <td><?php $closingdate = date_create("$closingdate"); echo date_format($closingdate,"d/m/Y");  ?></td>
   <td><input type="button" value="View" onClick="window.location.href='indexapplicant.php?view=expand&id=<?php echo $id; ?>';"></td>
								
							</tr>
<?php
}// end while
}else{
?>
<tr><td colspan="5" align="center">There are no openings on the system yet</td></tr>
  <tr> 
  <?php }?>
   <td colspan="5" align="right"><?php 
   echo $pagingLink;
   ?></td>
 </tbody>
					  </table>

						  
						  
							
						 