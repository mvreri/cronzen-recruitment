<?php
$cid =$_SESSION['centum_user_id']; 
$sql ="select date(lastlogin) thedate, count(id) theusers from c_useract GROUP BY date(lastlogin)";

$result     = dbQuery($sql);

?>
<div class="row-fluid sortable">		
				<div class="box span12">
					<div class="box-header" data-original-title>
						<h2><i class="halflings-icon user"></i><span class="break"></span>Applications</h2>
						<div class="box-icon">
							<a href="#" class="btn-setting"><i class="halflings-icon wrench"></i></a>
							<a href="#" class="btn-minimize"><i class="halflings-icon chevron-up"></i></a>
							<a href="#" class="btn-close"><i class="halflings-icon remove"></i></a>
						</div>
					</div>
					<div class="box-content">
						<table class="table table-striped table-bordered bootstrap-datatable datatable">
						  <thead>
							  <tr>
								  <th>Login Date</th>
								  <th>Number of Logins </th>
								  
							  </tr>
						  </thead>   
						  <tbody>
                           <?php
if (dbNumRows($result) > 0) {	
	while($row = dbFetchAssoc($result)) {
		extract($row);			
?> 
							<tr>
								<td class="center"><?php echo $thedate;  ?></td>
								<td class="center"><?php echo $theusers;  ?></td>
								
							</tr>
							
                            <?php
	} // end while


?>
  <?php
}else{
?>
							<tr>
								<td colspan="2">No logins have been recorded yet</td>								
							</tr>
<?php
}
?>							
						  </tbody>
					  </table>            
					</div>
				</div><!--/span-->
			
			</div><!--/row-->