<?php
if (!defined('WEB_ROOT')) {
	exit;
}

$rowsPerPage = 10;
$sql = "SELECT u.id id, u.fname fname,u.sname sname, u.dateadded dateadded, u.email email, ai.name areaofinterest,u.isactive isactive from c_users u left join c_applicantareasofinterest a on a.userid = u.id 
left join c_areasofinterest ai on ai.id = a.areaofinterest order by a.id desc ";
$result     = dbQuery(getPagingQuery($sql, $rowsPerPage));;
$pagingLink = getPagingLink($sql, $rowsPerPage);
?> 
<script language="javascript">
function changeDetails(id)
{
	window.location.href = 'indexadmin.php?view=modify&userid=' + id;
}
function deleteUser(id)
{
	if (confirm('Are you sure you would like to delete this user?')) {
		window.location.href = 'process_admin.php?action=deluser&id=' + id;
	}
}

function ActivateUser(id)
{
	if (confirm('Are you sure you would like to activate this user?')) {
		window.location.href = 'process_admin.php?action=activateuser&id=' + id;
	}
}
</script>
<div class="row-fluid sortable">		
				<div class="box span12">
					<div class="box-header" data-original-title>
						<h2><i class="halflings-icon user"></i><span class="break"></span>Current System Users</h2>
						<div class="box-icon">
							<a href="#" class="btn-setting"><i class="halflings-icon wrench"></i></a>
							<a href="#" class="btn-minimize"><i class="halflings-icon chevron-up"></i></a>
							<a href="#" class="btn-close"><i class="halflings-icon remove"></i></a>
						</div>
					</div>
					<div class="box-content">
						<table class="table table-striped table-bordered bootstrap-datatable datatable">
						  <thead>
							  <tr>
								  <th>Username</th>
								  <th>Date registered</th>
								  <th>Email</th>
                                  <th>Area of Interest</th>
								  <th>Status</th>
                                  <th>&nbsp;</th>
								  
						    </tr>
						  </thead>   
						  <tbody>
                          <?php
if (dbNumrows($result)>0){
	while($row = dbFetchAssoc($result)) {
		extract($row);
?><input name="hiduid" type="hidden" id="hiduid" value="<?php echo $id; ?>">
							<tr>
								<td><a href="indexadmin.php?view=moreonusers&uid=<?php echo $id; ?>"><?php echo $fname.' '. $sname; ?></a></td>
								<td class="center"><?php echo $dateadded;?></td>
								<td class="center"><?php echo $email ?></td>
                                <td class="center"><?php echo $areaofinterest ?></td>
								<td class="center"><?php if ($isactive==0){ ?>
									<span class="label label-fail">Not Active</span><?php } else if ($isactive==1){ ?><span class="label label-success">Active</span><?php } else { ?><span class="label label-fail">User removed from system</span><?php } ?>
								</td>
                                <td><!--a href="javascript:changeDetails(<?php echo $id; ?>);"><i class="halflings-icon wrench"></i></a-->&nbsp;&nbsp;&nbsp;&nbsp;<a href="javascript:deleteUser(<?php echo $id; ?>);"><i class="halflings-icon remove"></i></a>&nbsp;&nbsp;&nbsp;&nbsp;
                                <a href="javascript:ActivateUser(<?php echo $id; ?>);"><i class="halflings-icon play"></i></a></td>
								
							</tr>
                            <?php
}// end while
}else{
?>
<tr><td colspan="6" align="center">There are no applicants for vacancies, yet</td></tr>
<?php }?>
							
						  </tbody>
					  </table>            
					</div>
				</div><!--/span-->
			
			</div><!--/row-->