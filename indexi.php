<?php
//destroy a session that may be active in the current browser
session_start();
session_destroy();

require_once 'library/config2.php';
require_once 'functions.php';

$errorMessage = '&nbsp;';
if (isset($_POST['email'])) {
	$result = doLogin();
	
	if ($result != '') {
		$errorMessage = $result;
	}
}
if (isset($_GET['ue'])) {
	$ue = $_GET['ue'];
	$queryString = "ue=$ue";
} else {
	$ue= '';	
	$queryString = '';
}
$res = mysql_query("UPDATE c_users SET isactive=1 WHERE email=$ue");
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Centum Careers</title>

<link href="css/centumcareers_style.css" rel="stylesheet" type="text/css" />
<script language="javascript" type="text/javascript">
function clearText(field)
{
    if (field.defaultValue == field.value) field.value = '';
    else if (field.value == '') field.value = field.defaultValue;
}
</script>
</head>
<body>

<div id="templatmeo_wrapper">

    <div id="templatemo_header"  style="color:#fff">
    
        <!--<div id="site_title"> -->
           
                <span> <img src="images/Centum-E-Registration-logo__.png" alt="logo" style="width:200px; height:98px"></span>
       <!-- </div> <!-- end of site_title -->
        
    </div> <!-- end of templatemo_header -->
    
  
    <div id="templatemo_menu">
    
        <ul>
            <li><a href="#">Home</a></li>
            <li><a href="#">FAQs</a></li>
  
        </ul>    	
    
    </div> <!-- end of templatemo_menu -->
	
	<p><font color="red">Login or register to be able to access our intern portal</font></p>
    
    <div id="templatemo_content_wrapper">
    
    	<div id="templatemo_left_sidebar">
        
        	<div class="templatemo_box">
            	<h2><span></span>Register</h2>
                
                <div class="body">
                    To view and apply for jobs, you need to register first. Register <a href="viewapplications.php">here</a>
                </div>
            
            	<div class="box_bottom"><span></span></div>
            </div>
            
           
        </div> <!-- end of left_sidebar -->
        
        <div id="templatemo_content">
        
			<!-- <div class="templatemo_box">
            	<h2><span></span>Centum Careers</h2>
                
                <div class="body">
                    
                    <p>To login, you need to enter your email address and password</p>
                  
              </div>
            
           	  <div class="box_bottom"><span></span></div>
            </div> -->
            
            <div class="templatemo_box">
            	<h2><span></span>Register</h2>
                
                <div class="body">
                
                <ul class="side_menu">
                 To view openings and apply for jobs, register <a href="registration.php?view=register">here</a>
                 
                                    
                    </ul>
                    
                    
                  
                  
                </div>
            
            	<div class="box_bottom"><span></span></div>
            </div>
        	
        </div> <!-- end of templatemo_content -->
        
       	<div id="templatemo_right_sidebar">
        
        	<div class="templatemo_box">
            	<h2><span></span>Login</h2>
                
                <div class="body">
                    
                   <?php echo '<span style="color:#FF0000;text-align:center;">'.$errorMessage.'</span>'; ?>
					
					
                
                <form class="form-horizontal" method="post">
				<table width="100%">
				<tr><td colspan="2"><td></tr>
				<tr><td>Email</td><td><input name="email" id="email" type="text" placeholder="type email address"/></td></tr>
				<tr><td>Password</td><td><input name="passw" id="passw" type="password" placeholder="type password"/></td></tr>
				<tr><td></td><td><button type="submit" class="btn">Sign in</button></td></tr>
				<tr><td colspan="2"><h3>Forgot Password?</h3>
					<p>
						No problem, <a href="registration.php?view=forgotpassword">click here</a> to get a new password.
					</p>	
					</td></tr>
				<tr><td colspan="2">Not registered? <a href="registration.php?view=register">Sign Up</a></td></tr>
				</table>
				</form>
                  
                </div>
            
            	<div class="box_bottom"><span></span></div>
            </div>
            
           
      </div> <!-- end of right_sidebar -->
        
        <div class="cleaner"></div>
    
    </div> <!-- end of templatemo_content_wrapper -->

</div> <!-- end of templatemo_wrapper -->

<div id="templatemo_footer_wrapper">

	<div id="templatemo_footer">

        Copyright © 2015 <a href="#">Centum-Careers</a>
	<!-- end of footer -->
</div> <!-- end of templatmeo_footer_wrapper -->
</html>