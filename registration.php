<?php
require_once 'library/config2.php';
require_once 'functions.php';

//checkUser();

$errorMessage = (isset($_GET['error']) && $_GET['error'] != '') ? $_GET['error'] : '&nbsp;';


$view = (isset($_GET['view']) && $_GET['view'] != '') ? $_GET['view'] : '';

switch ($view) {
	case 'tnc' :
		$heading = 'E-Recruitment Terms and Conditions';
		$content 	= 'ext_tnc.php';		
		$pageTitle 	= 'Centum E-Recruitment | View Users';
		break;
		
	case 'register' :
		$heading = 'Register As User';
		$content 	= 'register.php';		
		$pageTitle 	= 'Centum E-Recruitment | User Registration';
		break;
		
	case 'afterregistration' :
		$heading = 'Register As User';
		$content 	= 'after_registration.php';		
		$pageTitle 	= 'Centum E-Recruitment | Confirm registration';
		break;
		
	case 'vacancies' :
		$heading = 'View Vacancies';
		$content 	= 'vacancies.php';		
		$pageTitle 	= 'Centum E-Recruitment | View Vacancies';
		break;
	
	case 'login' :
		$heading = 'Centum E-Recruitment Login';
		$content 	= 'login.php';		
		$pageTitle 	= 'Centum E-Recruitment | Login';
		break;
		
	case 'forgotpassword' :
		$heading = 'Centum E-Recruitment Login';
		$content 	= 'forgotpassword.php';		
		$pageTitle 	= 'Centum E-Recruitment | Forgot Password';
		break;
				
	default :
		$heading = 'Centum E-Recruitment Login';
		$content 	= 'login.php';		
		$pageTitle 	= 'Centum E-Recruitment | Login';
}

require_once 'template_external.php';
?>