<?php
require_once 'include/config.php';
$cid =$_SESSION['centum_user_id'];
  echo '<span style="color:#FF0000;text-align:center;">'.$errorMessage.'</span>'; ?>
  
  <form name="frmref" id="frmref" action="process_applicant.php?action=addreferences" method="post">
<table width="75%" align="center">
<input name="hiduid" type="hidden" id="hiduid" value="<?php echo $cid; ?>">
<tr>
  <td colspan="2"><b>References</b>
  </tr>
  <tr>
  <td>Designation *</td><td><select name="designation">
  <option value="Ms.">Ms.</option>
  <option value="Mrs.">Mrs.</option>
  <option value="Mr.">Mr.</option>
  <option value="Dr.">Dr.</option>
  <option value="Prof.">Prof.</option>
  <option value="Pst.">Pst.</option>
  <option value="Rev.">Rev.</option></select></td></tr>
<tr>
<td>Name *</td><td><input type="text" id="refname" name="refname" placeholder="Reference Name" required /></td>
</tr>
<tr>
  <td>Phone No. (Start with your country code, 254xxxxxxxxx) *</td><td><input type="number" id="refphone" name="refphone" placeholder="Reference Phone" title="start with your country code, 254xxxxxxxxx" required /></td></tr>
<tr>
<tr>
  <td>Alternative Phone No. *</td><td><input type="text" id="refphonealt" name="refphonealt" placeholder="Reference Other Phone" required /></td></tr>
<tr>
  <td>Email Address *</td><td><input type="text" id="refemail" name="refemail" placeholder="Reference Email" required /></td></tr>
  <tr>
  <td>Relationship *</td><td><select name="relationship" required/>
  <option value="Academic">Academic Mentor</option>
  <option value="Social">Social Mentor</option>
  <option value="Work">Direct Supervisor</option></select></td></tr>
  



<tr><td></td><td><input type="button" value="Back" onClick="window.location.href='indexapplicant.php?view=modifyref';"> <input type="Submit" value="Save" ></td></tr>
</table>
</form>