<?php
if (!defined('WEB_ROOT')) {
	exit;
}
$cid =$_SESSION['centum_user_id'];

$sql = "SELECT * FROM c_education WHERE userid=$cid";
$result     = dbQuery($sql);
echo '<span style="color:#FF0000;text-align:center;">'.$errorMessage.'</span>';
?>
<script language="javascript">
function deletehschool(id)
{
	if (confirm('Are you sure you would like to delete this High School?')) {
		window.location.href = 'process_applicant.php?action=delhschool&id=' + id;
	}
}
</script>
<div class="row-fluid sortable">
				<div class="box span12">
					<div class="box-header">
						<h2><i class="halflings-icon align-justify"></i><span class="break"></span>Education Background | High School | Two of Ten</h2>
						<div class="box-icon">
							<a href="#" class="btn-setting"><i class="halflings-icon wrench"></i></a>
							<a href="#" class="btn-minimize"><i class="halflings-icon chevron-up"></i></a>
							<a href="#" class="btn-close"><i class="halflings-icon remove"></i></a>
						</div>
					</div>
					<div class="box-content">
						<table class="table">
							  <thead>
								  <tr>
									  <th>School</th>
									  <th>Year of completion</th>
									  <th>Exam</th>
									  <th>Grade</th>
									  <th>&nbsp;</th>                                          
								  </tr>
							  </thead>   
							  <tbody>
                              <?php
if (dbNumRows($result) > 0) {
	$i = 0;
	
	while($row = dbFetchAssoc($result)) {
		extract($row);			
?> 
								<tr>
									<td><?php echo $hschool; ?></td>
									<td class="center"><?php echo $year_cleared; ?></td>
									<td class="center"><?php echo $exam; ?></td>
									<td class="center"><?php echo $hschoolgrade; ?></td>
									<td class="center"><a href="#" class="btn-setting"><i class="halflings-icon wrench"></i></a> <a href="javascript:deletehschool(<?php echo $id; ?>);"><i class="halflings-icon remove"></i></a></td>                                       
								</tr>
                                <?php
	} // end while


?>
  <?php
}else{
?>
<tr>
									<td colspan="4">You haven't added any High School to your profile yet</td>
									                                        
								</tr>
<?php
}
?>  

 <tr>
									<td colspan="4" align="right"><input name="btnhs" type="button" id="btnhs" value="Add High School" onClick="window.location.href='indexapplicant.php?view=addhighschool';"></td>
									                                        
								</tr>
                                <tr>
									<td colspan="4"></td>
									                                        
								</tr>                      
<tr>
									<td colspan="4" align="right"> <input name="back" type="button" id="back" value="BACK" onClick="window.location.href='indexapplicant.php?view=modifypd';">  <input name="next" type="button" id="next" value="NEXT" onClick="window.location.href='indexapplicant.php?view=modifytertiary';"></td></tr>
									
									                                        
								                              
							  </tbody>
						 </table>  
						      
					</div>
				</div><!--/span-->
				
				
			</div><!--/row-->