-- phpMyAdmin SQL Dump
-- version 4.1.6
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Aug 15, 2015 at 05:32 PM
-- Server version: 5.6.16
-- PHP Version: 5.5.9

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `centumdb`
--

-- --------------------------------------------------------

--
-- Table structure for table `applicationtrack`
--

CREATE TABLE IF NOT EXISTS `applicationtrack` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `userid` int(11) DEFAULT NULL,
  `haspersonaldetails` int(1) DEFAULT NULL,
  `haseducation` int(1) DEFAULT NULL,
  `hasareaofinterest` int(1) DEFAULT NULL,
  `hasskills` int(1) DEFAULT NULL,
  `haslanguages` int(1) DEFAULT NULL,
  `hasdocuments` int(1) DEFAULT NULL,
  `hasreferences` int(1) DEFAULT NULL,
  `dateupdated` date DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=5 ;

--
-- Dumping data for table `applicationtrack`
--

INSERT INTO `applicationtrack` (`id`, `userid`, `haspersonaldetails`, `haseducation`, `hasareaofinterest`, `hasskills`, `haslanguages`, `hasdocuments`, `hasreferences`, `dateupdated`) VALUES
(2, 9, 1, 0, 1, 0, 1, 1, 1, '2015-01-15'),
(3, 22, 1, 1, 0, 0, 0, 0, 0, '2014-12-18'),
(4, 23, 1, 1, 1, 1, 1, 1, 0, '2015-03-07');

-- --------------------------------------------------------

--
-- Table structure for table `c_additionalinfo`
--

CREATE TABLE IF NOT EXISTS `c_additionalinfo` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `userid` int(11) DEFAULT NULL,
  `CV` text NOT NULL,
  `adddocs` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=12 ;

--
-- Dumping data for table `c_additionalinfo`
--

INSERT INTO `c_additionalinfo` (`id`, `userid`, `CV`, `adddocs`) VALUES
(1, NULL, '', ''),
(2, NULL, '', ''),
(3, NULL, '', ''),
(4, NULL, '', ''),
(5, NULL, 'Capture.JPG', ''),
(6, NULL, 'Use of computing terms.docx', ''),
(7, NULL, 'BDM_Dynamics.png', ''),
(8, 9, 'akn.txt', ''),
(10, 9, 'atad.csv', 'bar-data.csv'),
(11, 23, 'db_highcharts.sql', 'tmhOAuth-master.zip');

-- --------------------------------------------------------

--
-- Table structure for table `c_applicantareasofinterest`
--

CREATE TABLE IF NOT EXISTS `c_applicantareasofinterest` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `areaofinterest` varchar(30) DEFAULT NULL,
  `userid` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=5 ;

--
-- Dumping data for table `c_applicantareasofinterest`
--

INSERT INTO `c_applicantareasofinterest` (`id`, `areaofinterest`, `userid`) VALUES
(2, '1', 8),
(3, '1', 9),
(4, '1', 23);

-- --------------------------------------------------------

--
-- Table structure for table `c_applicantprofqual`
--

CREATE TABLE IF NOT EXISTS `c_applicantprofqual` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `userid` int(11) DEFAULT NULL,
  `courseid` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=5 ;

--
-- Dumping data for table `c_applicantprofqual`
--

INSERT INTO `c_applicantprofqual` (`id`, `userid`, `courseid`) VALUES
(2, 22, 1),
(3, 22, 1),
(4, 23, 1);

-- --------------------------------------------------------

--
-- Table structure for table `c_applications`
--

CREATE TABLE IF NOT EXISTS `c_applications` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `refno` varchar(20) NOT NULL,
  `positiontitle` varchar(50) NOT NULL,
  `positionid` int(11) DEFAULT NULL,
  `deptid` int(11) DEFAULT NULL,
  `reportingto` int(11) DEFAULT NULL,
  `closingdate` date DEFAULT NULL,
  `jd` text,
  `dateadded` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `addedby` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `c_applications`
--

INSERT INTO `c_applications` (`id`, `refno`, `positiontitle`, `positionid`, `deptid`, `reportingto`, `closingdate`, `jd`, `dateadded`, `addedby`) VALUES
(1, 'werwer', '', 0, 1, 0, '0000-00-00', 'werwer', '2014-11-30 23:32:39', 0),
(2, 'intern/123/2015', 'IT Intern', 0, 3, 0, '0000-00-00', 'asdsadasdasd', '2014-12-15 00:17:09', 0),
(3, 'FC/Cent/123', 'Finance Contractor', 0, 1, 0, '0000-00-00', 'Finance Contractor stuff Finance Contractor stuff Finance Contractor stuff Finance Contractor stuff Finance Contractor stuff Finance Contractor stuff', '2014-12-17 03:18:04', 8);

-- --------------------------------------------------------

--
-- Table structure for table `c_appliedlog`
--

CREATE TABLE IF NOT EXISTS `c_appliedlog` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `userid` int(11) DEFAULT NULL,
  `applicationid` int(11) DEFAULT NULL,
  `dateapplied` datetime DEFAULT NULL,
  `applicationstatus` int(1) DEFAULT NULL,
  `isshortlisted` int(1) NOT NULL DEFAULT '0',
  `isemailsent` int(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=8 ;

--
-- Dumping data for table `c_appliedlog`
--

INSERT INTO `c_appliedlog` (`id`, `userid`, `applicationid`, `dateapplied`, `applicationstatus`, `isshortlisted`, `isemailsent`) VALUES
(1, NULL, 0, NULL, NULL, 0, 0),
(2, 9, 1, '2014-12-02 06:36:54', 1, 1, 1),
(3, 8, 3, '2014-12-15 00:49:38', 1, 0, 0),
(4, 9, 3, '2014-12-18 01:50:24', 1, 0, 1),
(5, 22, 1, '2014-12-18 08:54:16', 1, 0, 0),
(6, 22, 2, '2014-12-18 09:00:14', 1, 0, 0),
(7, 22, 3, '2014-12-18 09:07:49', 1, 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `c_areasofinterest`
--

CREATE TABLE IF NOT EXISTS `c_areasofinterest` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(40) DEFAULT NULL,
  `deptid` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `c_areasofinterest`
--

INSERT INTO `c_areasofinterest` (`id`, `name`, `deptid`) VALUES
(1, 'Financial Modelling', 1);

-- --------------------------------------------------------

--
-- Table structure for table `c_department`
--

CREATE TABLE IF NOT EXISTS `c_department` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `department` varchar(30) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `c_department`
--

INSERT INTO `c_department` (`id`, `department`) VALUES
(1, 'Finance'),
(2, 'Human Resources'),
(3, 'IT');

-- --------------------------------------------------------

--
-- Table structure for table `c_education`
--

CREATE TABLE IF NOT EXISTS `c_education` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `userid` int(11) DEFAULT NULL,
  `hschool` varchar(50) DEFAULT NULL,
  `exam` varchar(10) DEFAULT NULL,
  `hschoolgrade` varchar(3) NOT NULL,
  `dateupdated` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=10 ;

--
-- Dumping data for table `c_education`
--

INSERT INTO `c_education` (`id`, `userid`, `hschool`, `exam`, `hschoolgrade`, `dateupdated`) VALUES
(6, 9, 'Juja Boys', 'KCSE', 'A', NULL),
(8, 22, 'lenana School', 'KCSE', '', NULL),
(9, 23, 'Strathmore School', 'IGCSE', 'A*', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `c_education_tertiary`
--

CREATE TABLE IF NOT EXISTS `c_education_tertiary` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `userid` int(11) DEFAULT NULL,
  `institutionname` varchar(30) DEFAULT NULL,
  `datefrom` date DEFAULT NULL,
  `dateto` date DEFAULT NULL,
  `GRADE` varchar(15) DEFAULT NULL,
  `degree` text NOT NULL,
  `dateupdated` date DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=8 ;

--
-- Dumping data for table `c_education_tertiary`
--

INSERT INTO `c_education_tertiary` (`id`, `userid`, `institutionname`, `datefrom`, `dateto`, `GRADE`, `degree`, `dateupdated`) VALUES
(6, 9, 'JKUAT', '2014-12-01', '2014-12-03', 'A', 'Diploma', '2014-12-19'),
(7, 23, 'UON', '2015-03-01', '2015-03-04', 'Second Upper', 'Bachelors Degree', '2015-03-07');

-- --------------------------------------------------------

--
-- Table structure for table `c_emailmessages`
--

CREATE TABLE IF NOT EXISTS `c_emailmessages` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `email` text,
  `issent` int(1) DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=13 ;

--
-- Dumping data for table `c_emailmessages`
--

INSERT INTO `c_emailmessages` (`id`, `email`, `issent`) VALUES
(1, 'We are pleased to invite you to our offices for an interview', 1),
(2, 'We are pleased to invite you to our offices for an interview', 1),
(3, 'We are pleased to invite you to our offices for an interview', 1),
(4, 'ghfhfgfhfhgfghfhgfg', 1),
(5, 'ghfhfgfhfhgfghfhgfg', 1),
(6, 'ghfhfgfhfhgfghfhgfgdfgdgdfg', 1),
(7, 'sdfsdfsdfsdfsdfsdfsd', 1),
(8, 'sdfsdfsdfsdfsdfsdfrefrfesrefgregergegrd', 2),
(9, 'xfvdgfdgfdg', 0),
(10, 'sdfsdfsdfsdfsdfsfsfsdfsdfsf sdfsdfsdfdfdsfds', 1),
(11, 'dfgdfgdf dbdf df dff d fd df&nbsp;', 1),
(12, 'abc123343242', 0);

-- --------------------------------------------------------

--
-- Table structure for table `c_employment`
--

CREATE TABLE IF NOT EXISTS `c_employment` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `userid` int(11) DEFAULT NULL,
  `empl1` varchar(50) DEFAULT NULL,
  `empl1from` date DEFAULT NULL,
  `empl1to` date DEFAULT NULL,
  `empl1location` varchar(20) DEFAULT NULL,
  `empl1jt` varchar(20) NOT NULL,
  `empl1jd` text,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=7 ;

--
-- Dumping data for table `c_employment`
--

INSERT INTO `c_employment` (`id`, `userid`, `empl1`, `empl1from`, `empl1to`, `empl1location`, `empl1jt`, `empl1jd`) VALUES
(1, NULL, 'a', '0000-00-00', '0000-00-00', 'asas', 'aas', 'dasd'),
(3, 9, 'A', '0000-00-00', '0000-00-00', 'Nairobi', 'HR', 'job Description here'),
(4, 9, 'D', '0000-00-00', '0000-00-00', 'Location Here', 'Title here', 'JD'),
(5, 9, 'Emp', '0000-00-00', '0000-00-00', 'JLJD', 'JT', 'JD'),
(6, 9, 'R', '2014-11-02', '2014-12-18', 'JLoc', 'JTitle', 'JDesc');

-- --------------------------------------------------------

--
-- Table structure for table `c_exams`
--

CREATE TABLE IF NOT EXISTS `c_exams` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `exam` varchar(20) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `c_jobgroup`
--

CREATE TABLE IF NOT EXISTS `c_jobgroup` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `jobgroup` varchar(30) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `c_jobgroup`
--

INSERT INTO `c_jobgroup` (`id`, `jobgroup`) VALUES
(1, 'A'),
(2, 'A'),
(3, 'B');

-- --------------------------------------------------------

--
-- Table structure for table `c_languages`
--

CREATE TABLE IF NOT EXISTS `c_languages` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `userid` int(11) DEFAULT NULL,
  `language` varchar(20) DEFAULT NULL,
  `fluency` int(1) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=6 ;

--
-- Dumping data for table `c_languages`
--

INSERT INTO `c_languages` (`id`, `userid`, `language`, `fluency`) VALUES
(3, 9, 'Zulu', 1),
(4, 9, 'English', 3),
(5, 23, 'English', 3);

-- --------------------------------------------------------

--
-- Table structure for table `c_positions`
--

CREATE TABLE IF NOT EXISTS `c_positions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(30) DEFAULT NULL,
  `jobgroup` int(2) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `c_positions`
--

INSERT INTO `c_positions` (`id`, `name`, `jobgroup`) VALUES
(1, 'Head of Finance', 1),
(2, 'Head of HR', 1),
(3, 'Junior IT Officer', 3);

-- --------------------------------------------------------

--
-- Table structure for table `c_proqualifications`
--

CREATE TABLE IF NOT EXISTS `c_proqualifications` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `courses` varchar(30) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `c_proqualifications`
--

INSERT INTO `c_proqualifications` (`id`, `courses`) VALUES
(1, 'CPA');

-- --------------------------------------------------------

--
-- Table structure for table `c_referees`
--

CREATE TABLE IF NOT EXISTS `c_referees` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `userid` int(11) DEFAULT NULL,
  `designation` varchar(10) NOT NULL,
  `ref1name` varchar(30) DEFAULT NULL,
  `ref1phone` varchar(30) DEFAULT NULL,
  `ref1phonealt` int(11) NOT NULL,
  `ref1email` varchar(40) DEFAULT NULL,
  `relationship` varchar(20) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=14 ;

--
-- Dumping data for table `c_referees`
--

INSERT INTO `c_referees` (`id`, `userid`, `designation`, `ref1name`, `ref1phone`, `ref1phonealt`, `ref1email`, `relationship`) VALUES
(8, 9, 'Ms.', 'J', '021456987', 0, 'a@b.c', 'Uncle'),
(10, 9, 'Prof.', 'R', '4345345', 0, 'e@d.n', 'Cousin'),
(13, 23, 'Pst.', 'er', '232323232', 232323232, 'a@d.v', 'e');

-- --------------------------------------------------------

--
-- Table structure for table `c_skills`
--

CREATE TABLE IF NOT EXISTS `c_skills` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `userid` int(11) DEFAULT NULL,
  `skill` varchar(20) DEFAULT NULL,
  `isactive` int(11) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=6 ;

--
-- Dumping data for table `c_skills`
--

INSERT INTO `c_skills` (`id`, `userid`, `skill`, `isactive`) VALUES
(2, 9, 'Dancing', 1),
(4, 9, 'Swimming', 1),
(5, 23, 'Dancing', 1);

-- --------------------------------------------------------

--
-- Table structure for table `c_useract`
--

CREATE TABLE IF NOT EXISTS `c_useract` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `UID` int(11) DEFAULT NULL,
  `LastLogin` datetime DEFAULT NULL,
  `SessionID` text,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=6 ;

--
-- Dumping data for table `c_useract`
--

INSERT INTO `c_useract` (`ID`, `UID`, `LastLogin`, `SessionID`) VALUES
(1, 0, '2014-12-01 21:20:46', 'd3orug01ma07mbfdidslk2qbc6'),
(2, 0, '2014-12-01 21:20:53', 'd3orug01ma07mbfdidslk2qbc6'),
(3, 0, '2014-12-01 21:21:01', 'd3orug01ma07mbfdidslk2qbc6'),
(4, 0, '2014-12-01 21:21:42', 'd3orug01ma07mbfdidslk2qbc6'),
(5, 0, '2014-12-01 21:21:54', 'd3orug01ma07mbfdidslk2qbc6');

-- --------------------------------------------------------

--
-- Table structure for table `c_users`
--

CREATE TABLE IF NOT EXISTS `c_users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `fname` varchar(15) DEFAULT NULL,
  `mname` varchar(15) DEFAULT NULL,
  `sname` varchar(15) DEFAULT NULL,
  `email` varchar(45) DEFAULT NULL,
  `dob` date DEFAULT NULL,
  `gender` varchar(10) DEFAULT NULL,
  `nationality` varchar(20) NOT NULL,
  `natidno` varchar(10) DEFAULT NULL,
  `address` varchar(50) DEFAULT NULL,
  `phone` int(15) DEFAULT NULL,
  `usertype` int(11) NOT NULL,
  `password` varchar(55) DEFAULT NULL,
  `isactive` int(11) NOT NULL DEFAULT '0',
  `dateadded` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=24 ;

--
-- Dumping data for table `c_users`
--

INSERT INTO `c_users` (`id`, `fname`, `mname`, `sname`, `email`, `dob`, `gender`, `nationality`, `natidno`, `address`, `phone`, `usertype`, `password`, `isactive`, `dateadded`) VALUES
(3, 'werw', 'tyryrt', 'rtyrty', 'e', '0000-00-00', 'Male', 'j', '435345', '', 45354354, 0, NULL, 0, '2014-12-01 00:07:13'),
(4, 'qweqwe', 'qweqe', 'wqeqwe', 'e', '0000-00-00', 'Male', 'g', '23424', '', 2147483647, 0, NULL, 1, '2014-12-01 00:08:15'),
(5, '', '', '', 'e', '0000-00-00', '', '', '', '', 0, 0, NULL, 0, '2014-12-01 00:24:46'),
(6, 'Centum', '', 'Admin', 'admin@centum.co.ke', '0000-00-00', '3', '', '0000001', 'Life House HQ', 110011, 0, 'admin', 1, '2014-12-01 05:46:41'),
(7, 'Admin2', '', 'Centum', '2admin@centum.co.ke', '0000-00-00', '3', '', '21212121', 'Life House', 123654789, 0, 'admin2', 1, '2014-12-01 05:49:43'),
(8, 'centum', '', 'superadmin', 'super@centum.co.ke', '0000-00-00', '3', '', '12121212', 'LH', 72727272, 1, 'abc123', 1, '2014-12-01 06:10:37'),
(9, 'Roy', 'new', 'applicant', 'murerintwiga@gmail.com', '2013-07-03', 'Male', 'Kenyan', '12211221', '123werw', 2, 2, 'abc123', 1, '2014-12-02 05:22:06'),
(10, 'John', NULL, 'Doe', 'jd@j.d', NULL, NULL, '', NULL, NULL, NULL, 2, 'jd', 0, '2014-12-03 23:27:32'),
(11, 'r', NULL, 'r', 'r@r.r', NULL, NULL, '', NULL, NULL, NULL, 2, 'rr', 1, '2014-12-03 23:31:30'),
(12, 'w', NULL, 'e', 'w@w.n', NULL, NULL, '', NULL, NULL, NULL, 2, 'wn', 0, '2014-12-03 23:35:02'),
(13, 'ewr', NULL, 'wre', 'wer@f.m', NULL, NULL, '', NULL, NULL, NULL, 2, 'df', 0, '2014-12-03 23:36:49'),
(14, 'qwe', NULL, 'eqw', 'qe@e.m', NULL, NULL, '', NULL, NULL, NULL, 2, 'asdasd', 0, '2014-12-03 23:37:17'),
(15, 'asd', NULL, 'asd', 'asd@f.g', NULL, NULL, '', NULL, NULL, NULL, 2, 'sdfs', 0, '2014-12-03 23:38:09'),
(16, 'sedf', NULL, 'sdf', 'sdf@dxfg.l', NULL, NULL, '', NULL, NULL, NULL, 2, 'dfgdfg', 0, '2014-12-03 23:40:06'),
(17, 'wer', NULL, 'wer', 'd@d.k', NULL, NULL, '', NULL, NULL, NULL, 2, 'sdfsfd', 0, '2014-12-03 23:41:15'),
(18, '', '', '', 'e', '0000-00-00', 'Male', '', '', '', 0, 0, NULL, 1, '2014-12-14 19:03:36'),
(19, '', '', '', 'e', '0000-00-00', 'Male', '', '', '', 0, 0, NULL, 0, '2014-12-14 20:02:09'),
(20, '', '', '', 'e', '0000-00-00', 'Male', 'Kenyan', '24787878', '', 12345787, 0, NULL, 0, '2014-12-14 23:06:13'),
(21, 'a', NULL, 'b', 'c@f.m', NULL, NULL, '', '123123', NULL, 423234, 1, 'abc123', 1, '2014-12-18 01:58:10'),
(22, 'Collins', 'cyril', 'Waswa', 'collinswaswa@gmail.com', '0000-00-00', 'Male', 'kenyan', '29388383', 'jfjf', 724457624, 2, 'raboli', 1, '2014-12-18 08:53:44'),
(23, 'mur', 'eri', 'ntw', 'mur@gmail.com', '2009-11-03', 'Male', 'Kenyan', '23456789', '213 nrb', 725413458, 2, '123456', 1, '2015-03-07 12:49:44');

-- --------------------------------------------------------

--
-- Table structure for table `c_usertype`
--

CREATE TABLE IF NOT EXISTS `c_usertype` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(20) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `c_usertype`
--

INSERT INTO `c_usertype` (`id`, `name`) VALUES
(1, 'admin'),
(2, 'applicant');

-- --------------------------------------------------------

--
-- Table structure for table `shortlist`
--

CREATE TABLE IF NOT EXISTS `shortlist` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `userid` int(11) DEFAULT NULL,
  `vacancyid` int(11) DEFAULT NULL,
  `dateshortlisted` date DEFAULT NULL,
  `shortlistedby` int(11) DEFAULT NULL,
  `isemailsent` int(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=8 ;

--
-- Dumping data for table `shortlist`
--

INSERT INTO `shortlist` (`id`, `userid`, `vacancyid`, `dateshortlisted`, `shortlistedby`, `isemailsent`) VALUES
(6, 9, 3, '2015-01-19', 8, 0),
(7, 22, 3, '2015-01-19', 8, 0);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
