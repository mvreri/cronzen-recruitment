<?php
//require_once 'include/config.php';
$cid =$_SESSION['centum_user_id'];
$system_arr = array();
$system_arr[] = "KCSE";
$system_arr[] = "IGCSE";
$system_arr[] = "UACE";


$position = array();
$grade['KCSE'][] = "A";
$grade['KCSE'][] = "A-";
$grade['KCSE'][] = "B+";
$grade['KCSE'][] = "B";
$grade['KCSE'][] = "B-";
$grade['KCSE'][] = "C+";
$grade['KCSE'][] = "C";
$grade['KCSE'][] = "C-";
$grade['KCSE'][] = "D+";
$grade['IGCSE'][] = "A*";
$grade['IGCSE'][] = "A";
$grade['IGCSE'][] = "B";
$grade['IGCSE'][] = "C";
$grade['IGCSE'][] = "D";
$grade['IGCSE'][] = "E";
$grade['IGCSE'][] = "U";
$grade['UACE'][] = "First grade";
$grade['UACE'][] = "Second Grade";
$grade['UACE'][] = "Third Grade";

  echo '<span style="color:#FF0000;text-align:center;">'.$errorMessage.'</span>'; ?>
<form id="frmaddeducation" name="frmaddeducation" method="post" action="process_applicant.php?action=addhighschool">
<input name="hiduid" type="hidden" id="hiduid" value="<?php echo $cid; ?>">
<table width="75%" align="center">
<tr><td colspan="2"><b>High School Education</b></td></tr>
<tr>
<td>High School Attended *</td><td><input type="text" id="hs" name="hs" placeholder="High School" required/></td>
</tr>
<tr>
<td>Year of Completion *</td><td><input type="text" id="yc" name="yc" placeholder="Year of completion" required/>
</tr>
<tr><td>Exam Undertaken *</td><td>
<select name="exam" id="exam">
<option value="KCSE" active>KCSE</option>
<option value="IGCSE">IGCSE</option>
<option value="UACE">UACE</option>
</select></td></tr>

<tr>
<!--<td>Grade *</td><td><input type="text" id="hsgrade" name="hsgrade" placeholder="Grade"></td>-->
<td>Grade *</td><td><select id="hsgrade" name="hsgrade"></select></td>

</tr>


<tr><td></td><td><input type="button" value="Back" onClick="window.location.href='indexapplicant.php?view=modifyeb';"> <input type="submit" value="Save"></td></tr>
</table>
</form>
<script type="text/javascript">
var s1= document.getElementById("exam");
var s2 = document.getElementById("hsgrade");
onchange(); //Change options after page load
s1.onchange = onchange; // change options when s1 is changed

function onchange() {
    <?php foreach ($system_arr as $sa) {?>
        if (s1.value == '<?php echo $sa; ?>') {
            option_html = "";
            <?php if (isset($grade[$sa])) { ?> // Make sure position is exist
                <?php foreach ($grade[$sa] as $value) { ?>
                    option_html += "<option><?php echo $value; ?></option>";
                <?php } ?>
            <?php } ?>
            s2.innerHTML = option_html;
        }
    <?php } ?>
}
</script>