<?php
if (!defined('WEB_ROOT')) {
	exit;
}
$cid =$_SESSION['centum_user_id'];

$sql = "SELECT * FROM c_education_tertiary WHERE userid=$cid ";
$result     = dbQuery($sql);
echo '<span style="color:#FF0000;text-align:center;">'.$errorMessage.'</span>';
?>
<script language="javascript">
function deletetschool(id)
{
	if (confirm('Are you sure you would like to delete this College/University?')) {
		window.location.href = 'process_applicant.php?action=deltschool&id=' + id;
	}
}
</script>

  
 <div class="row-fluid sortable">
				<div class="box span12">
					<div class="box-header">
						<h2><i class="halflings-icon align-justify"></i><span class="break"></span>Education Background | College/University | Three of Ten</h2>
						<div class="box-icon">
							<a href="indexapplicant.php?view=addtertiary" ><i class="halflings-icon wrench"></i></a>
							<a href="#" class="btn-minimize"><i class="halflings-icon chevron-up"></i></a>
							<a href="#" class="btn-close"><i class="halflings-icon remove"></i></a>
						</div>
					</div>
					<div class="box-content">
						<table class="table">
							  <thead>
								  <tr>
									  <th>Institution Name</th>
									  <th>Grade Achieved</th>
									  <th>Period</th>
                                      <th>Cerificate</th>
									  <th>&nbsp;</th>                                          
								  </tr>
							  </thead>   
							  <tbody>
								<?php
if (dbNumRows($result) > 0) {
	$i = 0;
	
	while($row = dbFetchAssoc($result)) {
		extract($row);			
?> 
								<tr>
									<td><?php echo $institutionname; ?></td>
									<td class="center"><?php echo $GRADE; ?></td>
									<td class="center"><?php $df = date_create("$datefrom");
									$dt = date_create("$dateto"); echo date_format($df,"d/m/Y").' - '.date_format($dt,"d/m/Y"); ?></td>
                                    <td class="center"><?php echo $degree; ?></td>
									<td class="center"><a href="javascript:deletetschool(<?php echo $id; ?>);"><i class="halflings-icon remove"></i></a></td>                                       
								</tr>
                                <?php
	} // end while


?>
  <?php
}else{
?>
<tr>
									<td colspan="5">You haven't added any College/University to your profile yet</td>
									                                        
								</tr>
<?php
}
?>

                                <tr>
									<td colspan="5" align="right"><input name="btntertiary" type="button" id="btntertiary" value="Add College/University" onClick="window.location.href='indexapplicant.php?view=addtertiary';"></td>
									                                        
								</tr>
                                <tr>
									<td colspan="5"></td>
									                                        
								</tr>
<tr>
									<td colspan="5" align="right"> <input name="back" type="button" id="back" value="BACK" onClick="window.location.href='indexapplicant.php?view=modifyeb';"> <input name="next" type="button" id="next" value="NEXT" onClick="window.location.href='indexapplicant.php?view=modifyproq';"></td></tr>                                   
							  </tbody>
						 </table>  
						
					</div>
				</div><!--/span-->
				
			</div><!--/row-->
 