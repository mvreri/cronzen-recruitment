<?php
if (!defined('WEB_ROOT')) {
	exit;
}
if (isset($_GET['vid']) && (int)$_GET['vid'] >= 0) {
	$vid = (int)$_GET['vid'];
	$queryString = "&vid=$vid";
} else {
	$vid = 0;
	$queryString = '';
	header('Location: indexadmin.php?view=addvacancies&error=' . urlencode('No one has applied for that position')); 
}

$cid =$_SESSION['centum_user_id']; 
$sql = "select distinct(u.id) id, u.fname fname,u.sname sname,u.email email,e.exam exam,e.hschoolgrade grade, ai.CV cv, et.GRADE ugrade, et.degree degree
from c_users u
left join c_education e
on e.userid = u.id
left join c_education_tertiary et
on et.userid = u.id
left join c_additionalinfo ai
on ai.userid = u.id
left join c_appliedlog l
on l.userid = u.id
 WHERE l.applicationid=$vid";

$result     = dbQuery($sql);


?>
<script language="javascript">

function ShortlistApplicant(id,vid,cid)
{
	if (confirm('Shortlist this applicant?')) {
		window.location.href = 'process_admin.php?action=shortlist&id=' + id+'&vid='+vid+'&cid='+cid;
	}
}
</script>

<div class="row-fluid sortable">		
				<div class="box span12">
					<div class="box-header" data-original-title>
						<h2><i class="halflings-icon user"></i><span class="break"></span>Applicants</h2>
						<div class="box-icon">
							<a href="#" class="btn-setting"><i class="halflings-icon wrench"></i></a>
							<a href="#" class="btn-minimize"><i class="halflings-icon chevron-up"></i></a>
							<a href="#" class="btn-close"><i class="halflings-icon remove"></i></a>
						</div>
					</div>
					<div class="box-content">
                    <?php echo '<span style="color:#FF0000;text-align:center;">'.$errorMessage.'</span>'; ?>
						<table class="table table-striped table-bordered">
                        
						  <thead>
							  <tr>
									
								  <th>Name </th>
								  <th>High School Grade </th>
								  <th>University Grade</th>
                                  <th>Resume</th>
                                  <th>Shortlist</th>
                                  
                                  
                                  
								  
							  </tr>
						  </thead>   
						  <tbody>
                           <?php
if (dbNumRows($result) > 0) {	
	while($row = dbFetchAssoc($result)) {
		extract($row);			
?> 
							<tr>
							
							  <td><a href="indexadmin.php?view=moreonusers&uid=<?php echo $id; ?>"><?php echo $fname.' '.$sname; ?></a>
                              
                              </td>
								
							  <td class="center"><?php echo $exam.' - <strong>'.$grade.'</strong>'; ?></td>
								<td class="center"><?php echo $degree.' - <strong>'.$ugrade.'</strong>'; ?></td>
                                <td class="center"><a href="upload/<?php echo $cv;?>"><?php echo 'Download '.$sname. '\'s resume - '.$cv; ?></a></td>
                                <td class="center"><a href="javascript:ShortlistApplicant(<?php echo $id; ?>,<?php echo $vid; ?>, <?php echo $cid; ?>);"><i class="halflings-icon play"></i></a></td>
							</tr>
                            <?php
	} // end while


?>
  <?php
}else{
?>
<tr>
								<td colspan="5">No applicants have been shortlisted for this position</td>
								
							</tr>

<?php
}
?>
							
							
						  </tbody>
					  </table>            
					</div>
				</div><!--/span-->
			
			</div><!--/row-->