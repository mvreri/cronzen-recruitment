-- phpMyAdmin SQL Dump
-- version 4.1.6
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Dec 02, 2014 at 05:20 AM
-- Server version: 5.6.16
-- PHP Version: 5.5.9

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `centumdb`
--

-- --------------------------------------------------------

--
-- Table structure for table `c_additionalinfo`
--

CREATE TABLE IF NOT EXISTS `c_additionalinfo` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `userid` int(11) DEFAULT NULL,
  `language1` varchar(20) DEFAULT NULL,
  `language2` varchar(20) DEFAULT NULL,
  `language3` varchar(20) DEFAULT NULL,
  `disability` int(1) DEFAULT NULL,
  `CVLocation` varchar(100) DEFAULT NULL,
  `otherattachmentslocation` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `c_additionalinfo`
--

INSERT INTO `c_additionalinfo` (`id`, `userid`, `language1`, `language2`, `language3`, `disability`, `CVLocation`, `otherattachmentslocation`) VALUES
(1, NULL, 'q', 'w', 'e', 2, 'somepath', NULL),
(2, NULL, 'ere', 'sdf', 'sdfsd', 0, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `c_applications`
--

CREATE TABLE IF NOT EXISTS `c_applications` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `refno` varchar(20) NOT NULL,
  `positionid` int(11) DEFAULT NULL,
  `deptid` int(11) DEFAULT NULL,
  `reportingto` int(11) DEFAULT NULL,
  `closingdate` date DEFAULT NULL,
  `jd` text,
  `dateadded` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `addedby` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `c_applications`
--

INSERT INTO `c_applications` (`id`, `refno`, `positionid`, `deptid`, `reportingto`, `closingdate`, `jd`, `dateadded`, `addedby`) VALUES
(1, 'werwer', 0, 1, 0, '0000-00-00', 'werwer', '2014-11-30 23:32:39', 0);

-- --------------------------------------------------------

--
-- Table structure for table `c_appliedlog`
--

CREATE TABLE IF NOT EXISTS `c_appliedlog` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `userid` int(11) DEFAULT NULL,
  `applicationid` int(11) DEFAULT NULL,
  `dateapplied` datetime DEFAULT NULL,
  `applicationstatus` int(1) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `c_appliedlog`
--

INSERT INTO `c_appliedlog` (`id`, `userid`, `applicationid`, `dateapplied`, `applicationstatus`) VALUES
(1, NULL, 0, NULL, NULL),
(2, 9, 1, '2014-12-02 06:36:54', 1);

-- --------------------------------------------------------

--
-- Table structure for table `c_department`
--

CREATE TABLE IF NOT EXISTS `c_department` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `department` varchar(30) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `c_department`
--

INSERT INTO `c_department` (`id`, `department`) VALUES
(1, 'Finance'),
(2, 'Human Resources');

-- --------------------------------------------------------

--
-- Table structure for table `c_education`
--

CREATE TABLE IF NOT EXISTS `c_education` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `userid` int(11) DEFAULT NULL,
  `hschool` varchar(50) DEFAULT NULL,
  `hschoolfrom` date DEFAULT NULL,
  `hschoolto` date DEFAULT NULL,
  `hschoolgrade` varchar(3) NOT NULL,
  `tertiary1` varchar(50) DEFAULT NULL,
  `tertiary1from` date DEFAULT NULL,
  `tertiary1to` date DEFAULT NULL,
  `tertiary1grade` varchar(3) NOT NULL,
  `tertiary2` varchar(50) DEFAULT NULL,
  `tertiary2from` date DEFAULT NULL,
  `tertiary2to` date DEFAULT NULL,
  `tertiary2grade` varchar(3) NOT NULL,
  `dateupdated` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `c_education`
--

INSERT INTO `c_education` (`id`, `userid`, `hschool`, `hschoolfrom`, `hschoolto`, `hschoolgrade`, `tertiary1`, `tertiary1from`, `tertiary1to`, `tertiary1grade`, `tertiary2`, `tertiary2from`, `tertiary2to`, `tertiary2grade`, `dateupdated`) VALUES
(1, NULL, 'a', NULL, '0000-00-00', 'a', 'as', '0000-00-00', '0000-00-00', 'a', '', '0000-00-00', '0000-00-00', 'a', NULL),
(2, NULL, 'a', NULL, '0000-00-00', 'a', 'as', '0000-00-00', '0000-00-00', 'a', '', '0000-00-00', '0000-00-00', 'a', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `c_employment`
--

CREATE TABLE IF NOT EXISTS `c_employment` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `userid` int(11) DEFAULT NULL,
  `empl1` varchar(50) DEFAULT NULL,
  `empl1from` date DEFAULT NULL,
  `empl1to` date DEFAULT NULL,
  `empl1location` varchar(20) DEFAULT NULL,
  `empl1jt` varchar(20) NOT NULL,
  `empl1jd` text,
  `empl2` varchar(50) DEFAULT NULL,
  `empl2from` date DEFAULT NULL,
  `empl2to` date DEFAULT NULL,
  `empl2jt` varchar(20) NOT NULL,
  `empl2jd` text,
  `empl2location` varchar(20) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `c_employment`
--

INSERT INTO `c_employment` (`id`, `userid`, `empl1`, `empl1from`, `empl1to`, `empl1location`, `empl1jt`, `empl1jd`, `empl2`, `empl2from`, `empl2to`, `empl2jt`, `empl2jd`, `empl2location`) VALUES
(1, NULL, 'a', '0000-00-00', '0000-00-00', 'asas', 'aas', 'dasd', 'dda', '0000-00-00', '0000-00-00', 'fhfgh', 'gfghfghfghsd', 'fghfgh');

-- --------------------------------------------------------

--
-- Table structure for table `c_jobgroup`
--

CREATE TABLE IF NOT EXISTS `c_jobgroup` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `jobgroup` varchar(30) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `c_jobgroup`
--

INSERT INTO `c_jobgroup` (`id`, `jobgroup`) VALUES
(1, 'A'),
(2, 'A');

-- --------------------------------------------------------

--
-- Table structure for table `c_positions`
--

CREATE TABLE IF NOT EXISTS `c_positions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(30) DEFAULT NULL,
  `jobgroup` int(2) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `c_positions`
--

INSERT INTO `c_positions` (`id`, `name`, `jobgroup`) VALUES
(1, 'Head of Finance', 1),
(2, 'Head of HR', 1);

-- --------------------------------------------------------

--
-- Table structure for table `c_referees`
--

CREATE TABLE IF NOT EXISTS `c_referees` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `userid` int(11) DEFAULT NULL,
  `ref1name` varchar(30) DEFAULT NULL,
  `ref1phone` varchar(30) DEFAULT NULL,
  `ref1email` varchar(40) DEFAULT NULL,
  `ref2name` varchar(30) DEFAULT NULL,
  `ref2phone` varchar(30) DEFAULT NULL,
  `ref2email` varchar(40) DEFAULT NULL,
  `ref3name` varchar(30) DEFAULT NULL,
  `ref3phone` varchar(30) DEFAULT NULL,
  `ref3email` varchar(40) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `c_referees`
--

INSERT INTO `c_referees` (`id`, `userid`, `ref1name`, `ref1phone`, `ref1email`, `ref2name`, `ref2phone`, `ref2email`, `ref3name`, `ref3phone`, `ref3email`) VALUES
(1, NULL, 'sedsf', '32434234234', 'a@b.c', 'sdfsdf', '34345345', 'a@b.c', 'bnvnvb', '3225345', 'a@b.c'),
(2, NULL, '', '', '', '', '', '', '', '', ''),
(3, NULL, '', '', '', '', '', '', '', '', '');

-- --------------------------------------------------------

--
-- Table structure for table `c_useract`
--

CREATE TABLE IF NOT EXISTS `c_useract` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `UID` int(11) DEFAULT NULL,
  `LastLogin` datetime DEFAULT NULL,
  `SessionID` text,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=6 ;

--
-- Dumping data for table `c_useract`
--

INSERT INTO `c_useract` (`ID`, `UID`, `LastLogin`, `SessionID`) VALUES
(1, 0, '2014-12-01 21:20:46', 'd3orug01ma07mbfdidslk2qbc6'),
(2, 0, '2014-12-01 21:20:53', 'd3orug01ma07mbfdidslk2qbc6'),
(3, 0, '2014-12-01 21:21:01', 'd3orug01ma07mbfdidslk2qbc6'),
(4, 0, '2014-12-01 21:21:42', 'd3orug01ma07mbfdidslk2qbc6'),
(5, 0, '2014-12-01 21:21:54', 'd3orug01ma07mbfdidslk2qbc6');

-- --------------------------------------------------------

--
-- Table structure for table `c_users`
--

CREATE TABLE IF NOT EXISTS `c_users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `fname` varchar(15) DEFAULT NULL,
  `mname` varchar(15) DEFAULT NULL,
  `sname` varchar(15) DEFAULT NULL,
  `email` varchar(45) DEFAULT NULL,
  `dob` date DEFAULT NULL,
  `gender` varchar(10) DEFAULT NULL,
  `nationality` varchar(20) NOT NULL,
  `natidno` varchar(10) DEFAULT NULL,
  `address` varchar(50) DEFAULT NULL,
  `phone` int(15) DEFAULT NULL,
  `usertype` int(11) NOT NULL,
  `password` varchar(55) DEFAULT NULL,
  `isactive` int(11) NOT NULL DEFAULT '0',
  `dateadded` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=10 ;

--
-- Dumping data for table `c_users`
--

INSERT INTO `c_users` (`id`, `fname`, `mname`, `sname`, `email`, `dob`, `gender`, `nationality`, `natidno`, `address`, `phone`, `usertype`, `password`, `isactive`, `dateadded`) VALUES
(3, 'werw', 'tyryrt', 'rtyrty', 'e', '0000-00-00', 'Male', 'j', '435345', '', 45354354, 0, NULL, 0, '2014-12-01 00:07:13'),
(4, 'qweqwe', 'qweqe', 'wqeqwe', 'e', '0000-00-00', 'Male', 'g', '23424', '', 2147483647, 0, NULL, 0, '2014-12-01 00:08:15'),
(5, '', '', '', 'e', '0000-00-00', '', '', '', '', 0, 0, NULL, 0, '2014-12-01 00:24:46'),
(6, 'Centum', '', 'Admin', 'admin@centum.co.ke', '0000-00-00', '3', '', '0000001', 'Life House HQ', 110011, 0, 'admin', 0, '2014-12-01 05:46:41'),
(7, 'Admin2', '', 'Centum', '2admin@centum.co.ke', '0000-00-00', '3', '', '21212121', 'Life House', 123654789, 0, 'admin2', 1, '2014-12-01 05:49:43'),
(8, 'centum', '', 'superadmin', 'super@centum.co.ke', '0000-00-00', '3', '', '12121212', 'LH', 72727272, 1, 'abc123', 1, '2014-12-01 06:10:37'),
(9, 'very', 'new', 'applcant', 'new@a.b', '0000-00-00', '1', '', '12211221', '342342', 234242, 2, 'abc123', 1, '2014-12-02 05:22:06');

-- --------------------------------------------------------

--
-- Table structure for table `c_usertype`
--

CREATE TABLE IF NOT EXISTS `c_usertype` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(20) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `c_usertype`
--

INSERT INTO `c_usertype` (`id`, `name`) VALUES
(1, 'admin'),
(2, 'applicant');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
