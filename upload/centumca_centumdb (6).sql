-- phpMyAdmin SQL Dump
-- version 3.5.8.2
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Aug 25, 2015 at 11:18 AM
-- Server version: 5.5.42-37.1-log
-- PHP Version: 5.4.23

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `centumca_centumdb`
--

-- --------------------------------------------------------

--
-- Table structure for table `applicant_referals`
--

DROP TABLE IF EXISTS `applicant_referals`;
CREATE TABLE IF NOT EXISTS `applicant_referals` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(30) DEFAULT NULL,
  `ref` varchar(30) DEFAULT NULL,
  `userid` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=8 ;

--
-- Dumping data for table `applicant_referals`
--

INSERT INTO `applicant_referals` (`id`, `name`, `ref`, `userid`) VALUES
(1, '', '', 0),
(2, '', '', 0),
(3, '', '', 0),
(4, '', '', 0),
(5, '', '', 0),
(6, '', '', 0),
(7, '', '', 0);

-- --------------------------------------------------------

--
-- Table structure for table `applicationtrack`
--

DROP TABLE IF EXISTS `applicationtrack`;
CREATE TABLE IF NOT EXISTS `applicationtrack` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `userid` int(11) DEFAULT NULL,
  `haspersonaldetails` int(1) DEFAULT NULL,
  `haseducation` int(1) DEFAULT NULL,
  `hasareaofinterest` int(1) DEFAULT NULL,
  `hasskills` int(1) DEFAULT NULL,
  `haslanguages` int(1) DEFAULT NULL,
  `hasdocuments` int(1) DEFAULT NULL,
  `hasreferences` int(1) DEFAULT NULL,
  `dateupdated` date DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=17 ;

--
-- Dumping data for table `applicationtrack`
--

INSERT INTO `applicationtrack` (`id`, `userid`, `haspersonaldetails`, `haseducation`, `hasareaofinterest`, `hasskills`, `haslanguages`, `hasdocuments`, `hasreferences`, `dateupdated`) VALUES
(2, 9, 1, 1, 1, 1, 1, 1, 1, '2015-01-15'),
(3, 22, 1, 1, 1, 1, 1, 1, 1, '2015-03-04'),
(4, 26, 1, 1, 1, 1, 1, 0, 0, '2015-02-06'),
(5, 29, 1, 1, 0, 1, 1, 0, 0, '2015-02-25'),
(6, 56, 1, 1, 0, 0, 0, 0, 0, '2015-03-02'),
(7, 51, 1, 1, 1, 1, 1, 1, 0, '2015-03-04'),
(8, 63, 1, 1, 1, 1, 1, 1, 0, '2015-03-05'),
(9, 65, 1, 1, 1, 0, 0, 0, 0, '2015-03-09'),
(10, 66, 1, 1, 1, 1, 1, 1, 1, '2015-03-18'),
(11, 67, 1, 1, 1, 1, 1, 0, 0, '2015-05-07'),
(12, 70, 1, 1, 0, 0, 0, 0, 0, '2015-05-14'),
(13, 54, 1, 1, 0, 0, 0, 0, 0, '2015-07-27'),
(14, 90, 1, 1, 0, 0, 0, 0, 0, '2015-08-11'),
(15, 71, 1, 1, 1, 1, 1, 1, 0, '2015-08-13'),
(16, 93, 1, 1, 0, 0, 0, 1, 1, '2015-08-18');

-- --------------------------------------------------------

--
-- Table structure for table `c_additionalinfo`
--

DROP TABLE IF EXISTS `c_additionalinfo`;
CREATE TABLE IF NOT EXISTS `c_additionalinfo` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `userid` int(11) DEFAULT NULL,
  `CV` text NOT NULL,
  `adddocs` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=22 ;

--
-- Dumping data for table `c_additionalinfo`
--

INSERT INTO `c_additionalinfo` (`id`, `userid`, `CV`, `adddocs`) VALUES
(21, 93, 'Amiran.xlsx', 'Madura - What Every Investor Needs To Know About Accounting Fraud (2004) (1).rar');

-- --------------------------------------------------------

--
-- Table structure for table `c_applicantareasofinterest`
--

DROP TABLE IF EXISTS `c_applicantareasofinterest`;
CREATE TABLE IF NOT EXISTS `c_applicantareasofinterest` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `areaofinterest` varchar(30) DEFAULT NULL,
  `areaofinterestspecialization` varchar(30) DEFAULT NULL,
  `userid` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `c_applicantareasofinterest`
--

INSERT INTO `c_applicantareasofinterest` (`id`, `areaofinterest`, `areaofinterestspecialization`, `userid`) VALUES
(2, 'ICT', 'Automation', 66),
(3, 'AGRIC', 'Agronomy', 71);

-- --------------------------------------------------------

--
-- Table structure for table `c_applicantprofqual`
--

DROP TABLE IF EXISTS `c_applicantprofqual`;
CREATE TABLE IF NOT EXISTS `c_applicantprofqual` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `userid` int(11) DEFAULT NULL,
  `courseid` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=24 ;

--
-- Dumping data for table `c_applicantprofqual`
--

INSERT INTO `c_applicantprofqual` (`id`, `userid`, `courseid`) VALUES
(4, 26, 0),
(6, 26, 0),
(7, 22, 0),
(9, 51, 4),
(10, 56, 5),
(13, 63, 1),
(14, 66, 4),
(15, 22, 2),
(16, 67, 6),
(17, 22, 0),
(18, 90, 2),
(19, 90, 4),
(20, 54, 2),
(21, 54, 2),
(23, 71, 4);

-- --------------------------------------------------------

--
-- Table structure for table `c_applications`
--

DROP TABLE IF EXISTS `c_applications`;
CREATE TABLE IF NOT EXISTS `c_applications` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `refno` varchar(20) NOT NULL,
  `positiontitle` varchar(50) NOT NULL,
  `positionid` int(11) DEFAULT NULL,
  `deptid` int(11) DEFAULT NULL,
  `reportingto` int(11) DEFAULT NULL,
  `closingdate` date DEFAULT NULL,
  `jd` text,
  `dateadded` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `addedby` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=13 ;

--
-- Dumping data for table `c_applications`
--

INSERT INTO `c_applications` (`id`, `refno`, `positiontitle`, `positionid`, `deptid`, `reportingto`, `closingdate`, `jd`, `dateadded`, `addedby`) VALUES
(10, 'I001', 'Internship', 0, 4, 2, '2017-01-31', 'We are looking for passionate individuals who would like further their knowledge and sharpen their skills through practical experience working alongside our teams in the office.', '2015-06-29 12:19:17', 8),
(12, '1231', 'Development Manager', 0, 1, 1, '2015-08-11', 'Sole responsibility of ensuring delivery within the set business case parameters.\r\nEnd to end Development Management from land acquisition, advanced strategies, design, construction, funding and urban/asset management\r\nManaging stakeholders for effective collaboration between all parties involved, including public and private sectors, government agencies, consultants, employees, partners and shareholders\r\nProviding weekly project progress reports to the Managing Director highlighting progress, budget, program, issues and risks.\r\nChairing development related meetings and management of the project management teams.\r\nManaging contractual and financial matters relating to project consultant and contractors.\r\nAttendance at Athena development management workshops and board meetings.\r\nIdentification of new development opportunities', '2015-08-11 06:55:39', 8);

-- --------------------------------------------------------

--
-- Table structure for table `c_appliedlog`
--

DROP TABLE IF EXISTS `c_appliedlog`;
CREATE TABLE IF NOT EXISTS `c_appliedlog` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `userid` int(11) DEFAULT NULL,
  `applicationid` int(11) DEFAULT NULL,
  `dateapplied` datetime DEFAULT NULL,
  `applicationstatus` int(1) DEFAULT NULL,
  `isshortlisted` int(1) NOT NULL DEFAULT '0',
  `isemailsent` int(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=13 ;

--
-- Dumping data for table `c_appliedlog`
--

INSERT INTO `c_appliedlog` (`id`, `userid`, `applicationid`, `dateapplied`, `applicationstatus`, `isshortlisted`, `isemailsent`) VALUES
(1, NULL, 0, NULL, NULL, 0, 0),
(2, 9, 1, '2014-12-02 06:36:54', 1, 1, 1),
(3, 8, 3, '2014-12-15 00:49:38', 1, 1, 0),
(4, 9, 3, '2014-12-18 01:50:24', 1, 0, 1),
(5, 22, 1, '2014-12-18 08:54:16', 1, 0, 0),
(6, 22, 2, '2014-12-18 09:00:14', 1, 0, 0),
(7, 22, 3, '2014-12-18 09:07:49', 1, 0, 0),
(8, 22, 6, '2015-03-23 05:52:57', 1, 0, 0),
(9, 22, 9, '2015-06-09 09:48:22', 1, 0, 0),
(10, 22, 8, '2015-06-09 09:51:31', 1, 1, 0),
(11, 66, 10, '2015-07-28 07:02:02', 1, 0, 0),
(12, 22, 12, '2015-08-13 00:05:37', 1, 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `c_areasofinterest`
--

DROP TABLE IF EXISTS `c_areasofinterest`;
CREATE TABLE IF NOT EXISTS `c_areasofinterest` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(40) DEFAULT NULL,
  `deptid` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=31 ;

--
-- Dumping data for table `c_areasofinterest`
--

INSERT INTO `c_areasofinterest` (`id`, `name`, `deptid`) VALUES
(12, 'Agriculture', 1),
(13, 'Biological Sciences', 1),
(14, 'Building and Construction', 1),
(15, 'Business and Commerce/Accounting', 1),
(16, 'Information Communication and Technology', 1),
(17, 'Education', 1),
(18, 'Engineering', 1),
(19, 'Environmental Sciences', 1),
(20, 'Food Sciences and Technology', 1),
(21, 'Economics', 1),
(22, 'Health and Medicine', 1),
(23, 'Hospitalism', 1),
(24, 'Humanities', 1),
(25, 'Mass Communication', 1),
(26, 'Law/Legal', 1),
(27, 'Mathematics', 1),
(28, 'Natural Resource Management', 1),
(29, 'Veterinary', 1),
(30, 'Finance', 1);

-- --------------------------------------------------------

--
-- Table structure for table `c_areasofinterestspecializations`
--

DROP TABLE IF EXISTS `c_areasofinterestspecializations`;
CREATE TABLE IF NOT EXISTS `c_areasofinterestspecializations` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `aoi_id` int(11) DEFAULT NULL,
  `dateadded` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `c_department`
--

DROP TABLE IF EXISTS `c_department`;
CREATE TABLE IF NOT EXISTS `c_department` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `department` varchar(30) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=6 ;

--
-- Dumping data for table `c_department`
--

INSERT INTO `c_department` (`id`, `department`) VALUES
(1, 'Finance'),
(2, 'Human Resources'),
(3, 'IT'),
(4, 'ALL'),
(5, 'Real Estate');

-- --------------------------------------------------------

--
-- Table structure for table `c_education`
--

DROP TABLE IF EXISTS `c_education`;
CREATE TABLE IF NOT EXISTS `c_education` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `userid` int(11) DEFAULT NULL,
  `year_cleared` int(4) DEFAULT NULL,
  `hschool` varchar(50) DEFAULT NULL,
  `exam` varchar(10) DEFAULT NULL,
  `hschoolgrade` varchar(3) NOT NULL,
  `dateupdated` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=39 ;

--
-- Dumping data for table `c_education`
--

INSERT INTO `c_education` (`id`, `userid`, `year_cleared`, `hschool`, `exam`, `hschoolgrade`, `dateupdated`) VALUES
(6, 9, NULL, 'Juja Boys', 'KCSE', 'A', NULL),
(9, 26, NULL, 'Ndakaini Sec. School', 'KCSE', 'B- ', NULL),
(10, 29, NULL, 'Balkan High School', 'KCSE', 'D+', NULL),
(11, 29, NULL, 'James Gichuru High School', 'KCSE', 'E', NULL),
(12, 29, NULL, 'Hagh', 'KCSE', '3', NULL),
(14, 56, NULL, 'ubaguzi', 'KCSE', 'd', NULL),
(15, 56, NULL, 'hjuikjkhj', 'KCSE', '90', NULL),
(21, 51, 2009, 'Gayaza High School', 'UACE', 'Fir', NULL),
(23, 63, 1987, 'fdhgd', 'KCSE', 'A', NULL),
(24, 65, NULL, 'Lenana School', 'KCSE', 'B+', NULL),
(25, 66, NULL, 'Lenana School', 'IGCSE', 'A', NULL),
(30, 67, 1987, 'Lenana School', 'IGCSE', 'A*', NULL),
(31, 70, 1234, 'high', 'KCSE', 'A', NULL),
(32, 22, 2007, 'Lenana School', 'UACE', 'Fir', NULL),
(35, 54, 2007, 'Lenana School', 'IGCSE', 'B', NULL),
(36, 90, 2000, 'Kisii Boys', 'KCSE', 'C-', NULL),
(37, 71, 2014, 'Booker', 'KCSE', 'B+', NULL),
(38, 93, 2007, 'Lenana School', 'KCSE', 'A', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `c_education_tertiary`
--

DROP TABLE IF EXISTS `c_education_tertiary`;
CREATE TABLE IF NOT EXISTS `c_education_tertiary` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `userid` int(11) DEFAULT NULL,
  `institutionname` varchar(30) DEFAULT NULL,
  `datefrom` date DEFAULT NULL,
  `dateto` date DEFAULT NULL,
  `GRADE` varchar(15) DEFAULT NULL,
  `degree` text NOT NULL,
  `dateupdated` date DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=18 ;

--
-- Dumping data for table `c_education_tertiary`
--

INSERT INTO `c_education_tertiary` (`id`, `userid`, `institutionname`, `datefrom`, `dateto`, `GRADE`, `degree`, `dateupdated`) VALUES
(6, 9, 'JKUAT', '2014-12-01', '2014-12-03', 'A', 'Diploma', '2014-12-19'),
(7, 26, 'Kenyatta University', '2009-09-14', '2013-12-20', 'Second Class(Ho', 'Bachelors Degree', '2015-02-06'),
(8, 29, 'Nairobi Aviation', '2014-09-17', '2015-05-22', 'A', 'Certificate', '2015-02-25'),
(9, 22, 'JKUAT', '2007-02-03', '2013-02-05', 'A', 'Bachelors Degree', '2015-02-28'),
(10, 51, 'Makerere University', '2015-03-01', '2015-03-02', 'First class hon', 'Bachelors Degree', '2015-03-04'),
(11, 63, 'dfree', '2015-03-01', '2015-03-10', 'First class hon', 'Bachelors Degree', '2015-03-05'),
(12, 66, 'Nairobi University', '2015-03-03', '2015-03-26', 'First class hon', 'Bachelors Degree', '2015-03-18'),
(13, 67, 'JKUAT', '2015-05-10', '2015-05-20', 'First class hon', 'PhD', '2015-05-07'),
(14, 67, 'JKUAT', '2015-05-03', '2015-05-29', 'Credit', 'Diploma', '2015-05-07'),
(15, 67, 'JKUAT', '2015-05-03', '2015-05-29', 'Credit', 'Diploma', '2015-05-07'),
(16, 90, 'JKUAT', '2015-08-24', '2015-08-31', 'First class hon', 'Associate Professor', '2015-08-11'),
(17, 71, 'JKUAT', '2013-05-01', '2015-05-29', 'Second class up', 'Bachelors Degree', '2015-08-13');

-- --------------------------------------------------------

--
-- Table structure for table `c_emailmessages`
--

DROP TABLE IF EXISTS `c_emailmessages`;
CREATE TABLE IF NOT EXISTS `c_emailmessages` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `email` text,
  `issent` int(1) DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=13 ;

--
-- Dumping data for table `c_emailmessages`
--

INSERT INTO `c_emailmessages` (`id`, `email`, `issent`) VALUES
(1, 'We are pleased to invite you to our offices for an interview', 1),
(2, 'We are pleased to invite you to our offices for an interview', 1),
(3, 'We are pleased to invite you to our offices for an interview', 1),
(4, 'ghfhfgfhfhgfghfhgfg', 1),
(5, 'ghfhfgfhfhgfghfhgfg', 1),
(6, 'ghfhfgfhfhgfghfhgfgdfgdgdfg', 1),
(7, 'sdfsdfsdfsdfsdfsdfsd', 1),
(8, 'sdfsdfsdfsdfsdfsdfrefrfesrefgregergegrd', 2),
(9, 'xfvdgfdgfdg', 0),
(10, 'sdfsdfsdfsdfsdfsfsfsdfsdfsf sdfsdfsdfdfdsfds', 1),
(11, 'dfgdfgdf dbdf df dff d fd df&nbsp;', 1),
(12, 'abc123343242', 0);

-- --------------------------------------------------------

--
-- Table structure for table `c_employment`
--

DROP TABLE IF EXISTS `c_employment`;
CREATE TABLE IF NOT EXISTS `c_employment` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `userid` int(11) DEFAULT NULL,
  `empl1` varchar(50) DEFAULT NULL,
  `empl1from` date DEFAULT NULL,
  `empl1to` date DEFAULT NULL,
  `empl1location` varchar(20) DEFAULT NULL,
  `empl1jt` varchar(20) NOT NULL,
  `empl1jd` text,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=16 ;

--
-- Dumping data for table `c_employment`
--

INSERT INTO `c_employment` (`id`, `userid`, `empl1`, `empl1from`, `empl1to`, `empl1location`, `empl1jt`, `empl1jd`) VALUES
(1, NULL, 'a', '0000-00-00', '0000-00-00', 'asas', 'aas', 'dasd'),
(3, 9, 'A', '0000-00-00', '0000-00-00', 'Nairobi', 'HR', 'job Description here'),
(4, 9, 'D', '0000-00-00', '0000-00-00', 'Location Here', 'Title here', 'JD'),
(5, 9, 'Emp', '0000-00-00', '0000-00-00', 'JLJD', 'JT', 'JD'),
(6, 9, 'R', '2014-11-02', '2014-12-18', 'JLoc', 'JTitle', 'JDesc'),
(7, 26, 'e', '2015-02-02', '2015-02-03', 'l', 't', 'jd'),
(8, 22, 'hhs', '2015-03-09', '2015-03-24', 'dddd', 'ddd', 'dddddddddss'),
(9, 51, 'Centum Investments', '2015-03-02', '2015-03-12', 'Kenya', 'GT', 'HR Analyst'),
(10, 66, 'ddddddd', '2015-03-16', '2015-03-03', 'sdd', 'ddddsdddd', 'cxxzzzzzz'),
(11, 67, 'Centum', '2015-05-03', '2015-05-23', 'Nairobi', 'ICT Manager', 'reryfhsfhdhgdxxxvccxx'),
(12, 51, 'Office of the President', '2015-05-01', '2015-05-02', 'Kampala', 'Research Assistant', 'Research'),
(15, 71, 'Self', '2015-02-02', '2015-07-31', 'Nairobi', 'IT', 'Self');

-- --------------------------------------------------------

--
-- Table structure for table `c_exams`
--

DROP TABLE IF EXISTS `c_exams`;
CREATE TABLE IF NOT EXISTS `c_exams` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `exam` varchar(20) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `c_jobgroup`
--

DROP TABLE IF EXISTS `c_jobgroup`;
CREATE TABLE IF NOT EXISTS `c_jobgroup` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `jobgroup` varchar(30) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=5 ;

--
-- Dumping data for table `c_jobgroup`
--

INSERT INTO `c_jobgroup` (`id`, `jobgroup`) VALUES
(1, 'A'),
(2, 'A'),
(3, 'B'),
(4, 'Graduate trainee');

-- --------------------------------------------------------

--
-- Table structure for table `c_languages`
--

DROP TABLE IF EXISTS `c_languages`;
CREATE TABLE IF NOT EXISTS `c_languages` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `userid` int(11) DEFAULT NULL,
  `language` varchar(20) DEFAULT NULL,
  `fluency` int(1) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=21 ;

--
-- Dumping data for table `c_languages`
--

INSERT INTO `c_languages` (`id`, `userid`, `language`, `fluency`) VALUES
(3, 9, 'Zulu', 1),
(4, 9, 'English', 3),
(5, 26, 'English', 2),
(7, 26, 'Kikuyu', 3),
(8, 26, 'Swahili', 2),
(9, 29, 'asdasdas', 2),
(10, 29, 'ascz\\zczsadsasds', 1),
(11, 22, 'luhya', 3),
(14, 51, 'English', 3),
(15, 63, 'English', 3),
(18, 67, 'English', 3),
(19, 71, 'English', 3);

-- --------------------------------------------------------

--
-- Table structure for table `c_positions`
--

DROP TABLE IF EXISTS `c_positions`;
CREATE TABLE IF NOT EXISTS `c_positions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(30) DEFAULT NULL,
  `jobgroup` int(2) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `c_positions`
--

INSERT INTO `c_positions` (`id`, `name`, `jobgroup`) VALUES
(1, 'Head of Finance', 1),
(2, 'Head of HR', 1),
(3, 'Junior IT Officer', 3);

-- --------------------------------------------------------

--
-- Table structure for table `c_proqualifications`
--

DROP TABLE IF EXISTS `c_proqualifications`;
CREATE TABLE IF NOT EXISTS `c_proqualifications` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `courses` varchar(30) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=8 ;

--
-- Dumping data for table `c_proqualifications`
--

INSERT INTO `c_proqualifications` (`id`, `courses`) VALUES
(2, 'CPA'),
(3, 'CFA'),
(4, 'CCNA'),
(5, 'ACCA'),
(6, 'CISCO'),
(7, 'CCNP');

-- --------------------------------------------------------

--
-- Table structure for table `c_referees`
--

DROP TABLE IF EXISTS `c_referees`;
CREATE TABLE IF NOT EXISTS `c_referees` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `userid` int(11) DEFAULT NULL,
  `designation` varchar(10) NOT NULL,
  `ref1name` varchar(30) DEFAULT NULL,
  `ref1phone` varchar(30) DEFAULT NULL,
  `ref1phonealt` varchar(20) NOT NULL,
  `ref1email` varchar(40) DEFAULT NULL,
  `relationship` varchar(20) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=32 ;

--
-- Dumping data for table `c_referees`
--

INSERT INTO `c_referees` (`id`, `userid`, `designation`, `ref1name`, `ref1phone`, `ref1phonealt`, `ref1email`, `relationship`) VALUES
(8, 9, 'Ms.', 'J', '021456987', '0', 'a@b.c', 'Uncle'),
(10, 9, 'Prof.', 'R', '4345345', '0', 'e@d.n', 'Cousin'),
(11, 22, 'Dr.', 'Njenga', '0724457624', '733542187', 'njenga@gmail.com', 'lecturer'),
(12, 22, 'Ms.', 'swsssssss', '0723452672', '927877272', 'd@gmail.com', 'd.kama'),
(13, 22, 'Ms.', 'hdgd', '098988823', '789883882', 'der@gmail.com', 'ueye'),
(14, 51, 'Ms.', 'Naomi', '12345678', '12345678', 'dasaasira@gmail.com', 'former employer'),
(15, 51, 'Ms.', 'doreen', '12345678', '12345678', 'mugumyajr@gmail.com', 'Employer'),
(16, 51, 'Ms.', 'Jennifer', '12345678', '12345678', 'amoureen24@gmail.com', 'Supervisor'),
(17, 63, 'Ms.', 'dggd', '254724457625', '254724457629', 'ss@d.c', 'ghjhj'),
(18, 63, 'Ms.', 'wd', '254724457623', '254724457620', 'df@g.d', 'sws'),
(20, 66, 'Mrs.', 'jsuj skjs', '254724457624', '254724457624', 'j.gmail.com', 'teacher'),
(21, 66, 'Ms.', 'dkhe kjdskds', '254762525252', '254762525252', 'dhjhy@gmail.com', 'teacher'),
(24, 63, 'Ms.', 'Jean', '254556456852', '254556456853', 'jean@j.c', 'Social'),
(25, 71, 'Ms.', 'Self', '0202020202', '0202020202', 'kagsizo@gmail.com', 'Social'),
(26, 93, 'Mrs.', 'Wanyonyi Steve', '254724457624', '254724456762', 'wanyonyi@steve.com', 'Academic'),
(27, 93, 'Ms.', 'Njeri Kamau', '254723392002', '254731218981', 'njeri@gmail.com', 'Social'),
(28, 93, 'Dr.', 'Akanga David', '254718671902', '254731908217', 'akanga@gmail.com', 'Work'),
(30, 93, 'Ms.', 'Adam Eve', '25476777777', '25476777777', 'a@b.c', 'Academic'),
(31, 93, 'Ms.', 'Jean Doe', '254234234322', '254234234322', 'a@dfds.com', 'Academic');

-- --------------------------------------------------------

--
-- Table structure for table `c_skills`
--

DROP TABLE IF EXISTS `c_skills`;
CREATE TABLE IF NOT EXISTS `c_skills` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `userid` int(11) DEFAULT NULL,
  `skill` varchar(20) DEFAULT NULL,
  `isactive` int(11) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=20 ;

--
-- Dumping data for table `c_skills`
--

INSERT INTO `c_skills` (`id`, `userid`, `skill`, `isactive`) VALUES
(2, 9, 'Dancing', 1),
(4, 9, 'Swimming', 1),
(5, 26, 'Computer literate', 1),
(6, 26, 'Good Interpersonal S', 1),
(7, 29, 'zxcxczxc', 1),
(8, 29, 'zxczxczx', 1),
(11, 51, 'HR', 1),
(12, 63, 'swimming', 1),
(13, 66, 'swimming', 1),
(14, 67, 'skills', 1),
(15, 51, 'HR Consultancy', 1),
(17, 22, 'swimming', 1),
(19, 71, 'Coding', 1);

-- --------------------------------------------------------

--
-- Table structure for table `c_useract`
--

DROP TABLE IF EXISTS `c_useract`;
CREATE TABLE IF NOT EXISTS `c_useract` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `UID` int(11) DEFAULT NULL,
  `LastLogin` datetime DEFAULT NULL,
  `SessionID` text,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=33 ;

--
-- Dumping data for table `c_useract`
--

INSERT INTO `c_useract` (`ID`, `UID`, `LastLogin`, `SessionID`) VALUES
(1, 0, '2014-12-01 21:20:46', 'd3orug01ma07mbfdidslk2qbc6'),
(2, 0, '2014-12-01 21:20:53', 'd3orug01ma07mbfdidslk2qbc6'),
(3, 0, '2014-12-01 21:21:01', 'd3orug01ma07mbfdidslk2qbc6'),
(4, 0, '2014-12-01 21:21:42', 'd3orug01ma07mbfdidslk2qbc6'),
(5, 0, '2014-12-01 21:21:54', 'd3orug01ma07mbfdidslk2qbc6'),
(6, 8, '2015-02-04 02:34:45', '1bca90dc6a7a3a4808a9676c60214c62'),
(7, 8, '2015-02-04 03:00:18', '1bca90dc6a7a3a4808a9676c60214c62'),
(8, 8, '2015-02-04 07:51:27', '92cd107326efca7c4af2b3786ccd1724'),
(9, 26, '2015-02-06 05:28:01', 'b9676fbe39b4d59658116646ad7205b2'),
(10, 27, '2015-02-06 08:24:10', '8b48745d700d390f49e53b937d168e27'),
(11, 22, '2015-02-11 01:53:50', 'b458b9cd4f45d6692136a912076d6a02'),
(12, 8, '2015-02-11 02:08:39', 'b458b9cd4f45d6692136a912076d6a02'),
(13, 8, '2015-02-11 10:29:33', 'c78c9a5c93f639afb3f83050e0435735'),
(14, 26, '2015-02-17 02:38:28', 'a6ac017ede022c63d2cc9bed9e74c045'),
(15, 22, '2015-02-18 06:05:24', 'cc570595f8232fd772a0cf2c048ab1ab'),
(16, 22, '2015-02-22 21:19:02', '0da7ee1134b0ab389d3d5970e8f37fbf'),
(17, 28, '2015-02-25 01:56:21', 'cfc13c972a2c8f33863b5800f433ed0a'),
(18, 28, '2015-02-25 01:58:46', '0e4d91eb6ea3d3c739b0541353d223c0'),
(19, 28, '2015-02-25 02:00:32', '80617aa13c2cf7580b855258b162e6f5'),
(20, 28, '2015-02-25 02:00:47', '80617aa13c2cf7580b855258b162e6f5'),
(21, 29, '2015-02-25 02:02:34', 'a0ef28dcd057c1521929b8ad0fba30f6'),
(22, 29, '2015-02-25 02:03:00', 'cfc13c972a2c8f33863b5800f433ed0a'),
(23, 28, '2015-02-25 04:29:40', 'eaf78a8c477fc57648e7798cbf846e53'),
(24, 28, '2015-02-25 04:29:41', 'eaf78a8c477fc57648e7798cbf846e53'),
(25, 28, '2015-02-25 04:35:53', '6dd6375df217e227c79ffea9372fedf7'),
(26, 28, '2015-02-25 04:37:09', '482c182f3b5c56bae6f50ee68016beb5'),
(27, 28, '2015-02-25 04:37:22', '526b4960e69fb85f9b8d8a2901cf6b69'),
(28, 28, '2015-02-25 04:43:57', '6dd6375df217e227c79ffea9372fedf7'),
(29, 28, '2015-02-25 04:44:08', 'bafe811974e8b8bbedd8ffab215ef37b'),
(30, 28, '2015-02-27 09:08:31', '402208ea6040feb6130ba5160924eb43'),
(31, 8, '2015-02-27 09:09:53', '402208ea6040feb6130ba5160924eb43'),
(32, 22, '2015-02-27 09:58:34', '0da7ee1134b0ab389d3d5970e8f37fbf');

-- --------------------------------------------------------

--
-- Table structure for table `c_users`
--

DROP TABLE IF EXISTS `c_users`;
CREATE TABLE IF NOT EXISTS `c_users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `fname` varchar(15) DEFAULT NULL,
  `mname` varchar(15) DEFAULT NULL,
  `sname` varchar(15) DEFAULT NULL,
  `email` varchar(45) DEFAULT NULL,
  `dob` date DEFAULT NULL,
  `gender` varchar(10) DEFAULT NULL,
  `nationality` varchar(20) NOT NULL,
  `natidno` varchar(10) DEFAULT NULL,
  `address` varchar(50) DEFAULT NULL,
  `phone` bigint(20) DEFAULT NULL,
  `usertype` int(11) NOT NULL,
  `password` varchar(55) DEFAULT NULL,
  `isactive` int(11) NOT NULL DEFAULT '0',
  `dateadded` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=94 ;

--
-- Dumping data for table `c_users`
--

INSERT INTO `c_users` (`id`, `fname`, `mname`, `sname`, `email`, `dob`, `gender`, `nationality`, `natidno`, `address`, `phone`, `usertype`, `password`, `isactive`, `dateadded`) VALUES
(8, 'centum', '', 'superadmin', 'super@centum.co.ke', '0000-00-00', '3', '', '12121212', 'LH', 72727272, 1, 'abc123', 1, '2014-12-01 12:10:37'),
(22, 'Collins', 'cyril', 'Waswa', 'collinswaswa@gmail.com', '1969-12-31', 'Male', 'kenyan', '29388383', '', 254724457624, 2, 'raboli', 1, '2014-12-18 14:53:44'),
(24, 'glenn', NULL, 'omondi', 'glennogolah@gmail.com', NULL, NULL, '', NULL, NULL, NULL, 2, 'sejong$1', 1, '2015-01-22 07:00:59'),
(25, 'ken', NULL, 'wafula', 'kenwafula@gmail.com', NULL, NULL, '', NULL, NULL, NULL, 2, 'abc123', 0, '2015-01-24 07:37:37'),
(26, 'Daniel', 'Wainaina', 'Gikonyo', 'dan.gikonyo@yahoo.com', '1983-08-16', 'Male', 'Kenyan', '23953032', '8157-00100 Nairobi', 722269813, 2, 'Augustsixteen1983', 1, '2015-02-06 11:27:36'),
(51, 'Doreen', '', 'Asaasira', 'dasaasira@gmail.com', '2015-03-04', 'Male', 'Ugandan', 'B1126498', '123', 256773562684, 2, 'mugumyaamos', 1, '2015-03-02 08:51:04'),
(53, 'raymond', NULL, 'omondi', 'raycomondi@gmail.com', NULL, NULL, '', NULL, NULL, NULL, 2, 'abc123', 1, '2015-03-02 12:48:21'),
(70, 'James', '', 'Victor', 'james.ngomi@gmail.com', '1969-12-31', 'Male', 'kenyan', '29388383', '1234', 254724457624, 2, 'sainz2@CE', 1, '2015-05-14 13:26:23'),
(90, 'Steve', '', 'Karechio', 's.karechio@centum.co.ke', '2015-11-08', 'Male', 'Kenya', '23842233', '', 254721913945, 2, 'surgin123', 1, '2015-08-11 06:37:52'),
(93, 'Collins', 'cyril', 'Waswa', 'c.waswa@centum.co.ke', '1989-01-08', 'Male', 'Kenyan', '27325912', '229 Kiminini', 254724457624, 2, 'raboli', 1, '2015-08-18 13:27:57');

-- --------------------------------------------------------

--
-- Table structure for table `c_usertype`
--

DROP TABLE IF EXISTS `c_usertype`;
CREATE TABLE IF NOT EXISTS `c_usertype` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(20) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `c_usertype`
--

INSERT INTO `c_usertype` (`id`, `name`) VALUES
(1, 'admin'),
(2, 'applicant');

-- --------------------------------------------------------

--
-- Table structure for table `exam_name`
--

DROP TABLE IF EXISTS `exam_name`;
CREATE TABLE IF NOT EXISTS `exam_name` (
  `exam_id` int(11) NOT NULL AUTO_INCREMENT,
  `exam_name` text,
  PRIMARY KEY (`exam_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `exam_name`
--

INSERT INTO `exam_name` (`exam_id`, `exam_name`) VALUES
(1, 'KCSE'),
(2, 'UACE');

-- --------------------------------------------------------

--
-- Table structure for table `grade_exam`
--

DROP TABLE IF EXISTS `grade_exam`;
CREATE TABLE IF NOT EXISTS `grade_exam` (
  `grade_cat_id` int(11) NOT NULL AUTO_INCREMENT,
  `grade_cat_name` text,
  `exam_cat_id` int(11) NOT NULL,
  PRIMARY KEY (`grade_cat_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=6 ;

--
-- Dumping data for table `grade_exam`
--

INSERT INTO `grade_exam` (`grade_cat_id`, `grade_cat_name`, `exam_cat_id`) VALUES
(1, 'A', 1),
(2, 'B', 1),
(3, 'C', 1),
(4, 'A', 2),
(5, 'A-', 2);

-- --------------------------------------------------------

--
-- Table structure for table `shortlist`
--

DROP TABLE IF EXISTS `shortlist`;
CREATE TABLE IF NOT EXISTS `shortlist` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `userid` int(11) DEFAULT NULL,
  `vacancyid` int(11) DEFAULT NULL,
  `dateshortlisted` date DEFAULT NULL,
  `shortlistedby` int(11) DEFAULT NULL,
  `isemailsent` int(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=11 ;

--
-- Dumping data for table `shortlist`
--

INSERT INTO `shortlist` (`id`, `userid`, `vacancyid`, `dateshortlisted`, `shortlistedby`, `isemailsent`) VALUES
(6, 9, 3, '2015-01-19', 8, 0),
(7, 22, 3, '2015-01-19', 8, 0),
(8, 22, 3, '2015-01-30', 8, 0),
(9, 8, 3, '2015-01-30', 8, 0),
(10, 22, 8, '2015-06-10', 8, 0);

-- --------------------------------------------------------

--
-- Table structure for table `testr`
--

DROP TABLE IF EXISTS `testr`;
CREATE TABLE IF NOT EXISTS `testr` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `thetest` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=29 ;

--
-- Dumping data for table `testr`
--

INSERT INTO `testr` (`id`, `thetest`) VALUES
(1, 'year.ninety@gmail.com'),
(2, ''),
(3, ''),
(4, ''),
(5, ''),
(6, ''),
(7, ''),
(8, ''),
(9, ''),
(10, ''),
(11, ''),
(12, ''),
(13, ''),
(14, ''),
(15, ''),
(16, ''),
(17, ''),
(18, ''),
(19, 'c.waswa@centum.co.ke'),
(20, ''),
(21, ''),
(22, ''),
(23, ''),
(24, ''),
(25, ''),
(26, ''),
(27, ''),
(28, '');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
