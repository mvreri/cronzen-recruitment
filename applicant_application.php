<?php
$cid =$_SESSION['centum_user_id'];

$rowsPerPage = 10;
$sql = "SELECT * from c_users where id = $cid";
$result     = dbQuery(getPagingQuery($sql, $rowsPerPage));
$pagingLink = getPagingLink($sql, $rowsPerPage);
?> 
<script language="javascript">
function changeDetails(id)
{
	window.location.href = 'indexadmin.php?view=modify&userid=' + id;
}
function deleteUser(id)
{
	if (confirm('Are you sure you would like to delete this user?')) {
		window.location.href = 'process_admin.php?action=deluser&id=' + id;
	}
}

function ActivateUser(id)
{
	if (confirm('Are you sure you would like to activate this user?')) {
		window.location.href = 'process_admin.php?action=activateuser&id=' + id;
	}
}
</script>
<div class="row-fluid sortable">		
				<div class="box span12">
					<div class="box-header" data-original-title>
						<h2><i class="halflings-icon user"></i><span class="break"></span>Personal Profile</h2>
						<div class="box-icon">
							<a href="#" class="btn-setting"><i class="halflings-icon wrench"></i></a>
							<a href="#" class="btn-minimize"><i class="halflings-icon chevron-up"></i></a>
							<a href="#" class="btn-close"><i class="halflings-icon remove"></i></a>
						</div>
					</div>
					<div class="box-content">
						<table class="table table-striped table-bordered bootstrap-datatable datatable">
						  <thead>
							  <tr>
								  <th>Date of Birth</th>
								  <th>Gender</th>
								  <th>Phone</th>
                                  <th>Address</th>
                                  <th>Personal Details Status</th>
                                  
                                  
								  
						    </tr>
						  </thead>   
						  <tbody>
                          <?php
if (dbNumrows($result)>0){
	while($row = dbFetchAssoc($result)) {
		extract($row);
?><input name="hiduid" type="hidden" id="hiduid" value="<?php echo $cid; ?>">
							<tr>
								<td><?php echo $fname.' '. $sname; ?></td>
								<td class="center"><?php echo $gender;?></td>
								<td class="center"><?php echo $dob; ?></td>
                                <td class="center"><?php echo $address; ?></td>
								<td class="center"><?php if ($isactive==0){ ?>
									<span class="label label-fail">Not Complete</span><?php } else if ($isactive==1){ ?><span class="label label-success">Profile Complete</span><?php } ?>
								</td>
                                <td><!--a href="javascript:changeDetails(<?php echo $id; ?>);"><i class="halflings-icon wrench"></i></a-->&nbsp;&nbsp;&nbsp;&nbsp;<a href="javascript:deleteUser(<?php echo $id; ?>);"><i class="halflings-icon remove"></i></a>&nbsp;&nbsp;&nbsp;&nbsp;
                                <a href="javascript:ActivateUser(<?php echo $id; ?>);"><i class="halflings-icon play"></i></a></td>
								
							</tr>
                            <?php
}// end while
}else{
?>
<tr><td colspan="6" align="center">There are no openings on the system yet</td></tr>
<?php }?>
							
						  </tbody>
					  </table>            
					</div>
				</div><!--/span-->
			
			</div><!--/row-->