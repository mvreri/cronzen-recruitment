<?php
if (!defined('WEB_ROOT')) {
	exit;
}


$sql = "SELECT s.id sid, s.userid uid, u.fname fname, u.sname sname, us.sname ussname,us.id usid, us.fname usfname, us.email as usemail, a.positiontitle position, s.isemailsent emailsent, s.dateshortlisted ds , a.id as aid
from shortlist s
inner join c_applications a
on a.id = s.vacancyid
inner join c_users u
on u.id = s.shortlistedby
inner join c_users us
on us.id = s.userid
inner JOIN c_appliedlog l
ON l.userid = us.id
WHERE l.isshortlisted=1
";
$result     = dbQuery($sql);
 
?> 
<script language="javascript">
function changeDetails(id)
{
	window.location.href = 'indexadmin.php?view=modify&userid=' + id;
}
function deleteFromShortlist(uid,aid)
{
	if (confirm('Are you sure you would like to remove this applicant from the shortlist?')) {
		window.location.href = 'process_admin.php?action=delshortlist&uid=' + uid +'&aid='+aid;
	}
}

function SendEmail(uid)
{
	if (confirm('Are you sure you would like to email this user?')) {
		window.location.href = 'process_admin.php?action=activateuser&id=' + id;
	}
}
</script>
 <?php echo '<span style="color:#FF0000;text-align:center;">'.$errorMessage.'</span>'; ?>
<div class="row-fluid sortable">		
				<div class="box span12">
					<div class="box-header" data-original-title>
						<h2><i class="halflings-icon user"></i><span class="break"></span>Shortlisted Applicants</h2>
                        
						<div class="box-icon">
							<a href="#" class="btn-setting"><i class="halflings-icon wrench"></i></a>
							<a href="#" class="btn-minimize"><i class="halflings-icon chevron-up"></i></a>
							<a href="#" class="btn-close"><i class="halflings-icon remove"></i></a>
						</div>
					</div>
					<div class="box-content">
						<table class="table table-bordered bootstrap-datatable datatable">
						  <thead>
							  <tr>
									<th></th>
								  <th>Name</th>
								  <th>Date Shortlisted</th>
                                  <th>Shortlisted by</th>
								  <th>Position Shortlisted For</th>
								  <th>&nbsp;</th>
                                  
								  
						    </tr>
						  </thead>   
						  <tbody>
                          <?php
if (dbNumrows($result)>0){
	while($row = dbFetchAssoc($result)) {
		extract($row);
?><input name="hiduid" type="hidden" id="hiduid" value="<?php echo $id; ?>">
							<tr>
                            <td><a href="javascript:deleteFromShortlist(<?php echo $uid; ?>,<?php echo $aid; ?>);"><i class="halflings-icon remove"></i></a></td>
								<td><?php echo $usfname.' '. $ussname; ?></td>
								<td class="center"><?php echo $ds;?></td>
                                <td class="center"><?php echo $fname.' '.$sname; ?></td>
								<td class="center"><a href="indexadmin.php?view=expand&id=<?php echo $id; ?>"><?php echo $position; ?></a></td>
								<td class="center"><?php if ($emailsent==0){ ?>
									<span class="label label-fail">Email not yet sent</span><?php } else if ($emailsent==1){ ?><span class="label label-success">Email Sent</span><?php } else { ?><span class="label label-fail">Investigate User Details</span><?php } ?>
								</td>
                                
								
							</tr>
                            <?php
}// end while
}else{
?>
<tr><td colspan="6" align="center">There are no shortlisted applicants yet</td></tr>
<?php }?>

							
						  </tbody>
					  </table> 
                      <table>
                      <tr>
  <td colspan="6" align="right">&nbsp;</td><td><input type="button" value="Compose Email" onClick="window.location.href='indexadmin.php?view=email';"><br><br><input type="button" value="Compose Rejection Email" onClick="window.location.href='process_admin.php?action=sendRejectEmail';"></td></tr>
                      </table>           
					</div>
				</div><!--/span-->
			
			</div><!--/row-->