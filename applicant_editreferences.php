<?php
if (!defined('WEB_ROOT')) {
	exit;
}

$cid =$_SESSION['centum_user_id'];


$sql = "SELECT * FROM c_referees where userid = '$cid'";
$result     = dbQuery($sql);

?>
<script language="javascript">
function deleteRef(id)
{
	if (confirm('Are you sure you would like to delete this Reference?')) {
		window.location.href = 'process_applicant.php?action=delref&id=' + id;
	}
}
</script>
<div class="row-fluid sortable">
  <div class="box span12">
					<div class="box-header">
						<h2><i class="halflings-icon align-justify"></i><span class="break"></span>References</h2>
						<div class="box-icon">
							<a href="indexapplicant.php?view=addref" ><i class="halflings-icon wrench"></i></a>
							<a href="#" class="btn-minimize"><i class="halflings-icon chevron-up"></i></a>
							<a href="#" class="btn-close"><i class="halflings-icon remove"></i></a>
						</div>
					</div>
					<div class="box-content">
						<table class="table">
							  <thead>
								  <tr>
									  <th>Name</th>
									  <th>Phone Number</th>
									  <th>Email</th>
                                      <th>Relationship</th>
									  <th>&nbsp;</th>                                          
								  </tr>
							  </thead>   
							  <tbody>
                               <?php
if (dbNumRows($result) > 0) {
	$i = 0;
	
	while($row = dbFetchAssoc($result)) {
		extract($row);			
?> 
								<tr>
									<td><?php echo $ref1name; ?></td>
									<td class="center"><?php echo $ref1phone; ?></td>
									<td class="center"><?php echo $ref1email; ?></td>
                                    <td class="center"><?php echo $relationship; ?></td>
									<td class="center">
										<a href="javascript:deleteRef(<?php echo $id; ?>);"><i class="halflings-icon remove"></i></a>
									</td>                                       
								</tr>
                                <?php
	} // end while


?>
  <?php
}else{
?>
                                
								<tr>
									<td colspan="5">You have not added any references to your profile yet</td>                                       
								</tr>
                                <?php
}
?>
<tr>
									<td colspan="5"><input name="btnref" type="button" id="btnref" value="Add Reference" onClick="window.location.href='indexapplicant.php?view=addref';"></td>                                       
								</tr>
                                <tr>
									<td colspan="5">&nbsp;</td>                                       
								</tr>
                                <tr>
									<td colspan="5">&nbsp;</td>                                       
								</tr>
                                <tr>
									<td colspan="5"><input name="back" type="button" id="back" value="BACK" onClick="window.location.href='indexapplicant.php?view=modifydocs';"> <input name="next" type="button" id="next" value="FINISH" onClick="window.location.href='indexapplicant.php?view=finishsub';"> </td>                                       
								</tr>
								
							  </tbody>
					  </table>  
						 
					</div>
				</div><!--/span--><!--/span-->
</div><!--/row-->