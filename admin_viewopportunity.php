<?php
if (!defined('WEB_ROOT')) {
	exit;
}
if (isset($_GET['id']) && (int)$_GET['id'] >= 0) {
	$vid = (int)$_GET['id'];
	$queryString = "&id=$vid";
} else {
	$vid = 0;
	$queryString = '';
}

$sql = "SELECT *
        FROM c_applications a
		left join c_positions p
		on a.positionid = p.id
        inner join c_department d
        on d.id = a.deptid
		WHERE a.id=$vid";
$result     = dbQuery($sql);

 echo '<span style="color:#FF0000;text-align:center;">'.$errorMessage.'</span>'; ?>
<input name="hiduid" type="hidden" id="hiduid" value="<?php echo $cid; ?>">
<input name="hidaid" type="hidden" id="hidaid" value="<?php echo $vid; ?>">
<div class="row-fluid sortable">
				<div class="box span12">
					<div class="box-header">
						<h2><i class="halflings-icon align-justify"></i><span class="break"></span>Job Opportunity</h2>
						<div class="box-icon">
							
							<a href="#" class="btn-minimize"><i class="halflings-icon chevron-up"></i></a>
							<a href="#" class="btn-close"><i class="halflings-icon remove"></i></a>
						</div>
					</div>
					<div class="box-content">
						<table class="table">
						<?php
if (dbNumRows($result) > 0) {	
	while($row = dbFetchAssoc($result)) {
		extract($row);			
?> 	  
								  <tr>
									  <th>Ref No.</th>
									  <td><?php echo $refno; ?></td>
									                                            
								  </tr>
							  
								<tr>
									<th>Job Title</th>
									<td class="center"><?php echo $positiontitle; ?></td>
									                                       
								</tr>
								<tr>
									<th>Department</th>
									<td class="center"><?php echo $department; ?></td>
									                                       
								</tr>
								<tr>
									<th>Job Description</th>
									<td class="center"><?php echo $jd ?></td>
									                                        
								</tr>
								<?php
	} // end while


?>
  <?php
}else{
?>
<tr>
									<th></th>
		  <td class="center">Details for this opportunity have not been added yet</td>
									                                        
						  </tr>

<?php
}
?>
                                <tr>
                                <th>&nbsp;</th>
									<td class="center"> <input type="button" value="View Applicants" onClick="window.location.href='indexadmin.php?view=viewapplicants&vid=<?php echo $vid; ?>';"><input name="back" type="button" id="back" value="Back" onClick="window.location.href='indexadmin.php?view=viewvacancies';"></td>
                                </tr>  
                         
							  
					  </table>  
						    
					</div>
				</div><!--/span-->
				
				
</div><!--/row-->
