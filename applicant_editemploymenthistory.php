<?php
if (!defined('WEB_ROOT')) {
	exit;
}
$cid =$_SESSION['centum_user_id'];

$sql = "SELECT * FROM c_employment WHERE userid=$cid";
$result     = dbQuery($sql);
echo '<span style="color:#FF0000;text-align:center;">'.$errorMessage.'</span>';
?>

<script language="javascript">

function deleteEmployment(id)
{
	if (confirm('Are you sure you would like to delete this Employment Record?')) {
		window.location.href = 'process_applicant.php?action=delemployment&id=' + id;
	}
}
</script>
<div class="row-fluid sortable">
				<div class="box span12">
					<div class="box-header">
						<h2><i class="halflings-icon align-justify"></i><span class="break"></span>Employment History | Five of Ten</h2>
						<div class="box-icon">
							<a href="#" class="btn-setting"><i class="halflings-icon wrench"></i></a>
							<a href="#" class="btn-minimize"><i class="halflings-icon chevron-up"></i></a>
							<a href="#" class="btn-close"><i class="halflings-icon remove"></i></a>
						</div>
					</div>
					<div class="box-content">
						<table class="table">
							  <thead>
								  <tr>
									  <th>Employer</th>
									  <th>Period</th>
									  <th>Location</th>
                                      <th>Job Title</th>
                                      <th>Job Description</th>
									  <th>&nbsp;</th>                                          
								  </tr>
							  </thead>   
							  <tbody>
                              <?php
if (dbNumRows($result) > 0) {
	$i = 0;
	
	while($row = dbFetchAssoc($result)) {
		extract($row);			
?> 
								<tr>
									<td><?php echo $empl1; ?></td>
									<td class="center"><?php
									$from = date_create("$empl1from");
									$to = date_create("$empl1to"); 
									echo date_format($from,"d/m/Y").' - '.date_format($to,"d/m/Y"); ?></td>
									<td class="center"><?php echo $empl1location; ?></td>
									<td class="center">
										<?php echo $empl1jt; ?>
									</td>
                                    <td class="center">
										<?php echo $empl1jd; ?>
									</td>
                                    <td class="center">
										<a href="javascript:deleteEmployment(<?php echo $id; ?>);"><i class="halflings-icon remove"></i></a>
									</td>                                       
								</tr>
                                <?php
	} // end while


?>
  <?php
}else{
?>
<tr>
									<td colspan="6" align="right">No previous employer has been added for your profile</td>
								
	<?php
}
?>	
<tr>
									<td colspan="6" align="right"><input name="btnemployment" type="button" id="btnemployment" value="Add Employer" onClick="window.location.href='indexapplicant.php?view=addemployment';"></td>
									                                       
								</tr>
<tr>
									<td colspan="6" align="right">&nbsp;</td>
									                                       
								</tr>
                                <tr>
									<td colspan="6" align="right"> <input name="back" type="button" id="back" value="BACK" onClick="window.location.href='indexapplicant.php?view=modifyproq';"> <input name="next" type="button" id="next" value="NEXT" onClick="window.location.href='indexapplicant.php?view=modifysk';"></td>
									                                       
								</tr>
                                			                                   
							  </tbody>
					  </table>  
						 
					</div>
  </div><!--/span-->
				
				
</div><!--/row-->