<?php
$cid =$_SESSION['centum_user_id']; 
$sql ="SELECT (select count(*) from applicationtrack t inner join c_applicantareasofinterest a on a.userid = t.userid where t.haspersonaldetails=0 OR t.haseducation=0 OR t.hasareaofinterest=0 OR t.hasskills=0 OR t.haslanguages=0 OR t.hasdocuments=0 OR t.hasreferences=0) inco,
 (select count(*) from applicationtrack t inner join c_applicantareasofinterest a on a.userid = t.userid where t.haspersonaldetails=1 AND t.haseducation=1 AND t.hasareaofinterest=1 AND t.hasskills=1 AND t.haslanguages=1 and t.hasdocuments=1 AND t.hasreferences=1 ) co, a.name aoi 
 from applicationtrack t inner join c_applicantareasofinterest i
on i.userid = t.userid
inner join c_areasofinterest a
on i.areaofinterest = a.id";

$result     = dbQuery($sql);

?>
<div class="row-fluid sortable">		
				<div class="box span12">
					<div class="box-header" data-original-title>
						<h2><i class="halflings-icon user"></i><span class="break"></span>Applications</h2>
						<div class="box-icon">
							<a href="#" class="btn-setting"><i class="halflings-icon wrench"></i></a>
							<a href="#" class="btn-minimize"><i class="halflings-icon chevron-up"></i></a>
							<a href="#" class="btn-close"><i class="halflings-icon remove"></i></a>
						</div>
					</div>
					<div class="box-content">
						<table class="table table-striped table-bordered bootstrap-datatable datatable">
						  <thead>
							  <tr>
								  <th>Complete Applications</th>
								  <th>InComplete Applications </th>
								  <th>Area of Interest</th>
								  
							  </tr>
						  </thead>   
						  <tbody>
                           <?php
if (dbNumRows($result) > 0) {	
	while($row = dbFetchAssoc($result)) {
		extract($row);			
?> 
							<tr>
								<td><a href="#"><?php echo $co; ?></a></td>
								<td class="center"><?php echo $inco;  ?></td>
								<td class="center"><?php echo $aoi;  ?></td>
								
							</tr>
							
                            <?php
	} // end while


?>
  <?php
}else{
?>
							<tr>
								<td colspan="3">No applications exist yet</td>								
							</tr>
<?php
}
?>							
						  </tbody>
					  </table>            
					</div>
				</div><!--/span-->
			
			</div><!--/row-->