<?php
if (!defined('WEB_ROOT')) {
	exit;
}
$cid =$_SESSION['centum_user_id'];

$sql = "SELECT * FROM c_languages WHERE userid=$cid";
$result     = dbQuery($sql);
  ?>
 <script language="javascript">

function deleteLanguage(id)
{
	if (confirm('Are you sure you would like to remove this language?')) {
		window.location.href = 'process_applicant.php?action=dellang&id=' + id;
	}
}

</script>
<div class="row-fluid sortable">
  <div class="box span6">
					<div class="box-header">
						<h2><i class="halflings-icon align-justify"></i><span class="break"></span>Languages</h2>
						<div class="box-icon">
							<a href="#" class="btn-setting"><i class="halflings-icon wrench"></i></a>
							<a href="#" class="btn-minimize"><i class="halflings-icon chevron-up"></i></a>
							<a href="#" class="btn-close"><i class="halflings-icon remove"></i></a>
						</div>
					</div>
					<div class="box-content">
					  <table class="table">
							  <thead>
                               
								  <tr>
									  <th>Language</th>
									  <th>Fluency</th>
									  
									  <th>&nbsp;</th>                                          
								  </tr>
							  </thead>   
							  <tbody>
                              <?php
if (dbNumRows($result) > 0) {	
	while($row = dbFetchAssoc($result)) {
		extract($row);			
?> 
								<tr>
									<td><?php echo $language; ?></td>
									<td class="center"><?php if ($fluency==3){ ?>
									<span class="label label-success">Fluent</span><?php } else if ($fluency==2){ ?><span class="label label-success">Fluent</span><?php } else { ?><span class="label label-fail">Not fluent</span><?php } ?></td>
								<td class="center">
										<a href="javascript:deleteLanguage(<?php echo $id; ?>);"><i class="halflings-icon remove"></i></a>
									</td>                                       
								</tr>
                                 <?php
	} // end while


?>
  <?php
}else{
?>
								<tr>
									<td colspan="3">You have not added any languages to your profile</td>
									                                       
								</tr>
                                <?php
}
?>
<tr>
									<td colspan="3"><input name="btnlanguages" type="button" id="btnlanguages" value="Add Languages" onClick="window.location.href='indexapplicant.php?view=addlanguages';"></td>
									                                       
								</tr>
								
								<tr>
									<td colspan="3" align="right"> <input name="back" type="button" id="back" value="BACK" onClick="window.location.href='indexapplicant.php?view=modifyareasofinterest';">  <input name="next" type="button" id="next" value="NEXT" onClick="window.location.href='indexapplicant.php?view=modifydocs';"></td></tr>
									
	
								
								                                   
							  </tbody>
					  </table>  
						      
					</div>
				</div><!--/span--><!--/span-->
			</div><!--/row-->
            
            
        