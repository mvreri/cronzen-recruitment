<?php

require_once 'library/config2.php';
require_once 'functions.php';

//checkUser();

$action = isset($_GET['action']) ? $_GET['action'] : '';
switch ($action) {
	
    case 'addpersonal' :
        addPersonal();
        break;
		
	case 'addhighschool' :
        addHighSchool();
        break;
		
	case 'addtertiary' :
        addTertiary();
        break;
		
	case 'addskills' :
        addSkills();
        break;
      
    case 'addlanguages' :
       addLanguages();
        break;
		
	case 'addadditionalinfo' :
       AddAdditional();//for the documents
        break;
		
	case 'addemploymenthistory' :
       AddEmpHistory();
        break;
		
	case 'addreferences' :
       addRef();
        break;
		
	case 'addproq' :
       AddProfessionalQualifications();
        break;
		
	case 'addareasofinterest':
		AddAreasOfInterest();
		break;
		
	case 'addreferals':
		AddReferals();
		break;
		
	case 'applyvacancy' :
       ApplyVacancy();
        break;
		
	case 'dellang' :
       DeleteLanguage();
        break;
		
	case 'delskill' :
       DeleteSkill();
        break;
		
	case 'delemployment' :
       DeleteEmployment();
        break;
		
	case 'delref' :
       DeleteReference();
        break;		
		
	case 'delhschool' :
       DeleteHighSchool();
        break;
		
	case 'deltschool' :
       DeleteTertiary();
        break;
		
	case 'delproq' :
       DeleteProfessionalQualifications();
        break;
		
		
	case 'delareaofinterest' :
       DeleteAreaOfInterest();
        break;
  	   
    default :
        // if action is not defined or unknown
        // move to main admin page
        header('Location: indexapplicant.php');
}


/*
    Add a Personal Information
*/
function addPersonal()
{
	$uid = $_POST['hiduid'];
    $f = $_POST['fname'];
    $m = $_POST['mname'];
    $s = $_POST['sname'];
	$e = $_POST['email'];
	$g = $_POST['gender'];	
	$d = $_POST['dob'];
	$d = date('Y-m-d', strtotime(str_replace('-', '/',$d)));
	$na = $_POST['nationality'];
	$n = $_POST['natidno'];
	$a = $_POST['address'];
	$p = $_POST['phone'];
	//email comes in from session
	
	
	if($f==''){
		$sql   = "UPDATE c_users SET fname = '$f', mname = '$m', sname = '$s', dob = '$d', gender = '$g', nationality = '$na', natidno = '$n', address =  '$a', phone = '$p' WHERE id=$uid ";

		dbQuery($sql);
		header('Location: indexapplicant.php?view=addpd&error=' . urlencode('Please enter a firstname.'));
	}
	else if(!ctype_alpha($f)){
		$sql   = "UPDATE c_users SET fname = '$f', mname = '$m', sname = '$s', dob = '$d', gender = '$g', nationality = '$na', natidno = '$n', address =  '$a', phone = '$p' WHERE id=$uid ";

		dbQuery($sql);
		header('Location: indexapplicant.php?view=addpd&error=' . urlencode('Please enter a valid first name - alphabetic letters only!'));
	}	
	else if($s==''){
		$sql   = "UPDATE c_users SET fname = '$f', mname = '$m', sname = '$s', dob = '$d', gender = '$g', nationality = '$na', natidno = '$n', address =  '$a', phone = '$p' WHERE id=$uid ";

		dbQuery($sql);
		header('Location: indexapplicant.php?view=addpd&error=' . urlencode('Please enter a surname.'));
	}
	else if(!ctype_alpha($s)){
		$sql   = "UPDATE c_users SET fname = '$f', mname = '$m', sname = '$s', dob = '$d', gender = '$g', nationality = '$na', natidno = '$n', address =  '$a', phone = '$p' WHERE id=$uid ";

		dbQuery($sql);
		header('Location: indexapplicant.php?view=addpd&error=' . urlencode('Please enter a valid surname - alphabetic letters only!'));
	}
	/*else if(!ctype_alpha($m)){
		$sql   = "UPDATE c_users SET fname = '$f', mname = '$m', sname = '$s', dob = '$d', gender = '$g', nationality = '$na', natidno = '$n', address =  '$a', phone = '$p' WHERE id=$uid ";

		dbQuery($sql);
		header('Location: indexapplicant.php?view=addpd&error=' . urlencode('Please enter a valid middlename - alphabetic letters only!'));
	}*/
	else if ($d==''){
		$sql   = "UPDATE c_users SET fname = '$f', mname = '$m', sname = '$s', dob = '$d', gender = '$g', nationality = '$na', natidno = '$n', address =  '$a', phone = '$p' WHERE id=$uid ";

		dbQuery($sql);
		header('Location: indexapplicant.php?view=addpd&error=' . urlencode('Please enter a Date of Birth.'));
	}
	else if ($na==''){
		$sql   = "UPDATE c_users SET fname = '$f', mname = '$m', sname = '$s', dob = '$d', gender = '$g', nationality = '$na', natidno = '$n', address =  '$a', phone = '$p' WHERE id=$uid ";

		dbQuery($sql);
		header('Location: indexapplicant.php?view=addpd&error=' . urlencode('Please enter nationality.'));
	}
	else if(!ctype_alpha($na)){
		$sql   = "UPDATE c_users SET fname = '$f', mname = '$m', sname = '$s', dob = '$d', gender = '$g', nationality = '$na', natidno = '$n', address =  '$a', phone = '$p' WHERE id=$uid ";

		dbQuery($sql);
		header('Location: indexapplicant.php?view=addpd&error=' . urlencode('Please enter a valid nationality - alphabetic letters only!'));
	}
	else if($n==''){
		$sql   = "UPDATE c_users SET fname = '$f', mname = '$m', sname = '$s', dob = '$d', gender = '$g', nationality = '$na', natidno = '$n', address =  '$a', phone = '$p' WHERE id=$uid ";

		dbQuery($sql);
		header('Location: indexapplicant.php?view=addpd&error=' . urlencode('Please specify an ID number: National ID or Passport No.'));
	}
	/*else if ($a==''){
		
		$sql   = "UPDATE c_users SET fname = '$f', mname = '$m', sname = '$s', dob = '$d', gender = '$g', nationality = '$na', natidno = '$n', address =  '$a', phone = '$p' WHERE id=$uid ";

		dbQuery($sql);
		header('Location: indexapplicant.php?view=addpd&error=' . urlencode('Please specify an address.'));
	}*/
	else if ($p=='' or $p=='0'){
		$sql   = "UPDATE c_users SET fname = '$f', mname = '$m', sname = '$s', dob = '$d', gender = '$g', nationality = '$na', natidno = '$n', address =  '$a', phone = '$p' WHERE id=$uid ";

		dbQuery($sql);
		header('Location: indexapplicant.php?view=addpd&error=' . urlencode('Please enter a valid phone number.'));
	}
	else if(strlen($p) < 8 || strlen($p) > 15){
		$sql   = "UPDATE c_users SET fname = '$f', mname = '$m', sname = '$s', dob = '$d', gender = '$g', nationality = '$na', natidno = '$n', address =  '$a', phone = '$p' WHERE id=$uid ";

		dbQuery($sql);
		header('Location: indexapplicant.php?view=addpd&error=' . urlencode('Please enter a valid phone number - reasonable length (normally 8-15 digits, no punctuations)'));
	}
	else{
		
		//check if we had added this entry to the tracker before
		$sqlk = "SELECT userid from applicationtrack WHERE userid='$uid'";
		$resultsk=dbQuery($sqlk);
		
		if (dbNumRows($resultsk)>0){
			$sql   = "UPDATE c_users SET fname = '$f', mname = '$m', sname = '$s', dob = '$d', gender = '$g', nationality = '$na', natidno = '$n', address =  '$a', phone = '$p' WHERE id=$uid ";

				dbQuery($sql);
				
				$sql2   = "UPDATE applicationtrack SET haspersonaldetails=1 WHERE userid='$uid'";
				dbQuery($sql2);
				
				header('Location: indexapplicant.php?view=modifypd&error=' . urlencode('Personal details updated successfully.'));
		}
		else{//if this is a new entry
			$sql   = "UPDATE c_users SET fname = '$f', mname = '$m', sname = '$s', dob = '$d', gender = '$g', nationality = '$na', natidno = '$n', address =  '$a', phone = '$p' WHERE id=$uid ";

			dbQuery($sql);
			
			$sql2   = "INSERT INTO applicationtrack (userid, haspersonaldetails,  haseducation, hasareaofinterest, hasskills, haslanguages, hasdocuments, hasreferences, dateupdated) 
			VALUES ('$uid', 1, 0, 0, 0, 0, 0, 0, NOW())";
			dbQuery($sql2);
			
			header('Location: indexapplicant.php?view=modifypd&error=' . urlencode('Personal details added successfully.'));
			
		}
	}
}


/*
    Add HighSchool
*/
function addHighSchool()
{
	$uid = $_POST['hiduid'];
    $hs = $_POST['hs'];
    $yc= $_POST['yc'];
    $exam= $_POST['exam'];
	$hsg = $_POST['hsgrade'];
	
	//check if we had added this entry to the tracker before
		$sqlk = "SELECT userid from applicationtrack WHERE userid='$uid' AND haspersonaldetails=1";
		$resultsk=dbQuery($sqlk);
		
		
		
		if (dbNumRows($resultsk)<1){
		/*
			if($hs==''){
				$sql2   = "UPDATE applicationtrack SET haseducation = 0 WHERE userid = $uid";
				dbQuery($sql2);
				header('Location: indexapplicant.php?view=addhighschool&error=' . urlencode('Please enter a valid high school name'));
	
			}else if($yc==''){
				$sql2   = "UPDATE applicationtrack SET haseducation = 0 WHERE userid = $uid";
				dbQuery($sql2);
				header('Location: indexapplicant.php?view=addhighschool&error=' . urlencode('Please enter the year you left the high school your high school'));
			}else if($hsg==''){
				$sql2   = "UPDATE applicationtrack SET haseducation = 0 WHERE userid = $uid";
				dbQuery($sql2);
				header('Location: indexapplicant.php?view=addhighschool&error=' . urlencode('Please enter a grade for your high school'));
			}
		else{
   

		$sql = "INSERT INTO c_education (userid, hschool, exam, hschoolgrade)
					VALUES ('$uid','$hs', '$exam','$hsg')";
		dbQuery($sql);
		
		$sql2   = "UPDATE applicationtrack SET haseducation = 1 WHERE userid = $uid";
		dbQuery($sql2);
		header('Location: indexapplicant.php?view=addhighschool&error=' . urlencode('High School '.$hs.' added successfully')); 
		}*/
		header('Location: indexapplicant.php?view=addpd&error=' . urlencode('Please complete your personal profile before adding your education background'));
		}
		else{
			$sql = "INSERT INTO c_education (userid, year_cleared, hschool, exam, hschoolgrade)
					VALUES ('$uid','$yc','$hs', '$exam','$hsg')";
			dbQuery($sql);
		
			$sql2   = "UPDATE applicationtrack SET haseducation = 1 WHERE userid = $uid";
			dbQuery($sql2);
			header('Location: indexapplicant.php?view=modifyeb&error=' . urlencode('High School '.$hs.' added successfully'));
			
			//header('Location: indexapplicant.php?view=addpd&error=' . urlencode('Please complete your personal profile before adding your education background')); 
		}
		
	
	 

}




/*
    Add a Education Background
*/
function addTertiary()
{
	$uid = $_POST['hiduid'];    
    $tert1 = $_POST['tert1'];
	$datetert1from = $_POST['datetert1from'];	
	$datetert1from = date('Y-m-d', strtotime(str_replace('-', '/', $datetert1from)));
	$datetert1 = $_POST['datetert1'];
	$datetert1 = date('Y-m-d', strtotime(str_replace('-', '/', $datetert1)));
	$gradetert1 = $_POST['gradetert1'];
	$d = $_POST['degree'];
	
	
		//check if we had added this entry to the tracker before
		$sqlk = "SELECT userid from applicationtrack WHERE userid='$uid' and haseducation=1;";
		$resultsk=dbQuery($sqlk);
		
		if (dbNumRows($resultsk)==1){
		if($tert1==''){
			header('Location: indexapplicant.php?view=addtertiary&error=' . urlencode('Please add a valid University/college')); 
		}
		else if($datetert1from=='' or $datetert1from==0000-00-00){
			header('Location: indexapplicant.php?view=addtertiary&error=' . urlencode('Please add a valid DATE FROM date')); 
		}
		else if($datetert1=='' or $datetert1==0000-00-00){
			header('Location: indexapplicant.php?view=addtertiary&error=' . urlencode('Please add a valid DATE TO date')); 
		}
		else if($gradetert1==''){
			header('Location: indexapplicant.php?view=addtertiary&error=' . urlencode('Please add a valid grade')); 
		}
		else{
		$sql   = "INSERT INTO c_education_tertiary (userid,  institutionname, datefrom, dateto, grade,degree,dateupdated)
					VALUES ('$uid','$tert1', '$datetert1from', '$datetert1', '$gradetert1','$d',NOW())";
		dbQuery($sql);
		//$sql2   = "UPDATE applicationtrack SET haseducation = 1 WHERE userid = $uid";
		//dbQuery($sql2);		
		header('Location: indexapplicant.php?view=modifytertiary&error=' . urlencode('Tertiary Education at '.$tert1.' added to the database'));  
		}
		}else{//not updated any personal details
		
			header('Location: indexapplicant.php?view=addhighschool&error=' . urlencode('Please complete your high school education background'));
		
		}

}



function AddProfessionalQualifications(){
	$uid = $_POST['hiduid'];    
    $proq = $_POST['proq'];
	
	
	
	
		$sql   = "INSERT INTO c_applicantprofqual (userid, courseid)
					VALUES ('$uid','$proq')";

		dbQuery($sql);
		
		header('Location: indexapplicant.php?view=modifyproq&error=' . urlencode('You have added '.$p.' as a professional qualification'));
}


function AddAreasOfInterest(){
	$uid = $_POST['hiduid'];
	$aoi = $_POST['areaofinterest'];
	$sa = $_POST['subarea'];
	
	$sqlk = "SELECT userid from applicationtrack WHERE userid='$uid' AND haseducation=1";
	$resultsk=dbQuery($sqlk);
		
		
		
	if (dbNumRows($resultsk)<1){
		header('Location: indexapplicant.php?view=addhighschool&error=' . urlencode('You have not completed your educational background'));
	}else{
	
		$sqlx = "SELECT areaofinterest,areaofinterestspecialization, userid FROM c_applicantareasofinterest
			WHERE areaofinterest= '$aoi' AND areaofinterestspecialization='$sa' AND userid=$uid";
		$resultx = dbQuery($sqlx);
	
		if (dbNumRows($resultx) == 1) {
			header('Location: indexapplicant.php?view=addareasofinterest&error=' . urlencode('This interest is already part of your profile'));	
		} else{
	
			$sql   = "insert into c_applicantareasofinterest(areaofinterest,areaofinterestspecialization, userid) values('$aoi','$sa','$uid')";
			$sql2   = "UPDATE applicationtrack SET hasareaofinterest = 1 WHERE userid = $uid";
			dbQuery($sql2);

			dbQuery($sql);
			header('Location: indexapplicant.php?view=modifyareasofinterest&error=' . urlencode('Area of interest successfully added'));
		}
	
	}
	
	
	
	
}
/* 
Add referals
*/
function AddReferals()
{
	$ref = $_POST['referall'];
	$name = $_POST['name'];
	$uid = $_POST['hiduid'];
	
	$sql   = "INSERT INTO applicant_referals (name, ref,userid)
					VALUES ('$name','$ref', '$uid')";

			dbQuery($sql);
			
	header('Location: indexapplicant.php?view=finis&error=' . urlencode('Thank you for submitting your informaton. You can go to opening to check if there are any openings'));
	
}

/*
    Add a Skills
*/
function addSkills()
{
	$sk = $_POST['skill'];
	$uid = $_POST['hiduid'];
	
	
	
	
		
		if($sk==''){
			header('Location: indexapplicant.php?view=addskills&error=' . urlencode('You cannot proceed without specifying  skill!')); 
		}else{

			$sql   = "INSERT INTO c_skills (userid, skill,isactive)
					VALUES ('$uid','$sk', 1)";

			dbQuery($sql);
		
			$sql2   = "UPDATE applicationtrack SET hasskills = 1 WHERE userid = $uid";
			dbQuery($sql2);
		
			header('Location: indexapplicant.php?view=modifysk&error=' . urlencode($sk.' added successfully.'));  
		}
		
		
	

	
}


/*
    Add a Skills
*/
function addLanguages()
{
	$lan = $_POST['lang'];
	$flu = $_POST['fluency'];
	$uid = $_POST['hiduid'];
	
	//check if we had added this entry to the tracker before
		$sqlk = "SELECT userid from applicationtrack WHERE userid='$uid' and hasskills=1";
		$resultsk=dbQuery($sqlk);
		
		if (dbNumRows($resultsk)<1){
		
			header('Location: indexapplicant.php?view=addlanguages&error=' . urlencode('Please add skills before completing addition of languages'));
		}else{//not updated any personal details
		
			if($lan==''){
				header('Location: indexapplicant.php?view=addlanguages&error=' . urlencode('Please specify a language you can interact with'));
			}else{
				$sql   = "INSERT INTO c_languages (userid, language,fluency)
					VALUES ('$uid','$lan', '$flu')";

				dbQuery($sql);
		
				$sql2   = "UPDATE applicationtrack SET haslanguages = 1 WHERE userid = $uid";
				dbQuery($sql2);
		
				header('Location: indexapplicant.php?view=addlanguages&error=' . urlencode($lan.' added successfully.'));
			}

		}
}

function AddAdditional()
{
	$uid = $_POST['hiduid'];


//This is the directory where files will be saved
$target = "upload/";
$target2 = "certificates/";
$target = $target . basename( $_FILES['Filename']['name']);
$target2 = $target2 . basename( $_FILES['Filename2']['name']);

$maxsize    = 2097152;
    $acceptable = array(
        "application/zip",   
"application/x-zip",
"application/octet-stream",
"application/x-zip-compressed"
    );

//This gets all the other information from the form
$Filename=basename( $_FILES['Filename']['name']);
$Filename2=basename( $_FILES['Filename2']['name']);


if(($_FILES['Filename2']['size'] >= $maxsize) || ($_FILES["Filename2"]["size"] == 0)) {
            header('Location: indexapplicant.php?view=modifyai&error=' . urlencode('File too large. File must be less than 2mb. You must upload your certificates'));
    }

    else if((!in_array($_FILES['Filename2']['type'], $acceptable)) && (!empty($_FILES["Filename2"]["type"]))) {
    header('Location: indexapplicant.php?view=modifyai&error=' . urlencode('Invalid file type. Only .rar and .zip files are acceptable to upload your certificates'));
}
else{



//Writes the Filename to the server
if(move_uploaded_file($_FILES['Filename']['tmp_name'], $target) && move_uploaded_file($_FILES['Filename2']['tmp_name'], $target2)) {
    //Tells you if its all ok
    echo "The file ". basename( $_FILES['Filename']['name']). "and ".basename( $_FILES['Filename2']['name'])." have been uploaded, and your information has been added to the directories";
	

$rst = dbQuery("SELECT * FROM c_additionalinfo WHERE userid=$uid;");
if (dbNumRows($rst)>0){
    $rst = dbQuery("UPDATE c_additionalinfo SET CV='$Filename', adddocs='$Filename2' WHERE userid='$uid';");
}
else{
//Writes the information to the database
    $sql ="INSERT INTO c_additionalinfo (userid,CV,adddocs)
    VALUES ('$uid','$Filename','$Filename2')";
	dbQuery($sql);
	
	$sql2   = "UPDATE applicationtrack SET hasdocuments = 1 WHERE userid = $uid";
		dbQuery($sql2);
	
	header('Location: indexapplicant.php?view=modifydocs&error=' . urlencode('Additional Information added successfully.')); 

}

} else {
    //Gives and error if its not
    header('Location: indexapplicant.php?view=modifyai&error=' . urlencode('Please recheck that the information that you have created is correct'));
}
}

}

/*

function AddAdditional()
{
	$uid = $_POST['hiduid'];


//This is the directory where files will be saved
$target = "upload/";
$target2 = "certificates/";
$target = $target . basename( $_FILES['Filename']['name']);
$target2 = $target2 . basename( $_FILES['Filename2']['name']);

$maxsize    = 2097152;
    $acceptable = array(
        "application/zip",   
"application/x-zip",
"application/octet-stream",
"application/x-zip-compressed"
    );

//This gets all the other information from the form
$Filename=basename( $_FILES['Filename']['name']);
$Filename2=basename( $_FILES['Filename2']['name']);

$sqlk = "SELECT userid from applicationtrack WHERE userid='$uid' and haslanguages=1";
		$resultsk=dbQuery($sqlk);
		
		if (dbNumRows($resultsk)<1){
			header('Location: indexapplicant.php?view=addlanguages&error=' . urlencode('Please add a language(s) that you can communicate in'));
		}else{
		
			if(($_FILES['Filename2']['size'] >= $maxsize) || ($_FILES["Filename2"]["size"] == 0)) {
            header('Location: indexapplicant.php?view=modifyai&error=' . urlencode('File too large. File must be less than 2mb. You must upload your certificates'));
    }

    else if((!in_array($_FILES['Filename2']['type'], $acceptable)) && (!empty($_FILES["Filename2"]["type"]))) {
    header('Location: indexapplicant.php?view=modifyai&error=' . urlencode('Invalid file type. Only .rar and .zip files are acceptable to upload your certificates'));
}
else{



//Writes the Filename to the server
if(move_uploaded_file($_FILES['Filename']['tmp_name'], $target) && move_uploaded_file($_FILES['Filename2']['tmp_name'], $target2)) {
    //Tells you if its all ok
    echo "The file ". basename( $_FILES['Filename']['name']). "and ".basename( $_FILES['Filename2']['name'])." have been uploaded, and your information has been added to the directories";
	
//Writes the information to the database
    $sql ="INSERT INTO c_additionalinfo (userid,CV,adddocs)
    VALUES ('$uid','$Filename','$Filename2')";
	dbQuery($sql);
	
	$sql2   = "UPDATE applicationtrack SET hasdocuments = 1 WHERE userid = $uid";
		dbQuery($sql2);
	
	header('Location: indexapplicant.php?view=modifyai&error=' . urlencode('Your CV and certificate has been added successfully.')); 

} else {
    //Gives and error if its not
    header('Location: indexapplicant.php?view=modifyai&error=' . urlencode('Please recheck that the information that you have created is correct'));
}
}
		}




}

*/

/*
    Add a Employment History
*/
function addEmpHistory()
{
	$uid = $_POST['hiduid'];
    $e1 = $_POST['e1'];
    $e1f = $_POST['e1date'];
	$e1f = date('Y-m-d', strtotime(str_replace('-', '/',$e1f)));	
    $e1t = $_POST['e1dateb'];
	$e1t = date('Y-m-d', strtotime(str_replace('-', '/',$e1t)));
	$e1jt = $_POST['e1jt'];
	$e1l = $_POST['e1location'];	
	$e1j = $_POST['e1jd'];
	
	
	
	//check if we had added this entry to the tracker before
		$sqlk = "SELECT userid from applicationtrack WHERE userid='$uid'";
		$resultsk=dbQuery($sqlk);
		
		if (dbNumRows($resultsk)==1){	
	if($e1==''){
		header('Location: indexapplicant.php?view=addemployment&error=' . urlencode('Please specify an employer or cancel addition of one!')); 
	}else if ($e1f=='' or $e1f=='0000-00-00'){
		header('Location: indexapplicant.php?view=addemployment&error=' . urlencode('Please specify an begin date for this employer or cancel addition of one!'));
	}else if ($e1t=='' or $e1t=='0000-00-00'){
		header('Location: indexapplicant.php?view=addemployment&error=' . urlencode('Please specify an end date for this employer or cancel addition of one!'));
	}
	else if ($e1l==''){
		header('Location: indexapplicant.php?view=addemployment&error=' . urlencode('Please specify a valid location for this employment or cancel addition of this employer!'));
	}/*else if ($e1jd==''){
		header('Location: indexapplicant.php?view=addemployment&error=' . urlencode('Please specify job description or cancel addition of this employer!'));
	}*/else{
		$sql   = "INSERT INTO c_employment (userid,empl1, empl1from, empl1to, empl1location, empl1jt, empl1jd)
					VALUES ('$uid','$e1', '$e1f', '$e1t', '$e1l', '$e1jt', '$e1j')";

		dbQuery($sql);
		
		
		/*
		$sqlx ="SELECT COUNT(id) FROM c_employment WHERE id=$uid";
		$resultx = dbQuery($sqlx);
		if(dbNumRows($resultx) >= 1){
			
		header('Location: indexapplicant.php?view=addemployment&error=' . urlencode('Successfully added '.$e1.' as an employer'));  
			
		}else{
			
			
		header('Location: indexapplicant.php?view=addemployment'); 
		}*/
		header('Location: indexapplicant.php?view=modifyeh&error=' . urlencode('Successfully added '.$e1.' as an employer'));  
	}
		}else{//not updated any personal details
		header('Location: indexapplicant.php?view=addpd&error=' . urlencode('Please complete your personal profile before adding your language competence'));
		}

}


/*
    Add a References
	*/
function addRef()
{
	$uid = $_POST['hiduid'];
    	$rn = $_POST['refname'];
    	$rf = $_POST['refphone'];
	$rfa = $_POST['refphonealt'];
    	$re = $_POST['refemail'];
	$des = $_POST['designation'];
	$rel = $_POST['relationship'];
	
	
	//check if we had added this entry to the tracker before
		$sqlk = "SELECT userid from applicationtrack WHERE userid='$uid'";
		$resultsk=dbQuery($sqlk);
		
		if (dbNumRows($resultsk)==1){
	if ($rn==''){
		header('Location: indexapplicant.php?view=addref&error=' . urlencode('Please specify a valid reference name'));	
	}else if($rf=='' or $rf=='0'){
		header('Location: indexapplicant.php?view=addref&error=' . urlencode('Please specify a valid phone number for this reference'));
	}else if($re==''){
		header('Location: indexapplicant.php?view=addref&error=' . urlencode('Please specify a valid email address for this reference'));
	}else if($rel==''){
		header('Location: indexapplicant.php?view=addref&error=' . urlencode('Please specify the relation yo have with this reference'));
	}
	else if(strlen($rf) < 8 || strlen($rf) > 15){
		header('Location: indexapplicant.php?view=addref&error=' . urlencode('The telephone number needs to have 8 to 10 digits'));
	}
	else if(strlen($rfa) < 8 || strlen($rfa) > 15){
		header('Location: indexapplicant.php?view=addref&error=' . urlencode('The alternative telephone number needs to have 8 to 10 digits!'));
	}
	
	else{
	// check if the user is registered name is taken
	$sqlc = "SELECT ref1email,userid
	        FROM c_referees
			WHERE ref1email= '$re' AND userid ='$uid';";
	$resultc = dbQuery($sqlc);
	
	if (dbNumRows($resultc) == 1) {
		header('Location: indexapplicant.php?view=addref&error=' . urlencode('You already have a referee with this - '.$re.' email address'));	
	} else{
	
	
//adding references
		$sql   = "INSERT INTO c_referees (userid,ref1name, ref1phone,ref1phonealt, ref1email,designation,relationship)
					VALUES ('$uid','$rn', '$rf','$rfa', '$re', '$des', '$rel');";
		dbQuery($sql);
		
//finding the current number of referees for the applicant
$sqlx ="SELECT id FROM c_referees WHERE userid=$uid;";
		$resultx = dbQuery($sqlx);
		if(dbNumRows($resultx) >= 3){
//updating the applicant profile tracker		
		$sql2   = "UPDATE applicationtrack SET hasreferences = 1 WHERE userid = '$uid';";
		dbQuery($sql2);
		} else{
			$sql2   = "UPDATE applicationtrack SET hasreferences = 0 WHERE userid = '$uid';";
		dbQuery($sql2);
			
		}
		
		header('Location: indexapplicant.php?view=addref&error=' . urlencode('Application Completed. Please Check that you have at least 3 References'));
	}
	}
		}else{//not updated any personal details
		header('Location: indexapplicant.php?view=addpd&error=' . urlencode('Please complete your personal profile before adding your language competence'));
		}
}

/*
    Apply Vacancy
	*/
function ApplyVacancy(){
	
	$uid = $_POST['hiduid'];
    $aid = $_POST['hidaid'];
	
	// check if the user has made an application
	$sql = "SELECT userid,applicationid
	        FROM c_appliedlog
			WHERE userid= '$uid' and applicationid = '$aid'";
	$result = dbQuery($sql);
	
	// check if the user has completed all checks
	$sql2 = "SELECT *
	        FROM applicationtrack
			WHERE haspersonaldetails=1 AND haseducation=1  AND hasdocuments=1 and hasreferences=1 AND
			userid = '$uid'";
	$result2 = dbQuery($sql2);
	
	
	if (dbNumRows($result) == 1) {
		header('Location: indexapplicant.php?view=expand&id='.$aid.'&error=' . urlencode('You have already applied for this vacancy! '));	
	} else if(dbNumRows($result2) < 1){
		header('Location: indexapplicant.php?view=expand&id='.$aid.'&error=' . urlencode('Your profile is not complete yet. Please complete it first to be able to apply for this opportunity!'));
			}else { 
$sql   = "INSERT INTO c_appliedlog (userid,applicationid,dateapplied,applicationstatus)
					VALUES ('$uid','$aid',NOW(),1)";

		dbQuery($sql);
		
		
		
		header('Location: indexapplicant.php?view=expand&id='.$aid.'&error=' . urlencode('Application Successful'));  
	}

}



function DeleteLanguage(){
	if (isset($_GET['id']) && (int)$_GET['id'] > 0) {
		$lid = (int)$_GET['id'];
	} else {
		header('Location: indexapplicant.php?view=modifyai');
	}
	
	$sql   = "DELETE FROM c_languages where id=$lid";

		dbQuery($sql);
		
		
		$sqlx ="SELECT COUNT(id) FROM c_education WHERE id=$lid";
		$resultx = dbQuery($sqlx);
		if(dbNumRows($resultx) < 1){
			$sql2   = "UPDATE applicationtrack SET haslanguages = 0 WHERE userid = $lid";
			dbQuery($sql2);

		}
	
	header('Location: indexapplicant.php?view=modifyai'); 
	
}


function DeleteSkill(){
	
	if (isset($_GET['id']) && (int)$_GET['id'] > 0) {
		$sid = (int)$_GET['id'];
	} else {
		header('Location: indexapplicant.php?view=modifysk');
	}
	
	$sql   = "DELETE FROM c_skills where id=$sid";

		dbQuery($sql);
		
		$sqlx ="SELECT COUNT(id) FROM c_skills WHERE id=$sid";
		$resultx = dbQuery($sqlx);
		if(dbNumRows($resultx) < 1){
			$sql2   = "UPDATE applicationtrack SET hasskills = 0 WHERE userid = $sid";
			dbQuery($sql2);

		}
	
	header('Location: indexapplicant.php?view=modifysk'); 
}


function DeleteEmployment(){
	if (isset($_GET['id']) && (int)$_GET['id'] > 0) {
		$eid = (int)$_GET['id'];
	} else {
		header('Location: indexapplicant.php?view=modifyeh');
	}
	
	$sql   = "DELETE FROM c_employment where id=$eid";

		dbQuery($sql);
		
		
	
	header('Location: indexapplicant.php?view=modifyeh');
}


function DeleteReference(){
	if (isset($_GET['id']) && (int)$_GET['id'] > 0) {
		$rid = (int)$_GET['id'];
	} else {
		header('Location: indexapplicant.php?view=modifyref');
	}
	
	$sql   = "DELETE FROM c_referees where id=$rid";

		dbQuery($sql);
		
		
		$sqlx ="SELECT COUNT(id) FROM c_referees WHERE id=$rid";
		$resultx = dbQuery($sqlx);
		if(dbNumRows($resultx) < 1){
			$sql2   = "UPDATE applicationtrack SET hasreferences = 0 WHERE userid = $rid";
			dbQuery($sql2);
		}
	
	header('Location: indexapplicant.php?view=modifyref');
}


function DeleteHighSchool(){
	if (isset($_GET['id']) && (int)$_GET['id'] > 0) {
		$hsid = (int)$_GET['id'];
	} else {
		header('Location: indexapplicant.php?view=modifyeb');
	}
	
	$sql   = "DELETE FROM c_education where id=$hsid";
	dbQuery($sql);
	
	$sqlx ="SELECT COUNT(id) FROM c_education WHERE id=$hsid";
		$resultx = dbQuery($sqlx);
		if(dbNumRows($resultx) < 1){
			$sql2   = "UPDATE applicationtrack SET haseducation = 0 WHERE userid = $hsid";
			dbQuery($sql2);

		}
	
	header('Location: indexapplicant.php?view=modifyeb');
	
}

function DeleteTertiary(){
	if (isset($_GET['id']) && (int)$_GET['id'] > 0) {
		$tsid = (int)$_GET['id'];
	} else {
		header('Location: indexapplicant.php?view=modifytertiary');
	}
	
	$sql   = "DELETE FROM c_education_tertiary where id=$tsid";
	dbQuery($sql);
	
	$sqlx ="SELECT COUNT(id) FROM c_education_tertiary WHERE id=$tsid";
		$resultx = dbQuery($sqlx);
		if(dbNumRows($resultx) < 1){
			$sql2   = "UPDATE applicationtrack SET haseducation = 0 WHERE userid = $tsid";
			dbQuery($sql2);

		}
	
	header('Location: indexapplicant.php?view=modifytertiary');
}



function DeleteProfessionalQualifications(){
	if (isset($_GET['id']) && (int)$_GET['id'] > 0) {
		$pid = (int)$_GET['id'];
	} else {
		header('Location: indexapplicant.php?view=modifyproq');
	}
	
	$sql   = "DELETE FROM c_applicantprofqual where id=$pid";
	dbQuery($sql);
	
	header('Location: indexapplicant.php?view=modifyproq');
	
}

function DeleteSkills(){
	if (isset($_GET['id']) && (int)$_GET['id'] > 0) {
		$sid = (int)$_GET['id'];
	} else {
		header('Location: indexapplicant.php?view=modifyskills');
	}
	
	$sql   = "DELETE FROM c_skills where id=$sid";
	dbQuery($sql);
	
	header('Location: indexapplicant.php?view=modifyskills');
	
}



function DeleteAreaOfInterest(){
	if (isset($_GET['id']) && (int)$_GET['id'] > 0) {
		$aoiid = (int)$_GET['id'];
	} else {
		header('Location: indexapplicant.php?view=modifytertiary');
	}
	
	$sql   = "DELETE FROM c_applicantareasofinterest where id=$aoiid";
	dbQuery($sql);
	
	$sqlx ="SELECT COUNT(id) FROM c_applicantareasofinterest WHERE id=$aoiid";
		$resultx = dbQuery($sqlx);

		if(dbNumRows($resultx) < 1){
			$sql2   = "UPDATE applicationtrack SET hasareaofinterest = 0 WHERE userid = $aoiid";
			dbQuery($sql2);

		}
		
	
	header('Location: indexapplicant.php?view=modifyareasofinterest');
}

?>