<?php
if (!defined('WEB_ROOT')) {
	exit;
}

$rowsPerPage = 10;
$sql = "SELECT s.id sid, s.userid uid, u.fname fname, u.sname sname, us.sname ussname, us.fname usfname, us.email as usemail, a.positiontitle position, s.isemailsent emailsent, s.dateshortlisted ds , a.id as aid
from shortlist s
inner join c_applications a
on a.id = s.vacancyid
inner join c_users u
on u.id = s.shortlistedby
inner join c_users us
on us.id = s.userid
";
$result     = dbQuery($sql);
 
?> 

 <?php echo '<span style="color:#FF0000;text-align:center;">'.$errorMessage.'</span>'; 
  ?>

<div class="row-fluid sortable">		
				<div class="box span12">
					<div class="box-header" data-original-title>
						<h2><i class="halflings-icon user"></i><span class="break"></span>Shortlisted Applicants (Rejection List)</h2>
                        
						<div class="box-icon">
							<a href="#" class="btn-setting"><i class="halflings-icon wrench"></i></a>
							<a href="#" class="btn-minimize"><i class="halflings-icon chevron-up"></i></a>
							<a href="#" class="btn-close"><i class="halflings-icon remove"></i></a>
						</div>
					</div>
					<div class="box-content">
                    
                     <form action="email_module_rejection.php" action="post">
						<table class="table table-striped table-bordered bootstrap-datatable datatable">
						  <thead>
							  <tr>
									<th></th>
								  <th>Name</th>
								  <th>Date Shortlisted</th>
                                  <th>Shortlisted by</th>
								  <th>Position Shortlisted For</th>
								  <th>&nbsp;</th>
                                  
								  
						    </tr>
						  </thead>   
						  <tbody>
                          <?php
if (dbNumrows($result)>0){
	while($row = dbFetchAssoc($result)) {
		extract($row);
?><input name="hiduid" type="hidden" id="hiduid" value="<?php echo $id; ?>">
							<tr>
							<td><input type="checkbox" name="shortlisted[]" value="<?php echo $uid; ?>"></td>
								<td><?php echo $usfname.' '. $ussname; ?></td>
								<td class="center"><?php echo $ds;?></td>
                                <td class="center"><?php echo $fname.' '.$sname; ?></td>
								<td class="center"><a href="indexadmin.php?view=expand&id=<?php echo $id; ?>"><?php echo $position; ?></a></td>
								<td class="center"><?php if ($emailsent==0){ ?>
									<span class="label label-fail">Email not yet sent</span><?php } else if ($emailsent==1){ ?><span class="label label-success">Email Sent</span><?php } else { ?><span class="label label-fail">Investigate User Details</span><?php } ?>
								</td>
                                
								
							</tr>
                            <?php
}// end while
}else{
?>
<tr><td colspan="6" align="center">There are no shortlisted applicants yet</td></tr>
<?php }?>

							
						  </tbody>
					  </table> 
                      <table>
                      <tr>
  <td colspan="4" align="right">&nbsp;</td><td><input type="submit" value="Send Email" onClick="window.location.href='process_admin.php?action=sendRejectionEmail';"></td></tr>
                      </table>          
                      </form> 
					</div>
				</div><!--/span-->
			
			</div><!--/row-->
			