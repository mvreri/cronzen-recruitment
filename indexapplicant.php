<?php
require_once 'library/config2.php';
require_once 'functions.php';

$_SESSION['login_return_url'] = $_SERVER['REQUEST_URI'];
checkUser();

$view = (isset($_GET['view']) && $_GET['view'] != '') ? $_GET['view'] : '';

switch ($view) {
	case 'list' :
		$heading = 'Job Openings';
		$content 	= 'applicant_viewopenings.php';		
		$pageTitle 	= 'Centum E-Recruitment | Job Openings';
		break;

	case 'modifypd' :
		$heading = 'Personal Details';
		$content 	= 'applicant_editpersonaldetails.php';		
		$pageTitle 	= 'Centum E-Recruitment | Personal Details ';
		break;
		
	case 'addpd' :
		$heading = 'Personal Details';
		$content 	= 'applicant_addpersonal.php';		
		$pageTitle 	= 'Centum E-Recruitment | Add Personal Details';
		break;
		
	case 'addareasofinterest' :
		$heading = 'Areas of Interest';
		$content 	= 'applicant_addareasofinterest.php';		
		$pageTitle 	= 'Centum E-Recruitment | Add Areas of Interest';
		break;
		
	case 'modifyareasofinterest' :
		$heading = 'Areas of Interest';
		$content 	= 'applicant_editareasofinterest.php';		
		$pageTitle 	= 'Centum E-Recruitment | Modify Areas of Interest';
		break;
		
	
	//education background	
	case 'modifyeb' :
		$heading = 'Educational Background - High School';
		$content 	= 'applicant_editeducation.php';		
		$pageTitle 	= 'Centum E-Recruitment | Education *';
		break;
	case 'modifytertiary' :
		$heading = 'Educational Background - Tertiary';
		$content 	= 'applicant_edittertiary.php';		
		$pageTitle 	= 'Centum E-Recruitment | Tertiary Education';
		break;
		
	case 'addhighschool' :
		$heading = 'Educational Background';
		$content 	= 'applicant_addhighschool.php';		
		$pageTitle 	= 'Centum E-Recruitment | Applicant Education - High School';
		break;
	case 'addtertiary' :
		$heading = 'Educational Background';
		$content 	= 'applicant_addtertiary.php';		
		$pageTitle 	= 'Centum E-Recruitment | Applicant Education - Tertiary';
		break;
		
		//Professional Qualifications
	case 'modifyproq' :
		$heading = 'Professional Qualifications';
		$content 	= 'applicant_editprofessionalqualifications.php';		
		$pageTitle 	= 'Centum E-Recruitment | Professional Qualifications';
		break;
		
		case 'addproq' :
		$heading = 'Professional Qualifications';
		$content 	= 'applicant_addprofessionalqualifications.php';		
		$pageTitle 	= 'Centum E-Recruitment | Add Professional Qualifications';
		break;
		
		
		
		
	//employment history		
	case 'modifyeh' :
		$heading = 'Employment History';
		$content 	= 'applicant_editemploymenthistory.php';		
		$pageTitle 	= 'Centum E-Recruitment | Employment History';
		break;
		
	case 'addemployment' :
		$heading = 'Educational Background';
		$content 	= 'applicant_addemployment.php';		
		$pageTitle 	= 'Centum E-Recruitment | Applicant Employment';
		break;
		
		
	//skills
	case 'modifysk' :
		$heading = 'Skills';
		$content 	= 'applicant_editskills.php';		
		$pageTitle 	= 'Centum E-Recruitment | Applicant Skillset';
		break;
		
	case 'addskills' :
		$heading = 'Skills';
		$content 	= 'applicant_addskills.php';		
		$pageTitle 	= 'Centum E-Recruitment | Applicant Skillset';
		break;	
		
	//additional Info		
	case 'modifyai' :
		$heading = 'Additional Information';
		$content 	= 'applicant_editadditionalinfo.php';		
		$pageTitle 	= 'Centum E-Recruitment | Applicant Additional Info';
		break;
		
	case 'addlanguages' :
		$heading = 'Additional Information';
		$content 	= 'applicant_addlanguages.php';		
		$pageTitle 	= 'Centum E-Recruitment | Add Languages versed in';
		break;
		
	//documents		
	case 'modifydocs' :
		$heading = 'Applicant Documentation';
		$content 	= 'applicant_editdocuments.php';		
		$pageTitle 	= 'Centum E-Recruitment | Applicant Documentation';
		break;
	
	//documents		
	case 'adddocs' :
		$heading = 'Applicant Documentation';
		$content 	= 'applicant_adddocuments.php';		
		$pageTitle 	= 'Centum E-Recruitment | Add Applicant Documentation';
		break;
		
	
		
	case 'modifyref' :
		$heading = 'References';
		$content 	= 'applicant_editreferences.php';		
		$pageTitle 	= 'Centum E-Recruitment |  References';
		break;
		
	case 'addref' :
		$heading = 'References';
		$content 	= 'applicant_addreferences.php';		
		$pageTitle 	= 'Centum E-Recruitment | Add/Edit References';
		break;
		
	case 'tnc' :
		$heading = 'Application Submissions';
		$content 	= 'applicant_tnc.php';		
		$pageTitle 	= 'Centum E-Recruitment | Terms and Conditions';
		break;
			
	case 'updates' :
		$heading = 'Updates';
		$content 	= 'applicant_updates.php';		
		$pageTitle 	= 'Centum E-Recruitment | Updates';
		break;
			
		
	case 'expand' :
		$heading = 'Job Opportunity';
		$content 	= 'applicant_viewopportunity.php';		
		$pageTitle 	= 'Centum E-Recruitment | Job Opportunity';
		break;
		
	case 'vacancyapply' :
		$heading = 'Job Opportunity';
		$content 	= 'applicant_viewopportunity.php';		
		$pageTitle 	= 'Centum E-Recruitment | Job Opportunity';
		break;
		
		
	case 'finishsub' :
		$heading = 'Personal Profile Complete';
		$content 	= 'applicant_afterprofile.php';		
		$pageTitle 	= 'Centum E-Recruitment | Profile Completed Successfully';
		break;
		
	default :
		$heading = 'Welcome';
		$content 	= 'applicant_start.php';		
		$pageTitle 	= 'Centum E-Recruitment | Dashboard';
}

require_once 'templateapplicant.php';
?>