<?php
if (!defined('WEB_ROOT')) {
	exit;
}
$cid =$_SESSION['centum_user_id'];

$sql = "SELECT * FROM c_applicantareasofinterest WHERE userid=$cid";
$result     = dbQuery($sql);
echo '<span style="color:#FF0000;text-align:center;">'.$errorMessage.'</span>';
?>
<script language="javascript">
function deleteinterest(id)
{
	if (confirm('Are you sure you would like to delete this interest?')) {
		window.location.href = 'process_applicant.php?action=delareaofinterest&id=' + id;
	}
}
</script>
<div class="row-fluid sortable">
				<div class="box span6">
					<div class="box-header">
						<h2><i class="halflings-icon align-justify"></i><span class="break"></span>Career Discipline</h2>
						<div class="box-icon">
							<a href="#" class="btn-setting"><i class="halflings-icon wrench"></i></a>
							<a href="#" class="btn-minimize"><i class="halflings-icon chevron-up"></i></a>
							<a href="#" class="btn-close"><i class="halflings-icon remove"></i></a>
						</div>
					</div>
					<div class="box-content">
						<table class="table">
							  <thead>
								  <tr>
									  <th>Sector</th>
									  <th>Career Interest</th>
									  
									  <th>&nbsp;</th>                                          
								  </tr>
							  </thead>   
							  <tbody>
                              <?php
if (dbNumRows($result) > 0) {
	$i = 0;
	
	while($row = dbFetchAssoc($result)) {
		extract($row);			
?> 
								<tr>
									<td><?php echo $areaofinterest; ?></td>
									<td><?php echo $areaofinterestspecialization; ?></td>
									<td class="center"><a href="javascript:deleteinterest(<?php echo $id; ?>);"><i class="halflings-icon remove"></i></a></td>                                       
								</tr>
                                <?php
	} // end while


?>
  <?php
}else{
?>
<tr>
									<td colspan="2">You haven't added any interest areas to your profile yet</td>
									                                        
								</tr>
<?php
}
?>

                                <tr>
									<td colspan="2" align="right"><input name="btnaoi" type="button" id="btnaoi" value="Add Career discipline" onClick="window.location.href='indexapplicant.php?view=addareasofinterest';"></td>
									                                        
								</tr>
                                <tr>
									<td colspan="2"></td>
									                                        
								</tr>
<tr>
									<td colspan="2" align="right"><input name="back" type="button" id="back" value="BACK" onClick="window.location.href='indexapplicant.php?view=modifysk';"> <input name="next" type="button" id="next" value="NEXT" onClick="window.location.href='indexapplicant.php?view=modifyai';"> </td></tr>
									                                        
								                              
							  </tbody>
						 </table>  
						      
					</div>
				</div><!--/span-->
				
				
			</div><!--/row-->