<?php
if (!defined('WEB_ROOT')) {
	exit;
}
$cid =$_SESSION['centum_user_id'];


$sql = "SELECT * FROM c_skills where userid='$cid'";
$result     = dbQuery($sql);
echo '<span style="color:#FF0000;text-align:center;">'.$errorMessage.'</span>';
?>
<script language="javascript">

function deleteSkill(id)
{
	if (confirm('Are you sure you would like to remove this skill?')) {
		window.location.href = 'process_applicant.php?action=delskill&id=' + id;
	}
}</script>
<div class="row-fluid sortable">
				<div class="box span6">
					<div class="box-header">
						<h2><i class="halflings-icon align-justify"></i><span class="break"></span>Skills | Six of Ten</h2>
						<div class="box-icon">
							<a href="#" class="btn-setting"><i class="halflings-icon wrench"></i></a>
							<a href="#" class="btn-minimize"><i class="halflings-icon chevron-up"></i></a>
							<a href="#" class="btn-close"><i class="halflings-icon remove"></i></a>
						</div>
					</div>
					<div class="box-content">
					  <table class="table">
							  <thead>
								  <tr>
									  <th>Username</th>
									  <th>Status</th>                                          
								  </tr>
							  </thead>   
							  <tbody>
                               <?php
if (dbNumRows($result) > 0) {
	while($row = dbFetchAssoc($result)) {
		extract($row);			
?> 
								<tr>
									<td><?php echo $skill; ?></td>
								  <td class="center">
										<a href="javascript:deleteSkill(<?php echo $id; ?>);"><i class="halflings-icon remove"></i></a>
									</td>                                       
								</tr>
                                <?php
	} // end while


?>
  <?php
}else{
?>
								<tr>
									<td colspan="2">No skills exist in your profile yet</td>
									                                       
								</tr>
                                <?php
}
?>
	<tr>
									<td colspan="2"><input name="btnskill" type="button" id="btnskill" value="Add Skill" onClick="window.location.href='indexapplicant.php?view=addskills';"></td>
									                                       
								</tr>
                                	<tr>
									<td colspan="2">&nbsp;</td>
									                                       
								</tr>
                                	<tr>
									<td colspan="2"><input name="back" type="button" id="back" value="BACK" onClick="window.location.href='indexapplicant.php?view=modifyeh';"> <input name="next" type="button" id="next" value="NEXT" onClick="window.location.href='indexapplicant.php?view=modifyareasofinterest';"> </td>
									                                       
								</tr>

								                                   
					    </tbody>
						 </table>  
						   
					</div>
  </div><!--/span-->
				
</div><!--/row-->