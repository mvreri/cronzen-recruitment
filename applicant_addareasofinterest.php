<?php
//require_once 'include/config.php';
$cid =$_SESSION['centum_user_id'];


$system_arr = array();
$system_arr[] = "AGRIC";
$system_arr[] = "EDU";
$system_arr[] = "HEALTH";
$system_arr[] = "ENERGY";
$system_arr[] = "FINANCE";
$system_arr[] = "FMCG";
$system_arr[] = "ICT";
$system_arr[] = "RESTATE";
$system_arr[] = "OTHER";

$position = array();
$grade['AGRIC'][] = "Agronomy";
$grade['AGRIC'][] = "Food Science";
$grade['AGRIC'][] = "Veterinary/Animal Production";
$grade['EDU'][] = "Administration";
$grade['EDU'][] = "Teaching";
$grade['HEALTH'][] = "General Practice";
$grade['HEALTH'][] = "Nursing";
$grade['HEALTH'][] = "Specialist";
$grade['ENERGY'][] = "Engineering";
$grade['ENERGY'][] = "Operations Management";
$grade['ENERGY'][] = "Project Management";
$grade['ENERGY'][] = "Research";
$grade['FINANCE'][] = "Banking";
$grade['FINANCE'][] = "Insurance";
$grade['FINANCE'][] = "Portfolio Management";
$grade['FINANCE'][] = "Research";
$grade['FINANCE'][] = "Structural Finance";
$grade['FMCG'][] = "Marketing";
$grade['FMCG'][] = "Sales";
$grade['FMCG'][] = "Warehousing";
$grade['ICT'][] = "Networks";
$grade['ICT'][] = "Automation";
$grade['ICT'][] = "Infrastructure";
$grade['RESTATE'][] = "Project Management";
$grade['RESTATE'][] = "Urban Management";
$grade['RESTATE'][] = "Property Management";
$grade['RESTATE'][] = "Engineering";
$grade['RESTATE'][] = "Sales and Letting";
$grade['RESTATE'][] = "Security Management";
$grade['OTHER'][] = "Law";
$grade['OTHER'][] = "Human resource";
$grade['OTHER'][] = "Risk";
$grade['OTHER'][] = "Marketing and Communication";

echo '<span style="color:#FF0000;text-align:center;">'.$errorMessage.'</span>'; ?>
  
<form name="frmref" id="frmref" action="process_applicant.php?action=addareasofinterest" method="post">
<table width="75%" align="center">
<input name="hiduid" type="hidden" id="hiduid" value="<?php echo $cid; ?>">
<tr>
  <td colspan="2"><b>Career Interest *</b>
  </tr>
  <tr><td>Sector *</td><td>
<select name="areaofinterest" id="areaofinterest">
<option value="AGRIC" active>Agribusiness</option>
<option value="EDU">Education</option>
<option value="ENERGY">Energy</option>
<option value="FINANCE">Financial Services</option>
<option value="FMCG">FMCG</option>
<option value="HEALTH">Health Care</option>
<option value="ICT">Information and Communication Technology</option>
<option value="RESTATE">Real Estate</option>
<option value="OTHER">Others</option>
</select></td></tr>

<tr>
<td>Career Interest *</td><td><select id="subarea" name="subarea"></select></td>

</tr>

<tr><td></td><td><input type="button" value="Back" onClick="window.location.href='indexapplicant.php?view=modifyareasofinterest';"> <input type="Submit" value="Save" ></td></tr>
</table>
</form>

<script type="text/javascript">
var s1= document.getElementById("areaofinterest");
var s2 = document.getElementById("subarea");
onchange(); //Change options after page load
s1.onchange = onchange; // change options when s1 is changed

function onchange() {
    <?php foreach ($system_arr as $sa) {?>
        if (s1.value == '<?php echo $sa; ?>') {
            option_html = "";
            <?php if (isset($grade[$sa])) { ?> // Make sure position is exist
                <?php foreach ($grade[$sa] as $value) { ?>
                    option_html += "<option><?php echo $value; ?></option>";
                <?php } ?>
            <?php } ?>
            s2.innerHTML = option_html;
        }
    <?php } ?>
}
</script>