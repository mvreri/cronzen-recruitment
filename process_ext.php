<?php

require_once 'library/config2.php';
require_once 'functions.php';

//checkUser();

$action = isset($_GET['action']) ? $_GET['action'] : '';
switch ($action) {
	
    case 'reg' :
        regUser();
        break;
	
	case 'forgotpassword' :
        forgotPassword();
        break;
  	   
    default :
        // if action is not defined or unknown
        // move to main admin page
        header('Location: index.php');
}


/*
    Register a User
*/
function regUser()
{
    $f = $_POST['fname'];
    $s = $_POST['sname'];
    $e = $_POST['email'];
    $pass = $_POST['password'];
    $pass2= $_POST['password2'];
    

	
	// check if the user is registered name is taken
	$sql = "SELECT email
	        FROM c_users
			WHERE email= '$e'";
	$result = dbQuery($sql);
	
	if (dbNumRows($result) == 1) {
		header('Location: registration.php?view=register&error=' . urlencode('A user with these credentials exists already!'));	
	} else if($pass2!=$pass){
              header('Location: registration.php?view=register&error=' . urlencode('Your passwords do not match!'));	
}/*else if($f==''){
                header('Location: registration.php?view=register&error=' . urlencode('You need to specify a name for your first name'));	
         }else if(!ctype_alpha($f)){
                header('Location: registration.php?view=register&error=' . urlencode('The first name needs to be alphabetical'));
             }
         else if($s==''){
                header('Location: registration.php?view=register&error=' . urlencode('You need to specify a name for your surname'));	
         }else if(!ctype_alpha($s)){
                header('Location: registration.php?view=register&error=' . urlencode('The surname needs to be alphabetical'));
             }
       else if($e=''){
                header('Location: registration.php?view=register&error=' . urlencode('You need to specify an email address!'));	
         }else if($pass=='' || strlen($pass)<6){
                header('Location: registration.php?view=register&error=' . urlencode('You need to specify a password that is at least 6 characters long'));
             }*/else { 
		$sql   = "INSERT INTO c_users (fname, sname, email, password, isactive, usertype)
					VALUES ('$f', '$s', '$e', '$pass', 1, 2)";

		dbQuery($sql);
		
		confirmUserEmail($e, $f, $s, $pass);
					
					
		//header('Location: index.php?view=register&error=' . urlencode('Congratulations '.$f.' '.$s.'. You have successfully been registered. Check your email for confirmation <a href='."index.php".'>Log in here</a> '));  
		header('Location: registration.php?view=afterregistration&error=' . urlencode('Congratulations '.$f.' '.$s.'. You have been successfully registered. Check your email for confirmation link.'));
	}
}


//Registration Confirmation Email
function confirmUserEmail($email,$fname,$sname,$password){
error_reporting(E_STRICT | E_ALL);

date_default_timezone_set('Etc/UTC');

require 'PHPMailer-master/PHPMailerAutoload.php';

$mail = new PHPMailer();


$mail->isSMTP();
$mail->Host = 'localhost';
$mail->SMTPAuth = true;
$mail->SMTPKeepAlive = true; // SMTP connection will not close after each email sent, reduces SMTP overhead
$mail->Port = 465;
$mail->SMTPSecure = "ssl";
$mail->Username = 'careers@centum-careers.com';
$mail->Password = 'centum123!';
$mail->setFrom('careers@centum-careers.com', 'Centum careers');
$mail->addReplyTo('careers@centum-careers.com', 'Centum careers');

$mail->Subject = "Centum Careers: Successful Registration";

    $mail->AltBody = 'To view the message, please use an HTML compatible email viewer!';
    $mail->msgHTML('Dear '.$fname.' ,<br> Your Profile has been created successfully. Please proceed to the login page to complete your profile. Your password is <strong>'.$password.'</strong><br> First, though, you need to confirm your registration by clicking <a href='."http://centum-careers.com/recruitment/?confirmation=true&ue=".$email.'>HERE</a>  ');
    $mail->addAddress($email, $fname);
    //$mail->addStringAttachment($row['photo'], 'YourPhoto.jpg'); //Assumes the image data is stored in the DB

    if (!$mail->send()) {
        echo "Mailer Error (" . str_replace("@", "&#64;", $email) . ') ' . $mail->ErrorInfo . '<br />';
        break; //Abandon sending
    } else {
        echo "Message sent to :" . $email .'<br />';
       
        
		
    }
    // Clear all addresses and attachments for next loop
    $mail->clearAddresses();
    $mail->clearAttachments();
	
}




/*
    Register a User
*/
function forgotPassword()
{

	$e = $_POST['email'];
	$newpass = generateRandomString();
	
	
	
	error_reporting(E_STRICT | E_ALL);

date_default_timezone_set('Etc/UTC');

require 'PHPMailer-master/PHPMailerAutoload.php';

$mail = new PHPMailer();

//$body = file_get_contents('contents.html');

$mail->isSMTP();
$mail->Host = 'smtp.gmail.com';
$mail->SMTPAuth = true;
$mail->SMTPKeepAlive = true; // SMTP connection will not close after each email sent, reduces SMTP overhead
$mail->Port = 587;
$mail->SMTPSecure = "tls";
$mail->Username = 'careers@centum.co.ke';
$mail->Password = 'centum123!';
$mail->setFrom('careers@centum.co.ke', 'Centum careers');
$mail->addReplyTo('careers@centum.co.ke', 'Centum careers');

$mail->Subject = "Centum Careers: Password Reset";

    $mail->AltBody = 'To view the message, please use an HTML compatible email viewer!';
    $mail->msgHTML('Dear Centum User ,<br> Your password has been reset to '.$newpass.'');
    $mail->addAddress($e, $e);
    //$mail->addStringAttachment($row['photo'], 'YourPhoto.jpg'); //Assumes the image data is stored in the DB

    if (!$mail->send()) {
        echo "Mailer Error (" . str_replace("@", "&#64;", $newpass) . ') ' . $mail->ErrorInfo . '<br />';
        break; //Abandon sending
    } else {
        echo "Message sent to :" . $e .'<br />';
        //Mark it as sent in the DB
        mysql_query(
            "UPDATE c_users SET password = '".$newpass."' WHERE email = '" . $e. "'"
        );
		
    }
    // Clear all addresses and attachments for next loop
    $mail->clearAddresses();
    $mail->clearAttachments();

 
	
}



function generateRandomString($length = 10) {
    $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
    $charactersLength = strlen($characters);
    $randomString = '';
    for ($i = 0; $i < $length; $i++) {
        $randomString .= $characters[rand(0, $charactersLength - 1)];
    }
    return $randomString;

}

?>