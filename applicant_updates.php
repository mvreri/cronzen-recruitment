<?php
$id =$_SESSION['centum_user_id'];

$rowsPerPage = 10;
$sql = "SELECT u.fname as fname, u.sname as sname,u.email as email,d.department as dept,p.name as positioname,l.isemailsent emailsent, l.isshortlisted shortlisted, l.dateapplied as thedt,a.`positiontitle` as pt, a.refno as refno, d.department as dept  from c_users u 
inner join c_appliedlog l 
on l.userid = u.id
 inner join c_applications a 
 on a.id = l.applicationid 
 inner join c_department d 
 on d.id = a.deptid 
 left join c_positions p
  on a.id = a.positionid
  WHERE u.id=$id";
$result     = dbQuery(getPagingQuery($sql, $rowsPerPage));;
$pagingLink = getPagingLink($sql, $rowsPerPage);
?> 
<div class="row-fluid sortable">
  <div class="box span12">
					<div class="box-header">
						<h2><i class="halflings-icon align-justify"></i><span class="break"></span>Opportunities Applied For</h2>
						<div class="box-icon">
							<a href="#" class="btn-minimize"><i class="halflings-icon chevron-up"></i></a>
							<a href="#" class="btn-close"><i class="halflings-icon remove"></i></a>
						</div>
					</div>
					<div class="box-content">
						<table class="table">
							  <thead>
								  <tr>
									  <th>Ref. No.</th>
									  <th>Job Title</th>
									  <th>Date Applied</th>
                                      <th>Department</th>
									  <th>Status</th>                                          
								  </tr>
							  </thead>   
							  <tbody>
                              <?php
if (dbNumRows($result) > 0) {
	$i = 0;
	
	while($row = dbFetchAssoc($result)) {
		extract($row);			
?> 
								<tr>
									<td><?php echo $refno; ?></td>
									<td class="center"><?php echo $pt; ?></td>
									<td class="center"><?php echo $thedt; ?></td>
                                    <td class="center"><?php echo $dept; ?></td>
									<td class="center">
										<?php if (($shortlisted==0) && ($emailsent==0)){ ?>
									<span class="label label-fail">Application Pending</span><?php } else if (($shortlisted==1) && ($emailsent==0)){ ?><span class="label label-warning">Shortlisted, wait for communication</span><?php } else if (($shortlisted==1) && ($emailsent==1)) { ?><span class="label label-success">Shortlisted and application still active</span><?php } ?>
									</td>                                       
								</tr>
                                <?php
	} // end while


?>
  <?php
}else{
?>
								<tr>
									<td colspan="5">You have not applied for any Job</td>                                       
								</tr>
                                <?php
}
?>
								
								
							  </tbody>
					  </table>  
						 
					</div>
				</div><!--/span--><!--/span-->
			</div><!--/row-->