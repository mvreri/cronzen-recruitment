<?php

require_once 'library/config2.php';
require_once 'functions.php';

//checkUser();

$action = isset($_GET['action']) ? $_GET['action'] : '';
switch ($action) {
	
    case 'adduser' :
        addUser();
        break;
		
	case 'addvacancy' :
        addVacancy();
        break;
      
    case 'modifyu' :
        modifyUser();
        break;
		
	case 'adddepts' :
        AddDept();
        break;
		
	case 'addpos' :
        AddPosition();
        break;
		
	case 'addjobgroups' :
        AddJobGroup();
        break;
		
	case 'addareaofinterest':
		 AddAreaOfInterest();
        break;
		
	case 'addpapers':
		 AddPapers();
        break;
		
	case 'deluser' :
        DeleteUser();
        break;		
		
	case 'activateuser' :
        ActivateUser();
        break;
		
	case 'shortlist' :
        ShortListApplicant();
        break;
		
	case 'sendOfferEmail':
		sendOfferEmail();
		break;
		
	case 'sendRejectEmail':
		sendRejectEmail();
		break;
		
	case 'addemailmessage':
		addEmailMessage();
		break;
		
	case 'delshortlist':
		deleteFromShortlist();
		break;
		
		
		
		
		
  	   
    default :
        // if action is not defined or unknown
        // move to main admin page
        header('Location: indexadmin.php');
}


/*
    Add a User
*/
function addUser()
{
    $f = $_POST['fname'];
    $s = $_POST['sname'];
	$e = $_POST['email'];
	
    $n = $_POST['natidno'];
	$p = $_POST['phone'];
    $pass = $_POST['password'];
    

	
	// check if the user is registered name is taken
	$sql = "SELECT email,natidno
	        FROM c_users
			WHERE email= '$e' or natidno = '$n'";
	$result = dbQuery($sql);
	
	if (dbNumRows($result) == 1) {
		header('Location: indexadmin.php?view=add&error=' . urlencode('A user with the name '.$f.' '.$s.' already exists'));	
	} else { 
		$sql   = "INSERT INTO c_users (fname, sname, email,natidno,phone, password, isactive, usertype)
					VALUES ('$f', '$s', '$e', '$n', '$p', '$pass', 1, 1)";

		dbQuery($sql);
		header('Location: indexadmin.php?view=add&error=' . urlencode('User '.$f.' '.$s.' added successfully'));  
	}
}


function addDept()
{
    $d = $_POST['dept'];
	
		$sql   = "insert into c_department(department)
values('$d')";

		dbQuery($sql);
		header('Location: indexadmin.php?view=addpositions&error=' . urlencode('Department '.$d.' added successfully'));  

}


function addPosition()
{
    $p = $_POST['pos'];
	$j = $_POST['jobgroup'];
	
		$sql   = "insert into c_positions(name,jobgroup)
values('$p','$j')";

		dbQuery($sql);
		header('Location: indexadmin.php?view=addvacancies&error=' . urlencode('Position '.$p.' added to job group '.$j.' successfully'));  

}


function addJobGroup()
{

	$j = $_POST['jobgroup'];
	
		$sql   = "insert into c_jobgroup(jobgroup)
values('$j')";

		dbQuery($sql);
		header('Location: indexadmin.php?view=addpositions&error=' . urlencode('Job Group '.$j.' successfully added'));  

}




function AddAreaOfInterest(){
	$aoi = $_POST['aoi'];
	$dept = $_POST['dept'];
	
		$sql   = "insert into c_areasofinterest(name,deptid)
values('$aoi','$dept')";

		dbQuery($sql);
		header('Location: indexadmin.php?view=addareasofinterest&error=' . urlencode('Area of interest '.$aoi.' successfully added'));
}



function AddPapers(){
	$pp= $_POST['pp'];
	
	
		$sql   = "insert into c_proqualifications(courses)
values('$pp')";

		dbQuery($sql);
		header('Location: indexadmin.php?view=addpapers&error=' . urlencode('Professional Course '.$pp.' successfully added'));
}




function addVacancy()
{
    $p = $_POST['position'];
	$pt = $_POST['title'];
    $r = $_POST['refno'];
    $d = $_POST['dept'];
	$rto = $_POST['rto'];
	$a = $_POST['addcontacts'];
    $g = $_POST['gender'];
    $c = $_POST['cdate'];
    $c = date('Y-m-d', strtotime(str_replace('-', '/',$c)));
	$j = $_POST['jd'];
	$uid = $_POST['hiduid'];
	

	
	
	
	// check if the user is registered name is taken
	$sql = "SELECT refno
	        FROM c_applications
			WHERE refno= '$p'";
	$result = dbQuery($sql);
	
	if (dbNumRows($result) == 1) {
		header('Location: indexadmin.php?view=addvacancies&error=' . urlencode('A vacancy with the name '.$r.' already exists'));	
	} else { 
		$sql   = "INSERT INTO c_applications (refno, positiontitle, positionid, deptid, reportingto, closingdate, jd, addedby)
					VALUES ('$r','$pt', '$p', '$d', '$rto', '$c', '$j','$uid')";

		dbQuery($sql);
		header('Location: indexadmin.php?view=addvacancies&error=' . urlencode('Vacancy '.$f.' '.$s.' added successfully'));  
	}
}


function DeleteUser()
{
	if (isset($_GET['id']) && (int)$_GET['id'] > 0) {
		$userid = (int)$_GET['id'];
	} else {
		header('Location: indexadmin.php?view=list');
	}
    //$uid = $_POST['hiduid'];
	

		$sql   = "UPDATE c_users SET isactive='0' WHERE id='$userid'";

		dbQuery($sql);
		header('Location: indexadmin.php?view=list');  
	
}


function ActivateUser()
{
	if (isset($_GET['id']) && (int)$_GET['id'] > 0) {
		$userid = (int)$_GET['id'];
	} else {
		header('Location: indexadmin.php?view=list');
	}
    //$uid = $_POST['hiduid'];
	

		$sql   = "UPDATE c_users SET isactive='1' WHERE id='$userid'";

		dbQuery($sql);
		header('Location: indexadmin.php?view=list');  
	
}


function ShortlistApplicant()
{
	if (isset($_GET['id']) && (int)$_GET['id'] > 0) {
		$userid = (int)$_GET['id'];
	} else {
		header('Location: indexadmin.php?view=list');
	}
	
	
	if (isset($_GET['vid']) && (int)$_GET['vid'] > 0) {
		$vid = (int)$_GET['vid'];
	} else {
		header('Location: indexadmin.php?view=list');
	}
	
	if (isset($_GET['cid']) && (int)$_GET['cid'] > 0) {
		$cid = (int)$_GET['cid'];
	} else {
		header('Location: indexadmin.php?view=list');
	}
	
	// check if the user has been shortlisted
	$sqlx = "SELECT s.userid, s.vacancyid, u.fname fname, u.sname sname
	        FROM shortlist s
			INNER JOIN c_users u
			ON u.id = s.userid
			INNER JOIN c_appliedlog l
			ON l.userid = u.id
			WHERE s.userid=$userid AND vacancyid=$vid AND l.isshortlisted=1";
	$resultx = dbQuery($sqlx);
	
	if (dbNumRows($resultx) > 0) {
		$row=dbFetchAssoc($resultx);
		extract($row);
		header('Location: indexadmin.php?view=viewapplicants&vid='.$vid.'&error=' . urlencode($fname.' '.$sname.' has already been shortlisted'));	
	} else { 
    
		$sql   = "INSERT INTO shortlist(userid,vacancyid,dateshortlisted,shortlistedby)
		VALUES($userid,$vid,NOW(),$cid)";
		
		$sqlr = "UPDATE c_appliedlog SET isshortlisted=1 WHERE userid=$userid AND applicationid=$vid";
		dbQuery($sqlr);

		dbQuery($sql);
		header('Location: indexadmin.php?view=viewapplicants&vid='.$vid);
		}  
	
}

function addEmailMessage(){
	$email = $_POST['textarea2'];
	
	if(!$email==''){
	// check if the user is registered name is taken
	$sql = "INSERT INTO c_emailmessages(email)
	VALUES('$email')";
	$result = dbQuery($sql);
	
	header('Location: indexadmin.php?view=confirmrecepients&error=' . urlencode('Message added. Now select recipients of the email from this list'));
	}
	else{
		header('Location: indexadmin.php?view=email&error=' . urlencode('Recipients should not receive a blank email!'));
	
	}
}


function sendOfferEmail(){

error_reporting(E_STRICT | E_ALL);
date_default_timezone_set('Etc/UTC');
require 'PHPMailer-master/PHPMailerAutoload.php';
$mail = new PHPMailer();
$EmailTo = implode(',',$_POST['shortlisted']);

$sqla = "SELECT email from c_emailmessages ORDER BY id DESC LIMIT 1";
	$resulta = dbQuery($sqla);

		//header('Location: indexadmin.php?view=email&error=' . urlencode('some rsponse'));


require_once 'email_module_invitation.php';		
header('Location: indexadmin.php?view=viewshortlistedapplicants&error=' . urlencode('Emails sent ')); 	

}


function sendRejectEmail(){
	
require_once 'email_module_rejection.php';
header('Location: indexadmin.php?view=viewshortlistedapplicants&error=' . urlencode('Emails sent successfully')); 	

}

function deleteFromShortlist(){
	if (isset($_GET['uid']) && (int)$_GET['uid'] > 0) {
		$uid = (int)$_GET['uid'];
	} else {
		header('Location: indexadmin.php?view=viewshortlistedapplicants');
	}
	
	if (isset($_GET['aid']) && (int)$_GET['aid'] > 0) {
		$aid = (int)$_GET['aid'];
	} else {
		header('Location: indexadmin.php?view=viewshortlistedapplicants');
	}
    //$uid = $_POST['hiduid'];
	

		$sqlr = "UPDATE c_appliedlog SET isshortlisted=0 WHERE userid=$uid and applicationid = $aid";
		dbQuery($sqlr);
		
		
		header('Location: indexadmin.php?view=viewshortlistedapplicants'); 
	
}


?>