 <?php

error_reporting(E_STRICT | E_ALL);

date_default_timezone_set('Etc/UTC');

require 'PHPMailer-master/PHPMailerAutoload.php';

$mail = new PHPMailer();

//$body = file_get_contents('contents.html');

$mail->isSMTP();
$mail->Host = 'smtp.gmail.com';
$mail->SMTPAuth = true;
$mail->SMTPKeepAlive = true; // SMTP connection will not close after each email sent, reduces SMTP overhead
$mail->Port = 587;
$mail->SMTPSecure = "tls";
$mail->Username = 'careers@centum.co.ke';
$mail->Password = 'centum123!';
$mail->setFrom('careers@centum.co.ke', 'Superstar Programmer');
$mail->addReplyTo('careers@centum.co.ke', 'Superstar Programmer');

$mail->Subject = "Centum Application Acceptance";

//connect to the database and select the recipients from your mailing list that have not yet been sent to
//You'll need to alter this to match your database
$mysql = mysql_connect('localhost', 'root', '');
mysql_select_db('centumdb', $mysql);
$result = mysql_query("select u.id id, u.fname fn,u.sname sn, s.dateshortlisted ds,u.email email
from c_users u
inner join shortlist s
on u.id = s.userid
WHERE isemailsent=0
", $mysql);

while ($row = mysql_fetch_array($result)) {
    $mail->AltBody = 'To view the message, please use an HTML compatible email viewer!';
    $mail->msgHTML('Dear'.$row['fn'].' '.$row['sn'].',<br> Congratulations. You have been selected to take part in the next round of interviews. Once again, congratulations!');
    $mail->addAddress($row['email'], $row['fn']);
    //$mail->addStringAttachment($row['photo'], 'YourPhoto.jpg'); //Assumes the image data is stored in the DB

    if (!$mail->send()) {
        echo "Mailer Error (" . str_replace("@", "&#64;", $row["email"]) . ') ' . $mail->ErrorInfo . '<br />';
        break; //Abandon sending
    } else {
        echo "Message sent to :" . $row['fn'] . ' (' . str_replace("@", "&#64;", $row['id']) . ')<br />';
        //Mark it as sent in the DB
        mysql_query(
            "UPDATE shortlist SET isemailsent = 1 WHERE userid = '" . mysql_real_escape_string($row['id'], $mysql) . "'"
        );
		mysql_query(
            "UPDATE c_appliedlog l SET l.isemailsent = 1 WHERE l.userid = '" . mysql_real_escape_string($row['id'], $mysql) . "'"
        );
    }
    // Clear all addresses and attachments for next loop
    $mail->clearAddresses();
    $mail->clearAttachments();
}