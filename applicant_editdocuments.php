<?php
if (!defined('WEB_ROOT')) {
	exit;
}
$cid =$_SESSION['centum_user_id'];

$sql = "SELECT * FROM c_additionalinfo WHERE userid=$cid ORDER BY id desc LIMIT 1;";
$result     = dbQuery($sql);
echo '<span style="color:#FF0000;text-align:center;">'.$errorMessage.'</span>';
?>
<script language="javascript">
function deletedocs(id)
{
	if (confirm('Are you sure you would like to delete this document set?')) {
		window.location.href = 'process_applicant.php?action=deldocs&id=' + id;
	}
}
</script>
<div class="row-fluid sortable">
				<div class="box span8">
					<div class="box-header">
						<h2><i class="halflings-icon align-justify"></i><span class="break"></span>CV and relevant documentation</h2>
						<div class="box-icon">
							<a href="#" class="btn-setting"><i class="halflings-icon wrench"></i></a>
							<a href="#" class="btn-minimize"><i class="halflings-icon chevron-up"></i></a>
							<a href="#" class="btn-close"><i class="halflings-icon remove"></i></a>
						</div>
					</div>
					<div class="box-content">
						<table class="table">
							  <thead>
								  <tr>
									  
									  <th>CV</th>
									  <th>Additional Documents</th>
									  <th>&nbsp;</th>                                          
								  </tr>
							  </thead>   
							  <tbody>
                              <?php
if (dbNumRows($result) > 0) {
	$i = 0;
	
	while($row = dbFetchAssoc($result)) {
		extract($row);			
?> 
								<tr>
									
									<td class="center"><?php echo $CV; ?></td>
									<td class="center"><?php echo $adddocs; ?></td>
									<td class="center"><a href="#" class="btn-setting"><i class="halflings-icon wrench"></i></a> <a href="javascript:deletedocs(<?php echo $id; ?>);"><i class="halflings-icon remove"></i></a></td>                                       
								</tr>
                                <?php
	} // end while


?>
  <?php
}else{
?>
<tr>
									<td colspan="3">You haven't added any documents yet</td>
									                                        
								</tr>
<?php
}
?>  

 <tr>
									<td colspan="3" align="right"><input name="btnhs" type="button" id="btnhs" value="Add Documents  (Upload)" onClick="window.location.href='indexapplicant.php?view=adddocs';"></td>
									                                        
								</tr>
                                <tr>
									<td colspan="3"></td>
									                                        
								</tr>                      
<tr>
									<td colspan="3" align="right"> <input name="back" type="button" id="back" value="BACK" onClick="window.location.href='indexapplicant.php?view=modifyai';">  <input name="next" type="button" id="next" value="NEXT" onClick="window.location.href='indexapplicant.php?view=modifyref';"></td></tr>
									
									                                        
								                              
							  </tbody>
						 </table>  
						      
					</div>
				</div><!--/span-->
				
				
			</div><!--/row-->