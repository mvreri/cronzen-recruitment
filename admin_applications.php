<?php
if (!defined('WEB_ROOT')) {
	exit;
}

$rowsPerPage = 10;
$sql = "SELECT u.fname as fname, u.sname as sname,u.email,d.department,a.positiontitle as positioname, l.dateapplied as thedt, l.isemailsent as emailsent ,l.isshortlisted as shortlisted, a.id as id from c_users u 
inner join c_appliedlog l 
on l.userid = u.id
 inner join c_applications a 
 on a.id = l.applicationid 
 inner join c_department d 
 on d.id = a.deptid 
 left join c_positions p
  on a.id = a.positionid";
$result     = dbQuery(getPagingQuery($sql, $rowsPerPage));;
$pagingLink = getPagingLink($sql, $rowsPerPage);
?> 
<div class="row-fluid sortable">		
				<div class="box span12">
					<div class="box-header" data-original-title>
						<h2><i class="halflings-icon user"></i><span class="break"></span>Current Applications</h2>
						<div class="box-icon">
							<a href="#" ><i class="halflings-icon wrench"></i></a>
							<a href="#" class="btn-minimize"><i class="halflings-icon chevron-up"></i></a>
							<a href="#" class="btn-close"><i class="halflings-icon remove"></i></a>
						</div>
					</div>
					<div class="box-content">
						<table class="table table-striped table-bordered bootstrap-datatable datatable">
						  <thead>
							  <tr>
								  <th>Candidate Name</th>
								  <th>Email Address</th>
								  <th>Position Applied For</th>
                                  <th>Date of Application</th>
								  <th>Shortlisted Status</th>
                                  <th>Email Sent Status</th>
								  
							  </tr>
						  </thead>   
						  <tbody>
                           <?php
if (dbNumrows($result)>0){
	while($row = dbFetchAssoc($result)) {
		extract($row);
?>
							<tr>
								<td><?php echo $fname.' '.$sname; ?></td>
								<td class="center"><?php echo $email; ?></td>
								<td class="center"><a href="indexadmin.php?view=expand&id=<?php echo $id; ?>"><?php echo $positioname; ?></a></td>
                                <td class="center"><?php echo $thedt; ?></td>
                                <td class="center">
									<?php if ($shortlisted==0){ ?>
									<span class="label label-fail">Not Yet Shortlisted/Rejected</span><?php } else if ($shortlisted==1){ ?><span class="label label-success">Shortlisted</span><?php } else { ?><span class="label label-fail">Investigate User Details</span><?php } ?>
								</td>
								<td class="center">
									<?php if ($emailsent==0){ ?>
									<span class="label label-fail">Email not yet sent</span><?php } else if ($emailsent==1){ ?><span class="label label-success">Email Sent</span><?php } else { ?><span class="label label-fail">Investigate User Details</span><?php } ?>
								</td>
								
							</tr>
                            <?php
}// end while
}else{
?>
<tr><td colspan="6" align="center">There are no applications on the system yet</td></tr>
<?php }?>
							
						  </tbody>
					  </table>            
					</div>
				</div><!--/span-->
			
			</div><!--/row-->