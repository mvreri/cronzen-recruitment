<?php
require_once 'include/config.php';
$cid =$_SESSION['centum_user_id']; 
  echo '<span style="color:#FF0000;text-align:center;">'.$errorMessage.'</span>'; ?>


<form name="addvacancy" id="addvacancy" action="process_admin.php?action=addvacancy" method="post">
<table width="75%" align="center">
<input name="hiduid" type="hidden" id="hiduid" value="<?php echo $cid; ?>">
<tr>
<td>Position</td><td><input type="text" id="title" name="title" placeholder="Vacancy title" required /></td>
</tr>
<tr>
<td>Ref. No.</td><td><input type="text" id="refno" name="refno" placeholder="Ref. No." required /></td>
</tr>
<tr>
<td>Department</td><td><?php
   $r = mysqli_query($conn,"SELECT * FROM c_department");?>
   <select name="dept">
       <?php
       $dept=$_POST['dept'];
       while($queryd = mysqli_fetch_array($r, MYSQL_ASSOC)){?>
       <option value="<?php echo $queryd['id']; ?>"
			<?php if ($queryd['department']==$dept){?>
               selected
			<?php }?> >
					<?php echo $queryd['department']; ?>
       </option>
					<?php } ?>
</select>&nbsp;<a href="indexadmin.php?view=adddepts">Add Departments</a></td>
</tr>
<tr>
<td>Reporting To</td><td><?php
   $r = mysqli_query($conn,"SELECT * FROM c_positions");?>
   <select name="rto">
       <?php
       $rto=$_POST['rto'];
       while($querye = mysqli_fetch_array($r, MYSQL_ASSOC)){?>
       <option value="<?php echo $querye['id']; ?>"
			<?php if ($querye['name']==$rto){?>
               selected
			<?php }?> >
					<?php echo $querye['name']; ?>
       </option>
					<?php } ?>
</select>&nbsp;<a href="indexadmin.php?view=addpositions">Add Positions</a></td>
</tr>
<tr>
<td>Additional Contacts</td><td><input type="text" id="addcontacts" placeholder="Additional Contact" name="addcontacts" required /></td>
</tr>
<tr>
<td>Position Type</td><td><select id="position" name="position" required />
<option>Select Position Type</option>
<option value="Internship">Internship</option>
<option value="Temporary">Temporary</option>
<option value="Contract">Contract</option>
<option value="Permanent and Pensionable">Permanent and Pensionable</option>
</select></td>
</tr>

<tr>
<td>Closing Date</td><td><input type="text" class="input-xlarge datepicker" id="cdate" name="cdate" required /></td>
</tr>
<tr>
<td>Job Description</td><td><textarea cols="80" rows="10" id="jd" name="jd" required /></textarea></td>
</tr>
<tr><td></td><td><input type="submit" value="Add Vacancy"></td></tr>
</table>
</form>