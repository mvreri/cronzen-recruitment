<div class="row-fluid sortable">		
				<div class="box span12">
					<div class="box-header" data-original-title>
						<h2><i class="halflings-icon user"></i><span class="break"></span>Recruitment Reports</h2>
						<div class="box-icon">
							<a href="#" class="btn-setting"><i class="halflings-icon wrench"></i></a>
							<a href="#" class="btn-minimize"><i class="halflings-icon chevron-up"></i></a>
							<a href="#" class="btn-close"><i class="halflings-icon remove"></i></a>
						</div>
					</div>
					<div class="box-content">
						<table class="table table-striped table-bordered bootstrap-datatable datatable">
						  <thead>
							  <tr>
								  <th>Title </th>
								  <th>Report ID </th>
								  <th>Department</th>
								  
							  </tr>
						  </thead>   
						  <tbody>
							<tr>
								<td>Candidate Report, January</td>
								<td class="center">2012/01/01</td>
								<td class="center">Finance</td>
								
							</tr>
							<tr>
								<td>Candidate Report, February</td>
								<td class="center">2012/01/01</td>
								<td class="center">Finance</td>
								
							</tr>
							<tr>
								<td>Candidate Report, March</td>
								<td class="center">2012/01/01</td>
								<td class="center">Finance</td>
								
								</td>
							</tr>
							<tr>
								<td>Candidate Report, April</td>
								<td class="center">2012/01/01</td>
								<td class="center">Finance</td>
								
							</tr>
							<tr>
								<td>Candidate Report, January</td>
								<td class="center">2012/02/01</td>
								<td class="center">IT</td>
								
							</tr>
							<tr>
								<td>Candidate Report, February</td>
								<td class="center">2012/02/01</td>
								<td class="center">IT</td>
								
							</tr>
							<tr>
								<td>Candidate Report, June</td>
								<td class="center">2012/03/01</td>
								<td class="center">Finance</td>
								
							</tr>
							
						  </tbody>
					  </table>            
					</div>
				</div><!--/span-->
			
			</div><!--/row-->